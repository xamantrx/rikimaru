"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const null_checker_helper_1 = require("../helpers/null.checker.helper");
const client_1 = require("./client");
const config_1 = require("./config");
class RescueCenter {
    static async RequireParameter(message, cmd, command) {
        return new Promise((resolve, reject) => {
            const client = client_1.ClientManager.Client;
            const prefix = config_1.Config.COMMAND_PREFIX;
            let example = cmd.Example;
            if (example === undefined) {
                example = "";
            }
            else {
                example = cmd.Example.Get(command, cmd.Example.Count);
            }
            const msg = cmd.ParameterRequired && !null_checker_helper_1.NullCheck.Fine(command.Parameter)
                ? {
                    embed: {
                        color: message.member.highestRole.color,
                        title: `**${config_1.Config.BOT_NAME} Rescue Center**`,
                        description: `The command ***${prefix}${command.Name}*** requires a parameter.`,
                        fields: [
                            {
                                name: `Example|s for ***${prefix}${command.Name}*** : `,
                                // tslint:disable-next-line:max-line-length
                                value: example
                            }
                        ],
                        timestamp: new Date(),
                        footer: {
                            icon_url: client.user.avatarURL,
                            text: `© ${config_1.Config.BOT_NAME}`
                        }
                    }
                }
                : `The command ***${prefix}${command.Name}*** doesn't need a parameter.`;
            resolve(msg);
        });
    }
}
exports.RescueCenter = RescueCenter;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzY3VlLmNlbnRlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb3JlL3Jlc2N1ZS5jZW50ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFFQSx3RUFBMkQ7QUFHM0QscUNBQXlDO0FBQ3pDLHFDQUFrQztBQUVsQyxNQUFhLFlBQVk7SUFDaEIsTUFBTSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsQ0FDbEMsT0FBZ0IsRUFDaEIsR0FBZSxFQUNmLE9BQWlCO1FBRWpCLE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDckMsTUFBTSxNQUFNLEdBQUcsc0JBQWEsQ0FBQyxNQUFNLENBQUM7WUFDcEMsTUFBTSxNQUFNLEdBQUcsZUFBTSxDQUFDLGNBQWMsQ0FBQztZQUNyQyxJQUFJLE9BQU8sR0FBUSxHQUFHLENBQUMsT0FBTyxDQUFDO1lBQy9CLElBQUksT0FBTyxLQUFLLFNBQVMsRUFBRTtnQkFDekIsT0FBTyxHQUFHLEVBQUUsQ0FBQzthQUNkO2lCQUFNO2dCQUNMLE9BQU8sR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUN2RDtZQUNELE1BQU0sR0FBRyxHQUNQLEdBQUcsQ0FBQyxpQkFBaUIsSUFBSSxDQUFDLCtCQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUM7Z0JBQ3pELENBQUMsQ0FBQztvQkFDRSxLQUFLLEVBQUU7d0JBQ0wsS0FBSyxFQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLEtBQUs7d0JBQ3ZDLEtBQUssRUFBRSxLQUFLLGVBQU0sQ0FBQyxRQUFRLGtCQUFrQjt3QkFDN0MsV0FBVyxFQUFFLGtCQUFrQixNQUFNLEdBQ25DLE9BQU8sQ0FBQyxJQUNWLDJCQUEyQjt3QkFDM0IsTUFBTSxFQUFFOzRCQUNOO2dDQUNFLElBQUksRUFBRSxvQkFBb0IsTUFBTSxHQUFHLE9BQU8sQ0FBQyxJQUFJLFFBQVE7Z0NBQ3ZELDJDQUEyQztnQ0FDM0MsS0FBSyxFQUFFLE9BQU87NkJBQ2Y7eUJBQ0Y7d0JBQ0QsU0FBUyxFQUFFLElBQUksSUFBSSxFQUFFO3dCQUNyQixNQUFNLEVBQUU7NEJBQ04sUUFBUSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUzs0QkFDL0IsSUFBSSxFQUFFLEtBQUssZUFBTSxDQUFDLFFBQVEsRUFBRTt5QkFDN0I7cUJBQ0Y7aUJBQ0Y7Z0JBQ0gsQ0FBQyxDQUFDLGtCQUFrQixNQUFNLEdBQ3RCLE9BQU8sQ0FBQyxJQUNWLCtCQUErQixDQUFDO1lBQ3RDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNmLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztDQUNGO0FBNUNELG9DQTRDQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1lc3NhZ2UgfSBmcm9tICdkaXNjb3JkLmpzJztcblxuaW1wb3J0IHsgTnVsbENoZWNrIH0gZnJvbSAnLi4vaGVscGVycy9udWxsLmNoZWNrZXIuaGVscGVyJztcbmltcG9ydCB7IElDb21tYW5kIH0gZnJvbSAnLi4vaW50ZXJmYWNlcy9jb21tYW5kLmludGVyZmFjZSc7XG5pbXBvcnQgeyBCb3RDb21tYW5kIH0gZnJvbSAnLi8uLi9jb21tYW5kL2JvdC5jb21tYW5kJztcbmltcG9ydCB7IENsaWVudE1hbmFnZXIgfSBmcm9tICcuL2NsaWVudCc7XG5pbXBvcnQgeyBDb25maWcgfSBmcm9tICcuL2NvbmZpZyc7XG5cbmV4cG9ydCBjbGFzcyBSZXNjdWVDZW50ZXIge1xuICBwdWJsaWMgc3RhdGljIGFzeW5jIFJlcXVpcmVQYXJhbWV0ZXIoXG4gICAgbWVzc2FnZTogTWVzc2FnZSxcbiAgICBjbWQ6IEJvdENvbW1hbmQsXG4gICAgY29tbWFuZDogSUNvbW1hbmRcbiAgKSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGNvbnN0IGNsaWVudCA9IENsaWVudE1hbmFnZXIuQ2xpZW50O1xuICAgICAgY29uc3QgcHJlZml4ID0gQ29uZmlnLkNPTU1BTkRfUFJFRklYO1xuICAgICAgbGV0IGV4YW1wbGU6IGFueSA9IGNtZC5FeGFtcGxlO1xuICAgICAgaWYgKGV4YW1wbGUgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICBleGFtcGxlID0gXCJcIjtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGV4YW1wbGUgPSBjbWQuRXhhbXBsZS5HZXQoY29tbWFuZCwgY21kLkV4YW1wbGUuQ291bnQpO1xuICAgICAgfVxuICAgICAgY29uc3QgbXNnOiBhbnkgPVxuICAgICAgICBjbWQuUGFyYW1ldGVyUmVxdWlyZWQgJiYgIU51bGxDaGVjay5GaW5lKGNvbW1hbmQuUGFyYW1ldGVyKVxuICAgICAgICAgID8ge1xuICAgICAgICAgICAgICBlbWJlZDoge1xuICAgICAgICAgICAgICAgIGNvbG9yOiBtZXNzYWdlLm1lbWJlci5oaWdoZXN0Um9sZS5jb2xvcixcbiAgICAgICAgICAgICAgICB0aXRsZTogYCoqJHtDb25maWcuQk9UX05BTUV9IFJlc2N1ZSBDZW50ZXIqKmAsXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IGBUaGUgY29tbWFuZCAqKioke3ByZWZpeH0ke1xuICAgICAgICAgICAgICAgICAgY29tbWFuZC5OYW1lXG4gICAgICAgICAgICAgICAgfSoqKiByZXF1aXJlcyBhIHBhcmFtZXRlci5gLFxuICAgICAgICAgICAgICAgIGZpZWxkczogW1xuICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBuYW1lOiBgRXhhbXBsZXxzIGZvciAqKioke3ByZWZpeH0ke2NvbW1hbmQuTmFtZX0qKiogOiBgLFxuICAgICAgICAgICAgICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bWF4LWxpbmUtbGVuZ3RoXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlOiBleGFtcGxlXG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICB0aW1lc3RhbXA6IG5ldyBEYXRlKCksXG4gICAgICAgICAgICAgICAgZm9vdGVyOiB7XG4gICAgICAgICAgICAgICAgICBpY29uX3VybDogY2xpZW50LnVzZXIuYXZhdGFyVVJMLFxuICAgICAgICAgICAgICAgICAgdGV4dDogYMKpICR7Q29uZmlnLkJPVF9OQU1FfWBcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICA6IGBUaGUgY29tbWFuZCAqKioke3ByZWZpeH0ke1xuICAgICAgICAgICAgICBjb21tYW5kLk5hbWVcbiAgICAgICAgICAgIH0qKiogZG9lc24ndCBuZWVkIGEgcGFyYW1ldGVyLmA7XG4gICAgICByZXNvbHZlKG1zZyk7XG4gICAgfSk7XG4gIH1cbn1cbiJdfQ==