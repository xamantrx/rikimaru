"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Sender {
    static Send(message, content, isDM = false, callback) {
        if (isDM) {
            message.author
                .send(content)
                .then(($m) => {
                if (callback !== null && callback !== undefined) {
                    callback();
                }
                console.log(`Message <${$m.id}> was sent to "${message.author.username}".`);
            })
                .catch((err) => {
                message.reply(`Oh!, it seems that I can't dm you.`);
                if (callback !== null && callback !== undefined) {
                    callback();
                }
                console.log(`Sender.ts: "${err.message}"`);
            });
        }
        else {
            message
                .reply(content)
                .then(($m) => {
                if (callback !== null && callback !== undefined) {
                    callback();
                }
            })
                .catch((err) => {
                if (callback !== null && callback !== undefined) {
                    callback();
                }
                console.log(`Sender.ts: "${err.message}"`);
            });
        }
    }
    static SendInfo(message, content, isDM = false) {
        this.Send(message, content, isDM);
    }
    static SendError(message, isDM = false) {
        this.Send(message, `:x: Oops, there was some error, please try again later.`, isDM);
    }
}
exports.Sender = Sender;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VuZGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2NvcmUvc2VuZGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUEsTUFBYSxNQUFNO0lBQ1YsTUFBTSxDQUFDLElBQUksQ0FDaEIsT0FBZ0IsRUFDaEIsT0FBWSxFQUNaLE9BQWdCLEtBQUssRUFDckIsUUFBcUI7UUFFckIsSUFBSSxJQUFJLEVBQUU7WUFDUixPQUFPLENBQUMsTUFBTTtpQkFDWCxJQUFJLENBQUMsT0FBTyxDQUFDO2lCQUNiLElBQUksQ0FBQyxDQUFDLEVBQVcsRUFBRSxFQUFFO2dCQUNwQixJQUFJLFFBQVEsS0FBSyxJQUFJLElBQUksUUFBUSxLQUFLLFNBQVMsRUFBRTtvQkFDL0MsUUFBUSxFQUFFLENBQUM7aUJBQ1o7Z0JBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FDVCxZQUFZLEVBQUUsQ0FBQyxFQUFFLGtCQUFrQixPQUFPLENBQUMsTUFBTSxDQUFDLFFBQVEsSUFBSSxDQUMvRCxDQUFDO1lBQ0osQ0FBQyxDQUFDO2lCQUNELEtBQUssQ0FBQyxDQUFDLEdBQW9CLEVBQUUsRUFBRTtnQkFDOUIsT0FBTyxDQUFDLEtBQUssQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDO2dCQUNwRCxJQUFJLFFBQVEsS0FBSyxJQUFJLElBQUksUUFBUSxLQUFLLFNBQVMsRUFBRTtvQkFDL0MsUUFBUSxFQUFFLENBQUM7aUJBQ1o7Z0JBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEdBQUcsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1lBQzdDLENBQUMsQ0FBQyxDQUFDO1NBQ047YUFBTTtZQUNMLE9BQU87aUJBQ0osS0FBSyxDQUFDLE9BQU8sQ0FBQztpQkFDZCxJQUFJLENBQUMsQ0FBQyxFQUFXLEVBQUUsRUFBRTtnQkFDcEIsSUFBSSxRQUFRLEtBQUssSUFBSSxJQUFJLFFBQVEsS0FBSyxTQUFTLEVBQUU7b0JBQy9DLFFBQVEsRUFBRSxDQUFDO2lCQUNaO1lBQ0gsQ0FBQyxDQUFDO2lCQUNELEtBQUssQ0FBQyxDQUFDLEdBQW9CLEVBQUUsRUFBRTtnQkFDOUIsSUFBSSxRQUFRLEtBQUssSUFBSSxJQUFJLFFBQVEsS0FBSyxTQUFTLEVBQUU7b0JBQy9DLFFBQVEsRUFBRSxDQUFDO2lCQUNaO2dCQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxHQUFHLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQztZQUM3QyxDQUFDLENBQUMsQ0FBQztTQUNOO0lBQ0gsQ0FBQztJQUVNLE1BQU0sQ0FBQyxRQUFRLENBQ3BCLE9BQWdCLEVBQ2hCLE9BQVksRUFDWixPQUFnQixLQUFLO1FBRXJCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBRU0sTUFBTSxDQUFDLFNBQVMsQ0FBQyxPQUFnQixFQUFFLE9BQWdCLEtBQUs7UUFDN0QsSUFBSSxDQUFDLElBQUksQ0FDUCxPQUFPLEVBQ1AseURBQXlELEVBQ3pELElBQUksQ0FDTCxDQUFDO0lBQ0osQ0FBQztDQUNGO0FBekRELHdCQXlEQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpc2NvcmRBUElFcnJvciwgTWVzc2FnZSB9IGZyb20gJ2Rpc2NvcmQuanMnO1xuXG5leHBvcnQgY2xhc3MgU2VuZGVyIHtcbiAgcHVibGljIHN0YXRpYyBTZW5kKFxuICAgIG1lc3NhZ2U6IE1lc3NhZ2UsXG4gICAgY29udGVudDogYW55LFxuICAgIGlzRE06IGJvb2xlYW4gPSBmYWxzZSxcbiAgICBjYWxsYmFjaz86ICgpID0+IHZvaWRcbiAgKSB7XG4gICAgaWYgKGlzRE0pIHtcbiAgICAgIG1lc3NhZ2UuYXV0aG9yXG4gICAgICAgIC5zZW5kKGNvbnRlbnQpXG4gICAgICAgIC50aGVuKCgkbTogTWVzc2FnZSkgPT4ge1xuICAgICAgICAgIGlmIChjYWxsYmFjayAhPT0gbnVsbCAmJiBjYWxsYmFjayAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBjYWxsYmFjaygpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBjb25zb2xlLmxvZyhcbiAgICAgICAgICAgIGBNZXNzYWdlIDwkeyRtLmlkfT4gd2FzIHNlbnQgdG8gXCIke21lc3NhZ2UuYXV0aG9yLnVzZXJuYW1lfVwiLmBcbiAgICAgICAgICApO1xuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goKGVycjogRGlzY29yZEFQSUVycm9yKSA9PiB7XG4gICAgICAgICAgbWVzc2FnZS5yZXBseShgT2ghLCBpdCBzZWVtcyB0aGF0IEkgY2FuJ3QgZG0geW91LmApO1xuICAgICAgICAgIGlmIChjYWxsYmFjayAhPT0gbnVsbCAmJiBjYWxsYmFjayAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBjYWxsYmFjaygpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBjb25zb2xlLmxvZyhgU2VuZGVyLnRzOiBcIiR7ZXJyLm1lc3NhZ2V9XCJgKTtcbiAgICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIG1lc3NhZ2VcbiAgICAgICAgLnJlcGx5KGNvbnRlbnQpXG4gICAgICAgIC50aGVuKCgkbTogTWVzc2FnZSkgPT4ge1xuICAgICAgICAgIGlmIChjYWxsYmFjayAhPT0gbnVsbCAmJiBjYWxsYmFjayAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBjYWxsYmFjaygpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSlcbiAgICAgICAgLmNhdGNoKChlcnI6IERpc2NvcmRBUElFcnJvcikgPT4ge1xuICAgICAgICAgIGlmIChjYWxsYmFjayAhPT0gbnVsbCAmJiBjYWxsYmFjayAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBjYWxsYmFjaygpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBjb25zb2xlLmxvZyhgU2VuZGVyLnRzOiBcIiR7ZXJyLm1lc3NhZ2V9XCJgKTtcbiAgICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIHN0YXRpYyBTZW5kSW5mbyhcbiAgICBtZXNzYWdlOiBNZXNzYWdlLFxuICAgIGNvbnRlbnQ6IGFueSxcbiAgICBpc0RNOiBib29sZWFuID0gZmFsc2VcbiAgKSB7XG4gICAgdGhpcy5TZW5kKG1lc3NhZ2UsIGNvbnRlbnQsIGlzRE0pO1xuICB9XG5cbiAgcHVibGljIHN0YXRpYyBTZW5kRXJyb3IobWVzc2FnZTogTWVzc2FnZSwgaXNETTogYm9vbGVhbiA9IGZhbHNlKSB7XG4gICAgdGhpcy5TZW5kKFxuICAgICAgbWVzc2FnZSxcbiAgICAgIGA6eDogT29wcywgdGhlcmUgd2FzIHNvbWUgZXJyb3IsIHBsZWFzZSB0cnkgYWdhaW4gbGF0ZXIuYCxcbiAgICAgIGlzRE1cbiAgICApO1xuICB9XG59XG4iXX0=