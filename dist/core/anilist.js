"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const endeavor = require("endeavor");
const search_variables_1 = require("../graphql/variables/search.variables");
const graphql_1 = require("../graphql/graphql");
const config_1 = require("./config");
const request_promise_1 = __importDefault(require("request-promise"));
class AniList {
    static MediaSearch(search) {
        return new Promise(async (resolve, reject) => {
            const query = graphql_1.GraphQL.SearchQL;
            const variables = search_variables_1.SearchVariables.Get(search, 1, 100, "ANIME");
            await this.OnQueryDone();
            this.Querying = true;
            await endeavor
                .queryAnilist({ query, variables })
                .then(result => {
                this.Querying = false;
                resolve(result);
            })
                .catch(error => {
                this.Querying = false;
                reject(error);
            });
        });
    }
    static MediaQuery(id) {
        return new Promise(async (resolve, reject) => {
            const query = graphql_1.GraphQL.AnimeQL;
            const variables = search_variables_1.SearchVariables.Media(id);
            await this.OnQueryDone();
            this.Querying = true;
            await endeavor
                .queryAnilist({ query, variables })
                .then(result => {
                this.Querying = false;
                resolve(result);
            })
                .catch(error => {
                this.Querying = false;
                console.log(error);
            });
        });
    }
    static UserQuery(username) {
        return new Promise(async (resolve, reject) => {
            const pinged = await this.Ping(username);
            if (pinged) {
                const query = graphql_1.GraphQL.UserQL;
                const variables = search_variables_1.SearchVariables.User(username);
                await this.OnQueryDone();
                this.Querying = true;
                await endeavor
                    .queryAnilist({ query, variables })
                    .then(result => {
                    this.Querying = false;
                    resolve(result);
                })
                    .catch(error => {
                    console.log(error);
                    this.Querying = false;
                    resolve(null);
                });
            }
            else {
                console.log(`AniList user "${username}" was not found...`);
                resolve(null);
            }
        });
    }
    static async MediaListQuery(id) {
        return new Promise(async (resolve, reject) => {
            const query = graphql_1.GraphQL.UserMediaListQL;
            const variables = search_variables_1.SearchVariables.UserMediaList(id);
            await this.OnQueryDone();
            this.Querying = true;
            await endeavor
                .queryAnilist({ query, variables })
                .then(result => {
                this.Querying = false;
                resolve(result);
            })
                .catch(error => {
                console.log(error);
                this.Querying = false;
                resolve(null);
            });
        });
    }
    static Ping(username) {
        return new Promise(async (resolve, reject) => {
            await this.OnPingDone();
            const url = `${config_1.Config.ANILIST_USER_BASE}/${username}`;
            const options = { uri: url };
            this.Pinging = true;
            request_promise_1.default(options)
                .then(async () => {
                this.Pinging = false;
                resolve(true);
            })
                .catch(() => {
                this.Pinging = false;
                resolve(false);
            });
        });
    }
    static OnQueryDone() {
        return new Promise((resolve, reject) => {
            setInterval(() => {
                if (!this.Querying) {
                    resolve();
                }
            }, 100);
        });
    }
    static OnPingDone() {
        return new Promise((resolve, reject) => {
            setInterval(() => {
                if (!this.Pinging) {
                    resolve();
                }
            }, 100);
        });
    }
}
AniList.Querying = false;
AniList.Pinging = false;
exports.AniList = AniList;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5pbGlzdC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb3JlL2FuaWxpc3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxxQ0FBc0M7QUFDdEMsNEVBQXdFO0FBQ3hFLGdEQUE2QztBQUM3QyxxQ0FBa0M7QUFDbEMsc0VBQWlDO0FBRWpDLE1BQWEsT0FBTztJQUlYLE1BQU0sQ0FBQyxXQUFXLENBQUMsTUFBYztRQUN0QyxPQUFPLElBQUksT0FBTyxDQUFTLEtBQUssRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDbkQsTUFBTSxLQUFLLEdBQUcsaUJBQU8sQ0FBQyxRQUFRLENBQUM7WUFDL0IsTUFBTSxTQUFTLEdBQUcsa0NBQWUsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDL0QsTUFBTSxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDekIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7WUFDckIsTUFBTSxRQUFRO2lCQUNYLFlBQVksQ0FBQyxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsQ0FBQztpQkFDbEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUNiLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO2dCQUN0QixPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDbEIsQ0FBQyxDQUFDO2lCQUNELEtBQUssQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDYixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztnQkFDdEIsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2hCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0sTUFBTSxDQUFDLFVBQVUsQ0FBQyxFQUFVO1FBQ2pDLE9BQU8sSUFBSSxPQUFPLENBQVMsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNuRCxNQUFNLEtBQUssR0FBRyxpQkFBTyxDQUFDLE9BQU8sQ0FBQztZQUM5QixNQUFNLFNBQVMsR0FBRyxrQ0FBZSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUM1QyxNQUFNLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUN6QixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztZQUNyQixNQUFNLFFBQVE7aUJBQ1gsWUFBWSxDQUFDLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxDQUFDO2lCQUNsQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ2IsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7Z0JBQ3RCLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNsQixDQUFDLENBQUM7aUJBQ0QsS0FBSyxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUNiLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO2dCQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3JCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0sTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFnQjtRQUN0QyxPQUFPLElBQUksT0FBTyxDQUFTLEtBQUssRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDbkQsTUFBTSxNQUFNLEdBQUcsTUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3pDLElBQUksTUFBTSxFQUFFO2dCQUNWLE1BQU0sS0FBSyxHQUFHLGlCQUFPLENBQUMsTUFBTSxDQUFDO2dCQUM3QixNQUFNLFNBQVMsR0FBRyxrQ0FBZSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDakQsTUFBTSxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO2dCQUNyQixNQUFNLFFBQVE7cUJBQ1gsWUFBWSxDQUFDLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxDQUFDO3FCQUNsQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUU7b0JBQ2IsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7b0JBQ3RCLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDbEIsQ0FBQyxDQUFDO3FCQUNELEtBQUssQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDYixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNuQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztvQkFDdEIsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNoQixDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNMLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLFFBQVEsb0JBQW9CLENBQUMsQ0FBQztnQkFDM0QsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2Y7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTSxNQUFNLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxFQUFVO1FBQzNDLE9BQU8sSUFBSSxPQUFPLENBQVMsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNuRCxNQUFNLEtBQUssR0FBRyxpQkFBTyxDQUFDLGVBQWUsQ0FBQztZQUN0QyxNQUFNLFNBQVMsR0FBRyxrQ0FBZSxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNwRCxNQUFNLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUN6QixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztZQUNyQixNQUFNLFFBQVE7aUJBQ1gsWUFBWSxDQUFDLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxDQUFDO2lCQUNsQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ2IsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7Z0JBQ3RCLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNsQixDQUFDLENBQUM7aUJBQ0QsS0FBSyxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUNiLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ25CLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO2dCQUN0QixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDaEIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQWdCO1FBQ2xDLE9BQU8sSUFBSSxPQUFPLENBQVUsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNwRCxNQUFNLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUN4QixNQUFNLEdBQUcsR0FBRyxHQUFHLGVBQU0sQ0FBQyxpQkFBaUIsSUFBSSxRQUFRLEVBQUUsQ0FBQztZQUN0RCxNQUFNLE9BQU8sR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQztZQUM3QixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztZQUNwQix5QkFBRSxDQUFDLE9BQU8sQ0FBQztpQkFDUixJQUFJLENBQUMsS0FBSyxJQUFJLEVBQUU7Z0JBQ2YsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7Z0JBQ3JCLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNoQixDQUFDLENBQUM7aUJBQ0QsS0FBSyxDQUFDLEdBQUcsRUFBRTtnQkFDVixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDckIsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2pCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU8sTUFBTSxDQUFDLFdBQVc7UUFDeEIsT0FBTyxJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNyQyxXQUFXLENBQUMsR0FBRyxFQUFFO2dCQUNmLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO29CQUNsQixPQUFPLEVBQUUsQ0FBQztpQkFDWDtZQUNILENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNWLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLE1BQU0sQ0FBQyxVQUFVO1FBQ3ZCLE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDckMsV0FBVyxDQUFDLEdBQUcsRUFBRTtnQkFDZixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRTtvQkFDakIsT0FBTyxFQUFFLENBQUM7aUJBQ1g7WUFDSCxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDVixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7O0FBM0hjLGdCQUFRLEdBQUcsS0FBSyxDQUFDO0FBQ2pCLGVBQU8sR0FBRyxLQUFLLENBQUM7QUFGakMsMEJBNkhDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGVuZGVhdm9yID0gcmVxdWlyZShcImVuZGVhdm9yXCIpO1xyXG5pbXBvcnQgeyBTZWFyY2hWYXJpYWJsZXMgfSBmcm9tIFwiLi4vZ3JhcGhxbC92YXJpYWJsZXMvc2VhcmNoLnZhcmlhYmxlc1wiO1xyXG5pbXBvcnQgeyBHcmFwaFFMIH0gZnJvbSBcIi4uL2dyYXBocWwvZ3JhcGhxbFwiO1xyXG5pbXBvcnQgeyBDb25maWcgfSBmcm9tIFwiLi9jb25maWdcIjtcclxuaW1wb3J0IHJwIGZyb20gXCJyZXF1ZXN0LXByb21pc2VcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBBbmlMaXN0IHtcclxuICBwcml2YXRlIHN0YXRpYyBRdWVyeWluZyA9IGZhbHNlO1xyXG4gIHByaXZhdGUgc3RhdGljIFBpbmdpbmcgPSBmYWxzZTtcclxuXHJcbiAgcHVibGljIHN0YXRpYyBNZWRpYVNlYXJjaChzZWFyY2g6IHN0cmluZykge1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlPG9iamVjdD4oYXN5bmMgKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICBjb25zdCBxdWVyeSA9IEdyYXBoUUwuU2VhcmNoUUw7XHJcbiAgICAgIGNvbnN0IHZhcmlhYmxlcyA9IFNlYXJjaFZhcmlhYmxlcy5HZXQoc2VhcmNoLCAxLCAxMDAsIFwiQU5JTUVcIik7XHJcbiAgICAgIGF3YWl0IHRoaXMuT25RdWVyeURvbmUoKTtcclxuICAgICAgdGhpcy5RdWVyeWluZyA9IHRydWU7XHJcbiAgICAgIGF3YWl0IGVuZGVhdm9yXHJcbiAgICAgICAgLnF1ZXJ5QW5pbGlzdCh7IHF1ZXJ5LCB2YXJpYWJsZXMgfSlcclxuICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgdGhpcy5RdWVyeWluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgcmVzb2x2ZShyZXN1bHQpO1xyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLmNhdGNoKGVycm9yID0+IHtcclxuICAgICAgICAgIHRoaXMuUXVlcnlpbmcgPSBmYWxzZTtcclxuICAgICAgICAgIHJlamVjdChlcnJvcik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBzdGF0aWMgTWVkaWFRdWVyeShpZDogbnVtYmVyKSB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2U8b2JqZWN0Pihhc3luYyAocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgIGNvbnN0IHF1ZXJ5ID0gR3JhcGhRTC5BbmltZVFMO1xyXG4gICAgICBjb25zdCB2YXJpYWJsZXMgPSBTZWFyY2hWYXJpYWJsZXMuTWVkaWEoaWQpO1xyXG4gICAgICBhd2FpdCB0aGlzLk9uUXVlcnlEb25lKCk7XHJcbiAgICAgIHRoaXMuUXVlcnlpbmcgPSB0cnVlO1xyXG4gICAgICBhd2FpdCBlbmRlYXZvclxyXG4gICAgICAgIC5xdWVyeUFuaWxpc3QoeyBxdWVyeSwgdmFyaWFibGVzIH0pXHJcbiAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgIHRoaXMuUXVlcnlpbmcgPSBmYWxzZTtcclxuICAgICAgICAgIHJlc29sdmUocmVzdWx0KTtcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5jYXRjaChlcnJvciA9PiB7XHJcbiAgICAgICAgICB0aGlzLlF1ZXJ5aW5nID0gZmFsc2U7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBzdGF0aWMgVXNlclF1ZXJ5KHVzZXJuYW1lOiBzdHJpbmcpIHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZTxvYmplY3Q+KGFzeW5jIChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgY29uc3QgcGluZ2VkID0gYXdhaXQgdGhpcy5QaW5nKHVzZXJuYW1lKTtcclxuICAgICAgaWYgKHBpbmdlZCkge1xyXG4gICAgICAgIGNvbnN0IHF1ZXJ5ID0gR3JhcGhRTC5Vc2VyUUw7XHJcbiAgICAgICAgY29uc3QgdmFyaWFibGVzID0gU2VhcmNoVmFyaWFibGVzLlVzZXIodXNlcm5hbWUpO1xyXG4gICAgICAgIGF3YWl0IHRoaXMuT25RdWVyeURvbmUoKTtcclxuICAgICAgICB0aGlzLlF1ZXJ5aW5nID0gdHJ1ZTtcclxuICAgICAgICBhd2FpdCBlbmRlYXZvclxyXG4gICAgICAgICAgLnF1ZXJ5QW5pbGlzdCh7IHF1ZXJ5LCB2YXJpYWJsZXMgfSlcclxuICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuUXVlcnlpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgcmVzb2x2ZShyZXN1bHQpO1xyXG4gICAgICAgICAgfSlcclxuICAgICAgICAgIC5jYXRjaChlcnJvciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcclxuICAgICAgICAgICAgdGhpcy5RdWVyeWluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICByZXNvbHZlKG51bGwpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coYEFuaUxpc3QgdXNlciBcIiR7dXNlcm5hbWV9XCIgd2FzIG5vdCBmb3VuZC4uLmApO1xyXG4gICAgICAgIHJlc29sdmUobnVsbCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHN0YXRpYyBhc3luYyBNZWRpYUxpc3RRdWVyeShpZDogbnVtYmVyKSB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2U8b2JqZWN0Pihhc3luYyAocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgIGNvbnN0IHF1ZXJ5ID0gR3JhcGhRTC5Vc2VyTWVkaWFMaXN0UUw7XHJcbiAgICAgIGNvbnN0IHZhcmlhYmxlcyA9IFNlYXJjaFZhcmlhYmxlcy5Vc2VyTWVkaWFMaXN0KGlkKTtcclxuICAgICAgYXdhaXQgdGhpcy5PblF1ZXJ5RG9uZSgpO1xyXG4gICAgICB0aGlzLlF1ZXJ5aW5nID0gdHJ1ZTtcclxuICAgICAgYXdhaXQgZW5kZWF2b3JcclxuICAgICAgICAucXVlcnlBbmlsaXN0KHsgcXVlcnksIHZhcmlhYmxlcyB9KVxyXG4gICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICB0aGlzLlF1ZXJ5aW5nID0gZmFsc2U7XHJcbiAgICAgICAgICByZXNvbHZlKHJlc3VsdCk7XHJcbiAgICAgICAgfSlcclxuICAgICAgICAuY2F0Y2goZXJyb3IgPT4ge1xyXG4gICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xyXG4gICAgICAgICAgdGhpcy5RdWVyeWluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgcmVzb2x2ZShudWxsKTtcclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBzdGF0aWMgUGluZyh1c2VybmFtZTogc3RyaW5nKSB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2U8Ym9vbGVhbj4oYXN5bmMgKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICBhd2FpdCB0aGlzLk9uUGluZ0RvbmUoKTtcclxuICAgICAgY29uc3QgdXJsID0gYCR7Q29uZmlnLkFOSUxJU1RfVVNFUl9CQVNFfS8ke3VzZXJuYW1lfWA7XHJcbiAgICAgIGNvbnN0IG9wdGlvbnMgPSB7IHVyaTogdXJsIH07XHJcbiAgICAgIHRoaXMuUGluZ2luZyA9IHRydWU7XHJcbiAgICAgIHJwKG9wdGlvbnMpXHJcbiAgICAgICAgLnRoZW4oYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgICAgdGhpcy5QaW5naW5nID0gZmFsc2U7XHJcbiAgICAgICAgICByZXNvbHZlKHRydWUpO1xyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLmNhdGNoKCgpID0+IHtcclxuICAgICAgICAgIHRoaXMuUGluZ2luZyA9IGZhbHNlO1xyXG4gICAgICAgICAgcmVzb2x2ZShmYWxzZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgc3RhdGljIE9uUXVlcnlEb25lKCkge1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgc2V0SW50ZXJ2YWwoKCkgPT4ge1xyXG4gICAgICAgIGlmICghdGhpcy5RdWVyeWluZykge1xyXG4gICAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSwgMTAwKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBzdGF0aWMgT25QaW5nRG9uZSgpIHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgIHNldEludGVydmFsKCgpID0+IHtcclxuICAgICAgICBpZiAoIXRoaXMuUGluZ2luZykge1xyXG4gICAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSwgMTAwKTtcclxuICAgIH0pO1xyXG4gIH1cclxufVxyXG4iXX0=