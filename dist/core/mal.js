"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("./config");
const request_promise_1 = __importDefault(require("request-promise"));
const json_helper_1 = require("../helpers/json.helper");
const mal_anime_model_1 = require("../models/mal.anime.model");
const cheerio_1 = __importDefault(require("cheerio"));
class MAL {
    static GetCWList(username) {
        return new Promise(async (resolve, reject) => {
            await this.OnDoneFetch();
            const url = config_1.Config.MAL_CW_LINK(username);
            const options = {
                uri: url,
                json: true
            };
            this.Fetching = true;
            request_promise_1.default(options)
                .then(async (result) => {
                const converted = await json_helper_1.JsonHelper.ArrayConvert(result, mal_anime_model_1.MalAnime);
                if (converted != null || converted !== undefined) {
                    this.Fetching = false;
                    resolve(converted);
                }
                else {
                    console.log(`Result is either 'null' or 'undefined'.`);
                    this.Fetching = false;
                    resolve(null);
                }
            })
                .catch((err) => {
                console.log(err);
                this.Fetching = false;
                resolve(null);
            });
        });
    }
    static GetProfileAbout(username) {
        return new Promise(async (resolve, reject) => {
            await this.OnDoneFetch();
            const url = `${config_1.Config.MAL_PROFILE_BASE}/${username}`;
            const options = {
                uri: url,
                transform: function (body) {
                    return cheerio_1.default.load(body);
                }
            };
            this.Fetching = true;
            request_promise_1.default(options)
                .then(($) => {
                this.Fetching = false;
                resolve($(".profile-about-user")
                    .find(".word-break")
                    .text());
            })
                .catch(() => {
                console.log(`MAL user "${username}" not found...`);
                this.Fetching = false;
                resolve(null);
            });
        });
    }
    static OnDoneFetch() {
        return new Promise((resolve, reject) => {
            setInterval(() => {
                if (!this.Fetching) {
                    resolve();
                }
            }, 100);
        });
    }
}
MAL.Fetching = false;
exports.MAL = MAL;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2NvcmUvbWFsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEscUNBQWtDO0FBQ2xDLHNFQUFpQztBQUNqQyx3REFBb0Q7QUFDcEQsK0RBQXFEO0FBQ3JELHNEQUE4QjtBQUU5QixNQUFhLEdBQUc7SUFHUCxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQWdCO1FBQ3RDLE9BQU8sSUFBSSxPQUFPLENBQWEsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUN2RCxNQUFNLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUN6QixNQUFNLEdBQUcsR0FBRyxlQUFNLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3pDLE1BQU0sT0FBTyxHQUFHO2dCQUNkLEdBQUcsRUFBRSxHQUFHO2dCQUNSLElBQUksRUFBRSxJQUFJO2FBQ1gsQ0FBQztZQUNGLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ3JCLHlCQUFFLENBQUMsT0FBTyxDQUFDO2lCQUNSLElBQUksQ0FBQyxLQUFLLEVBQUUsTUFBVyxFQUFFLEVBQUU7Z0JBQzFCLE1BQU0sU0FBUyxHQUFHLE1BQU0sd0JBQVUsQ0FBQyxZQUFZLENBQzdDLE1BQU0sRUFDTiwwQkFBUSxDQUNULENBQUM7Z0JBQ0YsSUFBSSxTQUFTLElBQUksSUFBSSxJQUFJLFNBQVMsS0FBSyxTQUFTLEVBQUU7b0JBQ2hELElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO29CQUN0QixPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7aUJBQ3BCO3FCQUFNO29CQUNMLE9BQU8sQ0FBQyxHQUFHLENBQUMseUNBQXlDLENBQUMsQ0FBQztvQkFDdkQsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7b0JBQ3RCLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDZjtZQUNILENBQUMsQ0FBQztpQkFDRCxLQUFLLENBQUMsQ0FBQyxHQUFRLEVBQUUsRUFBRTtnQkFDbEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7Z0JBQ3RCLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNoQixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVNLE1BQU0sQ0FBQyxlQUFlLENBQUMsUUFBZ0I7UUFDNUMsT0FBTyxJQUFJLE9BQU8sQ0FBUyxLQUFLLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQ25ELE1BQU0sSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3pCLE1BQU0sR0FBRyxHQUFHLEdBQUcsZUFBTSxDQUFDLGdCQUFnQixJQUFJLFFBQVEsRUFBRSxDQUFDO1lBQ3JELE1BQU0sT0FBTyxHQUFHO2dCQUNkLEdBQUcsRUFBRSxHQUFHO2dCQUNSLFNBQVMsRUFBRSxVQUFTLElBQVk7b0JBQzlCLE9BQU8saUJBQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzVCLENBQUM7YUFDRixDQUFDO1lBQ0YsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7WUFDckIseUJBQUUsQ0FBQyxPQUFPLENBQUM7aUJBQ1IsSUFBSSxDQUFDLENBQUMsQ0FBZ0IsRUFBRSxFQUFFO2dCQUN6QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztnQkFDdEIsT0FBTyxDQUNMLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQztxQkFDckIsSUFBSSxDQUFDLGFBQWEsQ0FBQztxQkFDbkIsSUFBSSxFQUFFLENBQ1YsQ0FBQztZQUNKLENBQUMsQ0FBQztpQkFDRCxLQUFLLENBQUMsR0FBRyxFQUFFO2dCQUNWLE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxRQUFRLGdCQUFnQixDQUFDLENBQUM7Z0JBQ25ELElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO2dCQUN0QixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDaEIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTyxNQUFNLENBQUMsV0FBVztRQUN4QixPQUFPLElBQUksT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQ3JDLFdBQVcsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2YsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7b0JBQ2xCLE9BQU8sRUFBRSxDQUFDO2lCQUNYO1lBQ0gsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ1YsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOztBQXRFYyxZQUFRLEdBQUcsS0FBSyxDQUFDO0FBRGxDLGtCQXdFQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbmZpZyB9IGZyb20gXCIuL2NvbmZpZ1wiO1xuaW1wb3J0IHJwIGZyb20gXCJyZXF1ZXN0LXByb21pc2VcIjtcbmltcG9ydCB7IEpzb25IZWxwZXIgfSBmcm9tIFwiLi4vaGVscGVycy9qc29uLmhlbHBlclwiO1xuaW1wb3J0IHsgTWFsQW5pbWUgfSBmcm9tIFwiLi4vbW9kZWxzL21hbC5hbmltZS5tb2RlbFwiO1xuaW1wb3J0IGNoZWVyaW8gZnJvbSBcImNoZWVyaW9cIjtcblxuZXhwb3J0IGNsYXNzIE1BTCB7XG4gIHByaXZhdGUgc3RhdGljIEZldGNoaW5nID0gZmFsc2U7XG5cbiAgcHVibGljIHN0YXRpYyBHZXRDV0xpc3QodXNlcm5hbWU6IHN0cmluZykge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZTxNYWxBbmltZVtdPihhc3luYyAocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBhd2FpdCB0aGlzLk9uRG9uZUZldGNoKCk7XG4gICAgICBjb25zdCB1cmwgPSBDb25maWcuTUFMX0NXX0xJTksodXNlcm5hbWUpO1xuICAgICAgY29uc3Qgb3B0aW9ucyA9IHtcbiAgICAgICAgdXJpOiB1cmwsXG4gICAgICAgIGpzb246IHRydWVcbiAgICAgIH07XG4gICAgICB0aGlzLkZldGNoaW5nID0gdHJ1ZTtcbiAgICAgIHJwKG9wdGlvbnMpXG4gICAgICAgIC50aGVuKGFzeW5jIChyZXN1bHQ6IGFueSkgPT4ge1xuICAgICAgICAgIGNvbnN0IGNvbnZlcnRlZCA9IGF3YWl0IEpzb25IZWxwZXIuQXJyYXlDb252ZXJ0PE1hbEFuaW1lPihcbiAgICAgICAgICAgIHJlc3VsdCxcbiAgICAgICAgICAgIE1hbEFuaW1lXG4gICAgICAgICAgKTtcbiAgICAgICAgICBpZiAoY29udmVydGVkICE9IG51bGwgfHwgY29udmVydGVkICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHRoaXMuRmV0Y2hpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgIHJlc29sdmUoY29udmVydGVkKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY29uc29sZS5sb2coYFJlc3VsdCBpcyBlaXRoZXIgJ251bGwnIG9yICd1bmRlZmluZWQnLmApO1xuICAgICAgICAgICAgdGhpcy5GZXRjaGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgcmVzb2x2ZShudWxsKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaCgoZXJyOiBhbnkpID0+IHtcbiAgICAgICAgICBjb25zb2xlLmxvZyhlcnIpO1xuICAgICAgICAgIHRoaXMuRmV0Y2hpbmcgPSBmYWxzZTtcbiAgICAgICAgICByZXNvbHZlKG51bGwpO1xuICAgICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIHB1YmxpYyBzdGF0aWMgR2V0UHJvZmlsZUFib3V0KHVzZXJuYW1lOiBzdHJpbmcpIHtcbiAgICByZXR1cm4gbmV3IFByb21pc2U8c3RyaW5nPihhc3luYyAocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBhd2FpdCB0aGlzLk9uRG9uZUZldGNoKCk7XG4gICAgICBjb25zdCB1cmwgPSBgJHtDb25maWcuTUFMX1BST0ZJTEVfQkFTRX0vJHt1c2VybmFtZX1gO1xuICAgICAgY29uc3Qgb3B0aW9ucyA9IHtcbiAgICAgICAgdXJpOiB1cmwsXG4gICAgICAgIHRyYW5zZm9ybTogZnVuY3Rpb24oYm9keTogc3RyaW5nKSB7XG4gICAgICAgICAgcmV0dXJuIGNoZWVyaW8ubG9hZChib2R5KTtcbiAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIHRoaXMuRmV0Y2hpbmcgPSB0cnVlO1xuICAgICAgcnAob3B0aW9ucylcbiAgICAgICAgLnRoZW4oKCQ6IENoZWVyaW9TdGF0aWMpID0+IHtcbiAgICAgICAgICB0aGlzLkZldGNoaW5nID0gZmFsc2U7XG4gICAgICAgICAgcmVzb2x2ZShcbiAgICAgICAgICAgICQoXCIucHJvZmlsZS1hYm91dC11c2VyXCIpXG4gICAgICAgICAgICAgIC5maW5kKFwiLndvcmQtYnJlYWtcIilcbiAgICAgICAgICAgICAgLnRleHQoKVxuICAgICAgICAgICk7XG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaCgoKSA9PiB7XG4gICAgICAgICAgY29uc29sZS5sb2coYE1BTCB1c2VyIFwiJHt1c2VybmFtZX1cIiBub3QgZm91bmQuLi5gKTtcbiAgICAgICAgICB0aGlzLkZldGNoaW5nID0gZmFsc2U7XG4gICAgICAgICAgcmVzb2x2ZShudWxsKTtcbiAgICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIHN0YXRpYyBPbkRvbmVGZXRjaCgpIHtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgc2V0SW50ZXJ2YWwoKCkgPT4ge1xuICAgICAgICBpZiAoIXRoaXMuRmV0Y2hpbmcpIHtcbiAgICAgICAgICByZXNvbHZlKCk7XG4gICAgICAgIH1cbiAgICAgIH0sIDEwMCk7XG4gICAgfSk7XG4gIH1cbn1cbiJdfQ==