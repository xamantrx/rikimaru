"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const title_helper_1 = require("../helpers/title.helper");
const client_1 = require("./client");
const random_helper_1 = require("../helpers/random.helper");
const anime_cache_1 = require("./anime.cache");
const null_checker_helper_1 = require("../helpers/null.checker.helper");
class BotPresence {
    static Init() {
        return new Promise(async (resolve, reject) => {
            const media = await anime_cache_1.AnimeCache.GetRandom();
            if (!null_checker_helper_1.NullCheck.Fine(media)) {
                resolve();
                return;
            }
            const title = title_helper_1.TitleHelper.Get(media.title);
            const action = random_helper_1.Random.Range(2, 3);
            let musicType = "";
            if (action === 2) {
                musicType = this.MusicType[random_helper_1.Random.Range(0, 1)];
            }
            const user = await client_1.ClientManager.GetClientUser();
            user
                .setActivity(`${musicType} ${title}`, { type: action })
                .then(presence => {
                resolve();
            })
                .catch((err) => {
                console.log(err.name);
            });
        });
    }
}
BotPresence.MusicType = ["Ending Song of", "Opening Song of"];
exports.BotPresence = BotPresence;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJlc2VuY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvY29yZS9wcmVzZW5jZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDBEQUFzRDtBQUN0RCxxQ0FBeUM7QUFFekMsNERBQWtEO0FBQ2xELCtDQUEyQztBQUMzQyx3RUFBMkQ7QUFDM0QsTUFBYSxXQUFXO0lBR2YsTUFBTSxDQUFDLElBQUk7UUFDaEIsT0FBTyxJQUFJLE9BQU8sQ0FBQyxLQUFLLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQzNDLE1BQU0sS0FBSyxHQUFHLE1BQU0sd0JBQVUsQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUMzQyxJQUFJLENBQUMsK0JBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQzFCLE9BQU8sRUFBRSxDQUFDO2dCQUNWLE9BQU87YUFDUjtZQUNELE1BQU0sS0FBSyxHQUFHLDBCQUFXLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMzQyxNQUFNLE1BQU0sR0FBRyxzQkFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDbEMsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO1lBQ25CLElBQUksTUFBTSxLQUFLLENBQUMsRUFBRTtnQkFDaEIsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsc0JBQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDaEQ7WUFDRCxNQUFNLElBQUksR0FBRyxNQUFNLHNCQUFhLENBQUMsYUFBYSxFQUFFLENBQUM7WUFDakQsSUFBSTtpQkFDRCxXQUFXLENBQUMsR0FBRyxTQUFTLElBQUksS0FBSyxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLENBQUM7aUJBQ3RELElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDZixPQUFPLEVBQUUsQ0FBQztZQUNaLENBQUMsQ0FBQztpQkFDRCxLQUFLLENBQUMsQ0FBQyxHQUFvQixFQUFFLEVBQUU7Z0JBQzlCLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOztBQXpCYyxxQkFBUyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztBQURuRSxrQ0EyQkMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBUaXRsZUhlbHBlciB9IGZyb20gXCIuLi9oZWxwZXJzL3RpdGxlLmhlbHBlclwiO1xuaW1wb3J0IHsgQ2xpZW50TWFuYWdlciB9IGZyb20gXCIuL2NsaWVudFwiO1xuaW1wb3J0IHsgRGlzY29yZEFQSUVycm9yIH0gZnJvbSBcImRpc2NvcmQuanNcIjtcbmltcG9ydCB7IFJhbmRvbSB9IGZyb20gXCIuLi9oZWxwZXJzL3JhbmRvbS5oZWxwZXJcIjtcbmltcG9ydCB7IEFuaW1lQ2FjaGUgfSBmcm9tIFwiLi9hbmltZS5jYWNoZVwiO1xuaW1wb3J0IHsgTnVsbENoZWNrIH0gZnJvbSBcIi4uL2hlbHBlcnMvbnVsbC5jaGVja2VyLmhlbHBlclwiO1xuZXhwb3J0IGNsYXNzIEJvdFByZXNlbmNlIHtcbiAgcHJpdmF0ZSBzdGF0aWMgTXVzaWNUeXBlID0gW1wiRW5kaW5nIFNvbmcgb2ZcIiwgXCJPcGVuaW5nIFNvbmcgb2ZcIl07XG5cbiAgcHVibGljIHN0YXRpYyBJbml0KCkge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZShhc3luYyAocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBjb25zdCBtZWRpYSA9IGF3YWl0IEFuaW1lQ2FjaGUuR2V0UmFuZG9tKCk7XG4gICAgICBpZiAoIU51bGxDaGVjay5GaW5lKG1lZGlhKSkge1xuICAgICAgICByZXNvbHZlKCk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIGNvbnN0IHRpdGxlID0gVGl0bGVIZWxwZXIuR2V0KG1lZGlhLnRpdGxlKTtcbiAgICAgIGNvbnN0IGFjdGlvbiA9IFJhbmRvbS5SYW5nZSgyLCAzKTtcbiAgICAgIGxldCBtdXNpY1R5cGUgPSBcIlwiO1xuICAgICAgaWYgKGFjdGlvbiA9PT0gMikge1xuICAgICAgICBtdXNpY1R5cGUgPSB0aGlzLk11c2ljVHlwZVtSYW5kb20uUmFuZ2UoMCwgMSldO1xuICAgICAgfVxuICAgICAgY29uc3QgdXNlciA9IGF3YWl0IENsaWVudE1hbmFnZXIuR2V0Q2xpZW50VXNlcigpO1xuICAgICAgdXNlclxuICAgICAgICAuc2V0QWN0aXZpdHkoYCR7bXVzaWNUeXBlfSAke3RpdGxlfWAsIHsgdHlwZTogYWN0aW9uIH0pXG4gICAgICAgIC50aGVuKHByZXNlbmNlID0+IHtcbiAgICAgICAgICByZXNvbHZlKCk7XG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaCgoZXJyOiBEaXNjb3JkQVBJRXJyb3IpID0+IHtcbiAgICAgICAgICBjb25zb2xlLmxvZyhlcnIubmFtZSk7XG4gICAgICAgIH0pO1xuICAgIH0pO1xuICB9XG59XG4iXX0=