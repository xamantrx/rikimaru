"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongo_1 = require("../core/mongo");
const table_1 = require("../core/table");
const json_helper_1 = require("../helpers/json.helper");
const array_helper_1 = require("../helpers/array.helper");
const ani_bind_model_1 = require("../models/ani.bind.model");
const null_checker_helper_1 = require("../helpers/null.checker.helper");
class AniBindData {
    static Init() {
        return new Promise(async (resolve, reject) => {
            await this.OnReady();
            this.Initializing = true;
            const results = await mongo_1.Mongo.FindAll(table_1.Table.aniBind);
            const list = await json_helper_1.JsonHelper.ArrayConvert(results, ani_bind_model_1.AniBind);
            if (list === undefined || list === null) {
                this.Initializing = false;
                console.log(`JsonHelper.ArrayConvert<AniBind>(results, AniBind) is 'null' or 'undefined'.`);
                resolve();
            }
            else {
                if (list.length === 0) {
                    this.Initializing = false;
                    console.log(`AniBind List Length: ${this.List.length}`);
                    resolve();
                }
                else {
                    this.List = list;
                    this.Initializing = false;
                    console.log(`AniBind List Length: ${this.List.length}`);
                    resolve();
                }
            }
        });
    }
    static Insert(discordId, anilistId, anilistUsername, code) {
        return new Promise(async (resolve, reject) => {
            await this.OnReady();
            const exists = await this.Exists(discordId);
            if (exists === false) {
                const data = {
                    anilist_id: anilistId,
                    discord_id: discordId,
                    anilist_username: anilistUsername,
                    code: code,
                    verified: false
                };
                const result = await mongo_1.Mongo.Insert(table_1.Table.aniBind, data);
                console.log(result.insertedId);
                const aniBind = new ani_bind_model_1.AniBind();
                aniBind.Id = result.insertedId;
                aniBind.AniListId = anilistId;
                aniBind.DiscordId = discordId;
                aniBind.AniListUsername = anilistUsername;
                aniBind.Code = code;
                aniBind.Verified = false;
                this.List.push(aniBind);
                resolve(aniBind);
            }
            else {
                resolve(this.All.find(x => x.DiscordId === discordId));
            }
        });
    }
    static get All() {
        return this.List;
    }
    static Verify(discordId, username) {
        return new Promise(async (resolve, reject) => {
            await this.OnReady();
            const query = { discord_id: discordId };
            const newValue = { $set: { verified: true } };
            await mongo_1.Mongo.Update(table_1.Table.aniBind, query, newValue);
            const oldValue = await this.Get(discordId, username);
            array_helper_1.ArrayHelper.remove(this.List, oldValue, async () => {
                const res = await mongo_1.Mongo.FindOne(table_1.Table.aniBind, query);
                const ms = await json_helper_1.JsonHelper.ArrayConvert(res, ani_bind_model_1.AniBind);
                const m = ms[0];
                console.log(`Update AniList bind: ${m.Code}`);
                if (m !== null && m !== undefined) {
                    this.List.push(m);
                    resolve(m);
                }
                else {
                    console.log(`JsonHelper.ArrayConvert<AniBind>(res, AniBind) is 'null' or 'undefined'.`);
                    resolve(null);
                }
            });
        });
    }
    static Exists(discordId) {
        return new Promise(async (resolve, reject) => {
            await this.OnReady();
            const aniBind = this.List.find(m => m.DiscordId === discordId);
            if (aniBind === null || aniBind === undefined) {
                resolve(false);
            }
            else {
                resolve(true);
            }
        });
    }
    static Get(discordId, username) {
        return new Promise(async (resolve, reject) => {
            await this.OnReady();
            if (this.List.length === 0) {
                console.log(`List is empty.`);
                resolve(null);
            }
            let aniBind = this.List.find(x => x.DiscordId === discordId);
            if (null_checker_helper_1.NullCheck.Fine(username)) {
                aniBind = this.List.find(x => x.DiscordId === discordId && x.AniListUsername === username);
            }
            if (null_checker_helper_1.NullCheck.Fine(aniBind)) {
                resolve(aniBind);
                return;
            }
            else {
                console.log(`this.List.find(m => m.DiscordId === discordId) is 'null' or 'undefined'.`);
                resolve(null);
            }
        });
    }
    static LogAll() {
        return new Promise(async (resolve, reject) => {
            await this.OnReady();
            if (this.List === null ||
                this.List === undefined ||
                this.List.length === 0) {
                reject(new Error(`this.List is 'null' or 'empty'.`));
            }
            else {
                console.log(this.List);
                resolve();
            }
        });
    }
    static OnReady() {
        return new Promise((resolve, reject) => {
            setInterval(() => {
                if (this.Initializing === false) {
                    resolve();
                }
            }, 1);
        });
    }
}
AniBindData.List = [];
AniBindData.Initializing = false;
exports.AniBindData = AniBindData;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5pLmJpbmQuZGF0YS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9kYXRhL2FuaS5iaW5kLmRhdGEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSx5Q0FBc0M7QUFDdEMseUNBQXNDO0FBQ3RDLHdEQUFvRDtBQUNwRCwwREFBc0Q7QUFDdEQsNkRBQW1EO0FBQ25ELHdFQUEyRDtBQUUzRCxNQUFhLFdBQVc7SUFJZixNQUFNLENBQUMsSUFBSTtRQUNoQixPQUFPLElBQUksT0FBTyxDQUFDLEtBQUssRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDM0MsTUFBTSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDckIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7WUFDekIsTUFBTSxPQUFPLEdBQUcsTUFBTSxhQUFLLENBQUMsT0FBTyxDQUFDLGFBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNuRCxNQUFNLElBQUksR0FBRyxNQUFNLHdCQUFVLENBQUMsWUFBWSxDQUFVLE9BQU8sRUFBRSx3QkFBTyxDQUFDLENBQUM7WUFDdEUsSUFBSSxJQUFJLEtBQUssU0FBUyxJQUFJLElBQUksS0FBSyxJQUFJLEVBQUU7Z0JBQ3ZDLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO2dCQUMxQixPQUFPLENBQUMsR0FBRyxDQUNULDhFQUE4RSxDQUMvRSxDQUFDO2dCQUNGLE9BQU8sRUFBRSxDQUFDO2FBQ1g7aUJBQU07Z0JBQ0wsSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtvQkFDckIsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7b0JBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztvQkFDeEQsT0FBTyxFQUFFLENBQUM7aUJBQ1g7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7b0JBQ2pCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO29CQUMxQixPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7b0JBQ3hELE9BQU8sRUFBRSxDQUFDO2lCQUNYO2FBQ0Y7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTSxNQUFNLENBQUMsTUFBTSxDQUNsQixTQUFpQixFQUNqQixTQUFpQixFQUNqQixlQUF1QixFQUN2QixJQUFZO1FBRVosT0FBTyxJQUFJLE9BQU8sQ0FBVSxLQUFLLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQ3BELE1BQU0sSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3JCLE1BQU0sTUFBTSxHQUFHLE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUM1QyxJQUFJLE1BQU0sS0FBSyxLQUFLLEVBQUU7Z0JBQ3BCLE1BQU0sSUFBSSxHQUFHO29CQUNYLFVBQVUsRUFBRSxTQUFTO29CQUNyQixVQUFVLEVBQUUsU0FBUztvQkFDckIsZ0JBQWdCLEVBQUUsZUFBZTtvQkFDakMsSUFBSSxFQUFFLElBQUk7b0JBQ1YsUUFBUSxFQUFFLEtBQUs7aUJBQ2hCLENBQUM7Z0JBQ0YsTUFBTSxNQUFNLEdBQUcsTUFBTSxhQUFLLENBQUMsTUFBTSxDQUFDLGFBQUssQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQ3ZELE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUMvQixNQUFNLE9BQU8sR0FBRyxJQUFJLHdCQUFPLEVBQUUsQ0FBQztnQkFDOUIsT0FBTyxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDO2dCQUMvQixPQUFPLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztnQkFDOUIsT0FBTyxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7Z0JBQzlCLE9BQU8sQ0FBQyxlQUFlLEdBQUcsZUFBZSxDQUFDO2dCQUMxQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztnQkFDcEIsT0FBTyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUN4QixPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDbEI7aUJBQU07Z0JBQ0wsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO2FBQ3hEO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0sTUFBTSxLQUFLLEdBQUc7UUFDbkIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO0lBQ25CLENBQUM7SUFFTSxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQWlCLEVBQUUsUUFBZ0I7UUFDdEQsT0FBTyxJQUFJLE9BQU8sQ0FBVSxLQUFLLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQ3BELE1BQU0sSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3JCLE1BQU0sS0FBSyxHQUFHLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxDQUFDO1lBQ3hDLE1BQU0sUUFBUSxHQUFHLEVBQUUsSUFBSSxFQUFFLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxFQUFFLENBQUM7WUFDOUMsTUFBTSxhQUFLLENBQUMsTUFBTSxDQUFDLGFBQUssQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQ25ELE1BQU0sUUFBUSxHQUFHLE1BQU0sSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDckQsMEJBQVcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsS0FBSyxJQUFJLEVBQUU7Z0JBQ2pELE1BQU0sR0FBRyxHQUFHLE1BQU0sYUFBSyxDQUFDLE9BQU8sQ0FBQyxhQUFLLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUN0RCxNQUFNLEVBQUUsR0FBRyxNQUFNLHdCQUFVLENBQUMsWUFBWSxDQUFVLEdBQUcsRUFBRSx3QkFBTyxDQUFDLENBQUM7Z0JBQ2hFLE1BQU0sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDaEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7Z0JBQzlDLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssU0FBUyxFQUFFO29CQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDbEIsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNaO3FCQUFNO29CQUNMLE9BQU8sQ0FBQyxHQUFHLENBQ1QsMEVBQTBFLENBQzNFLENBQUM7b0JBQ0YsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUNmO1lBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTSxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQWlCO1FBQ3BDLE9BQU8sSUFBSSxPQUFPLENBQVUsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNwRCxNQUFNLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUNyQixNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLEtBQUssU0FBUyxDQUFDLENBQUM7WUFDL0QsSUFBSSxPQUFPLEtBQUssSUFBSSxJQUFJLE9BQU8sS0FBSyxTQUFTLEVBQUU7Z0JBQzdDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNoQjtpQkFBTTtnQkFDTCxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDZjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVNLE1BQU0sQ0FBQyxHQUFHLENBQUMsU0FBaUIsRUFBRSxRQUFnQjtRQUNuRCxPQUFPLElBQUksT0FBTyxDQUFVLEtBQUssRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDcEQsTUFBTSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDckIsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7Z0JBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztnQkFDOUIsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2Y7WUFDRCxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLEtBQUssU0FBUyxDQUFDLENBQUM7WUFDN0QsSUFBSSwrQkFBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDNUIsT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUN0QixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLEtBQUssU0FBUyxJQUFJLENBQUMsQ0FBQyxlQUFlLEtBQUssUUFBUSxDQUNqRSxDQUFDO2FBQ0g7WUFDRCxJQUFJLCtCQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUMzQixPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ2pCLE9BQU87YUFDUjtpQkFBTTtnQkFDTCxPQUFPLENBQUMsR0FBRyxDQUNULDBFQUEwRSxDQUMzRSxDQUFDO2dCQUNGLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNmO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0sTUFBTSxDQUFDLE1BQU07UUFDbEIsT0FBTyxJQUFJLE9BQU8sQ0FBQyxLQUFLLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQzNDLE1BQU0sSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3JCLElBQ0UsSUFBSSxDQUFDLElBQUksS0FBSyxJQUFJO2dCQUNsQixJQUFJLENBQUMsSUFBSSxLQUFLLFNBQVM7Z0JBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxLQUFLLENBQUMsRUFDdEI7Z0JBQ0EsTUFBTSxDQUFDLElBQUksS0FBSyxDQUFDLGlDQUFpQyxDQUFDLENBQUMsQ0FBQzthQUN0RDtpQkFBTTtnQkFDTCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDdkIsT0FBTyxFQUFFLENBQUM7YUFDWDtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVNLE1BQU0sQ0FBQyxPQUFPO1FBQ25CLE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDckMsV0FBVyxDQUFDLEdBQUcsRUFBRTtnQkFDZixJQUFJLElBQUksQ0FBQyxZQUFZLEtBQUssS0FBSyxFQUFFO29CQUMvQixPQUFPLEVBQUUsQ0FBQztpQkFDWDtZQUNILENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNSLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7QUExSmEsZ0JBQUksR0FBYyxFQUFFLENBQUM7QUFDckIsd0JBQVksR0FBRyxLQUFLLENBQUM7QUFGckMsa0NBNEpDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTW9uZ28gfSBmcm9tIFwiLi4vY29yZS9tb25nb1wiO1xyXG5pbXBvcnQgeyBUYWJsZSB9IGZyb20gXCIuLi9jb3JlL3RhYmxlXCI7XHJcbmltcG9ydCB7IEpzb25IZWxwZXIgfSBmcm9tIFwiLi4vaGVscGVycy9qc29uLmhlbHBlclwiO1xyXG5pbXBvcnQgeyBBcnJheUhlbHBlciB9IGZyb20gXCIuLi9oZWxwZXJzL2FycmF5LmhlbHBlclwiO1xyXG5pbXBvcnQgeyBBbmlCaW5kIH0gZnJvbSBcIi4uL21vZGVscy9hbmkuYmluZC5tb2RlbFwiO1xyXG5pbXBvcnQgeyBOdWxsQ2hlY2sgfSBmcm9tIFwiLi4vaGVscGVycy9udWxsLmNoZWNrZXIuaGVscGVyXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgQW5pQmluZERhdGEge1xyXG4gIHB1YmxpYyBzdGF0aWMgTGlzdDogQW5pQmluZFtdID0gW107XHJcbiAgcHVibGljIHN0YXRpYyBJbml0aWFsaXppbmcgPSBmYWxzZTtcclxuXHJcbiAgcHVibGljIHN0YXRpYyBJbml0KCkge1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKGFzeW5jIChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgYXdhaXQgdGhpcy5PblJlYWR5KCk7XHJcbiAgICAgIHRoaXMuSW5pdGlhbGl6aW5nID0gdHJ1ZTtcclxuICAgICAgY29uc3QgcmVzdWx0cyA9IGF3YWl0IE1vbmdvLkZpbmRBbGwoVGFibGUuYW5pQmluZCk7XHJcbiAgICAgIGNvbnN0IGxpc3QgPSBhd2FpdCBKc29uSGVscGVyLkFycmF5Q29udmVydDxBbmlCaW5kPihyZXN1bHRzLCBBbmlCaW5kKTtcclxuICAgICAgaWYgKGxpc3QgPT09IHVuZGVmaW5lZCB8fCBsaXN0ID09PSBudWxsKSB7XHJcbiAgICAgICAgdGhpcy5Jbml0aWFsaXppbmcgPSBmYWxzZTtcclxuICAgICAgICBjb25zb2xlLmxvZyhcclxuICAgICAgICAgIGBKc29uSGVscGVyLkFycmF5Q29udmVydDxBbmlCaW5kPihyZXN1bHRzLCBBbmlCaW5kKSBpcyAnbnVsbCcgb3IgJ3VuZGVmaW5lZCcuYFxyXG4gICAgICAgICk7XHJcbiAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGlmIChsaXN0Lmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgdGhpcy5Jbml0aWFsaXppbmcgPSBmYWxzZTtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKGBBbmlCaW5kIExpc3QgTGVuZ3RoOiAke3RoaXMuTGlzdC5sZW5ndGh9YCk7XHJcbiAgICAgICAgICByZXNvbHZlKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMuTGlzdCA9IGxpc3Q7XHJcbiAgICAgICAgICB0aGlzLkluaXRpYWxpemluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgY29uc29sZS5sb2coYEFuaUJpbmQgTGlzdCBMZW5ndGg6ICR7dGhpcy5MaXN0Lmxlbmd0aH1gKTtcclxuICAgICAgICAgIHJlc29sdmUoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHN0YXRpYyBJbnNlcnQoXHJcbiAgICBkaXNjb3JkSWQ6IHN0cmluZyxcclxuICAgIGFuaWxpc3RJZDogbnVtYmVyLFxyXG4gICAgYW5pbGlzdFVzZXJuYW1lOiBzdHJpbmcsXHJcbiAgICBjb2RlOiBzdHJpbmdcclxuICApIHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZTxBbmlCaW5kPihhc3luYyAocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgIGF3YWl0IHRoaXMuT25SZWFkeSgpO1xyXG4gICAgICBjb25zdCBleGlzdHMgPSBhd2FpdCB0aGlzLkV4aXN0cyhkaXNjb3JkSWQpO1xyXG4gICAgICBpZiAoZXhpc3RzID09PSBmYWxzZSkge1xyXG4gICAgICAgIGNvbnN0IGRhdGEgPSB7XHJcbiAgICAgICAgICBhbmlsaXN0X2lkOiBhbmlsaXN0SWQsXHJcbiAgICAgICAgICBkaXNjb3JkX2lkOiBkaXNjb3JkSWQsXHJcbiAgICAgICAgICBhbmlsaXN0X3VzZXJuYW1lOiBhbmlsaXN0VXNlcm5hbWUsXHJcbiAgICAgICAgICBjb2RlOiBjb2RlLFxyXG4gICAgICAgICAgdmVyaWZpZWQ6IGZhbHNlXHJcbiAgICAgICAgfTtcclxuICAgICAgICBjb25zdCByZXN1bHQgPSBhd2FpdCBNb25nby5JbnNlcnQoVGFibGUuYW5pQmluZCwgZGF0YSk7XHJcbiAgICAgICAgY29uc29sZS5sb2cocmVzdWx0Lmluc2VydGVkSWQpO1xyXG4gICAgICAgIGNvbnN0IGFuaUJpbmQgPSBuZXcgQW5pQmluZCgpO1xyXG4gICAgICAgIGFuaUJpbmQuSWQgPSByZXN1bHQuaW5zZXJ0ZWRJZDtcclxuICAgICAgICBhbmlCaW5kLkFuaUxpc3RJZCA9IGFuaWxpc3RJZDtcclxuICAgICAgICBhbmlCaW5kLkRpc2NvcmRJZCA9IGRpc2NvcmRJZDtcclxuICAgICAgICBhbmlCaW5kLkFuaUxpc3RVc2VybmFtZSA9IGFuaWxpc3RVc2VybmFtZTtcclxuICAgICAgICBhbmlCaW5kLkNvZGUgPSBjb2RlO1xyXG4gICAgICAgIGFuaUJpbmQuVmVyaWZpZWQgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLkxpc3QucHVzaChhbmlCaW5kKTtcclxuICAgICAgICByZXNvbHZlKGFuaUJpbmQpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJlc29sdmUodGhpcy5BbGwuZmluZCh4ID0+IHguRGlzY29yZElkID09PSBkaXNjb3JkSWQpKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgc3RhdGljIGdldCBBbGwoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5MaXN0O1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHN0YXRpYyBWZXJpZnkoZGlzY29yZElkOiBzdHJpbmcsIHVzZXJuYW1lOiBzdHJpbmcpIHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZTxBbmlCaW5kPihhc3luYyAocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgIGF3YWl0IHRoaXMuT25SZWFkeSgpO1xyXG4gICAgICBjb25zdCBxdWVyeSA9IHsgZGlzY29yZF9pZDogZGlzY29yZElkIH07XHJcbiAgICAgIGNvbnN0IG5ld1ZhbHVlID0geyAkc2V0OiB7IHZlcmlmaWVkOiB0cnVlIH0gfTtcclxuICAgICAgYXdhaXQgTW9uZ28uVXBkYXRlKFRhYmxlLmFuaUJpbmQsIHF1ZXJ5LCBuZXdWYWx1ZSk7XHJcbiAgICAgIGNvbnN0IG9sZFZhbHVlID0gYXdhaXQgdGhpcy5HZXQoZGlzY29yZElkLCB1c2VybmFtZSk7XHJcbiAgICAgIEFycmF5SGVscGVyLnJlbW92ZSh0aGlzLkxpc3QsIG9sZFZhbHVlLCBhc3luYyAoKSA9PiB7XHJcbiAgICAgICAgY29uc3QgcmVzID0gYXdhaXQgTW9uZ28uRmluZE9uZShUYWJsZS5hbmlCaW5kLCBxdWVyeSk7XHJcbiAgICAgICAgY29uc3QgbXMgPSBhd2FpdCBKc29uSGVscGVyLkFycmF5Q29udmVydDxBbmlCaW5kPihyZXMsIEFuaUJpbmQpO1xyXG4gICAgICAgIGNvbnN0IG0gPSBtc1swXTtcclxuICAgICAgICBjb25zb2xlLmxvZyhgVXBkYXRlIEFuaUxpc3QgYmluZDogJHttLkNvZGV9YCk7XHJcbiAgICAgICAgaWYgKG0gIT09IG51bGwgJiYgbSAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICB0aGlzLkxpc3QucHVzaChtKTtcclxuICAgICAgICAgIHJlc29sdmUobSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKFxyXG4gICAgICAgICAgICBgSnNvbkhlbHBlci5BcnJheUNvbnZlcnQ8QW5pQmluZD4ocmVzLCBBbmlCaW5kKSBpcyAnbnVsbCcgb3IgJ3VuZGVmaW5lZCcuYFxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHJlc29sdmUobnVsbCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHN0YXRpYyBFeGlzdHMoZGlzY29yZElkOiBzdHJpbmcpIHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZTxib29sZWFuPihhc3luYyAocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgIGF3YWl0IHRoaXMuT25SZWFkeSgpO1xyXG4gICAgICBjb25zdCBhbmlCaW5kID0gdGhpcy5MaXN0LmZpbmQobSA9PiBtLkRpc2NvcmRJZCA9PT0gZGlzY29yZElkKTtcclxuICAgICAgaWYgKGFuaUJpbmQgPT09IG51bGwgfHwgYW5pQmluZCA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgcmVzb2x2ZShmYWxzZSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcmVzb2x2ZSh0cnVlKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgc3RhdGljIEdldChkaXNjb3JkSWQ6IHN0cmluZywgdXNlcm5hbWU6IHN0cmluZykge1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlPEFuaUJpbmQ+KGFzeW5jIChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgYXdhaXQgdGhpcy5PblJlYWR5KCk7XHJcbiAgICAgIGlmICh0aGlzLkxpc3QubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coYExpc3QgaXMgZW1wdHkuYCk7XHJcbiAgICAgICAgcmVzb2x2ZShudWxsKTtcclxuICAgICAgfVxyXG4gICAgICBsZXQgYW5pQmluZCA9IHRoaXMuTGlzdC5maW5kKHggPT4geC5EaXNjb3JkSWQgPT09IGRpc2NvcmRJZCk7XHJcbiAgICAgIGlmIChOdWxsQ2hlY2suRmluZSh1c2VybmFtZSkpIHtcclxuICAgICAgICBhbmlCaW5kID0gdGhpcy5MaXN0LmZpbmQoXHJcbiAgICAgICAgICB4ID0+IHguRGlzY29yZElkID09PSBkaXNjb3JkSWQgJiYgeC5BbmlMaXN0VXNlcm5hbWUgPT09IHVzZXJuYW1lXHJcbiAgICAgICAgKTtcclxuICAgICAgfVxyXG4gICAgICBpZiAoTnVsbENoZWNrLkZpbmUoYW5pQmluZCkpIHtcclxuICAgICAgICByZXNvbHZlKGFuaUJpbmQpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcclxuICAgICAgICAgIGB0aGlzLkxpc3QuZmluZChtID0+IG0uRGlzY29yZElkID09PSBkaXNjb3JkSWQpIGlzICdudWxsJyBvciAndW5kZWZpbmVkJy5gXHJcbiAgICAgICAgKTtcclxuICAgICAgICByZXNvbHZlKG51bGwpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBzdGF0aWMgTG9nQWxsKCkge1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKGFzeW5jIChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgYXdhaXQgdGhpcy5PblJlYWR5KCk7XHJcbiAgICAgIGlmIChcclxuICAgICAgICB0aGlzLkxpc3QgPT09IG51bGwgfHxcclxuICAgICAgICB0aGlzLkxpc3QgPT09IHVuZGVmaW5lZCB8fFxyXG4gICAgICAgIHRoaXMuTGlzdC5sZW5ndGggPT09IDBcclxuICAgICAgKSB7XHJcbiAgICAgICAgcmVqZWN0KG5ldyBFcnJvcihgdGhpcy5MaXN0IGlzICdudWxsJyBvciAnZW1wdHknLmApKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLkxpc3QpO1xyXG4gICAgICAgIHJlc29sdmUoKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgc3RhdGljIE9uUmVhZHkoKSB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICBzZXRJbnRlcnZhbCgoKSA9PiB7XHJcbiAgICAgICAgaWYgKHRoaXMuSW5pdGlhbGl6aW5nID09PSBmYWxzZSkge1xyXG4gICAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSwgMSk7XHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuIl19