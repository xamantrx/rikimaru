"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
require("class-transformer");
const rescue_center_1 = require("../core/rescue.center");
const manager_command_1 = require("../command/manager.command");
const sender_1 = require("./../core/sender");
const cooldown_model_1 = require("../models/cooldown.model");
const null_checker_helper_1 = require("../helpers/null.checker.helper");
class ResponseHandler {
    static async Get(message, command) {
        const cmd = await manager_command_1.CommandManager.Validate(command);
        if (null_checker_helper_1.NullCheck.Fine(cmd)) {
            const cooldown = await cooldown_model_1.Cooldown.Get(cmd, message.member.user);
            await cooldown
                .Register(message)
                .then(() => {
                const parameter = command.Parameter;
                const paramRequired = cmd.ParameterRequired;
                if (cmd.CanHaveMention && null_checker_helper_1.NullCheck.Fine(message.mentions)) {
                    cmd.Function.Execute(message, command, cmd.DirectMessage);
                    return;
                }
                else if (!null_checker_helper_1.NullCheck.Fine(parameter) &&
                    paramRequired) {
                    this.SendRescue(message, cmd.DirectMessage, cmd, command);
                    return;
                }
                else if (null_checker_helper_1.NullCheck.Fine(parameter) &&
                    !paramRequired) {
                    this.SendRescue(message, cmd.DirectMessage, cmd, command);
                    return;
                }
                else {
                    if (cmd.Function !== null) {
                        if (cmd.DevOnly === true &&
                            message.author.id === "442621672714010625") {
                            cmd.Function.Execute(message, command, cmd.DirectMessage);
                            return;
                        }
                        cmd.Function.Execute(message, command, cmd.DirectMessage);
                        return;
                    }
                }
            })
                .catch((response) => {
                message.channel.send(response.content).then(($m) => {
                    cooldown.Respond($m).then(() => {
                        if (message.deletable) {
                            message.delete();
                        }
                        setTimeout(() => {
                            $m.delete();
                        }, response.timeout);
                    });
                });
            });
        }
    }
    static SendRescue(message, dm, botCommand, command) {
        rescue_center_1.RescueCenter.RequireParameter(message, botCommand, command).then(embed => {
            sender_1.Sender.SendInfo(message, embed, dm);
        });
    }
}
exports.ResponseHandler = ResponseHandler;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzcG9uc2UuaGFuZGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9oYW5kbGVycy9yZXNwb25zZS5oYW5kbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsNEJBQTBCO0FBRTFCLDZCQUEyQjtBQUczQix5REFBcUQ7QUFDckQsZ0VBQTREO0FBQzVELDZDQUEwQztBQUMxQyw2REFBc0U7QUFDdEUsd0VBQTJEO0FBRTNELE1BQWEsZUFBZTtJQUNuQixNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxPQUFnQixFQUFFLE9BQWlCO1FBQ3pELE1BQU0sR0FBRyxHQUFHLE1BQU0sZ0NBQWMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDbkQsSUFBSSwrQkFBUyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUN2QixNQUFNLFFBQVEsR0FBRyxNQUFNLHlCQUFRLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzlELE1BQU0sUUFBUTtpQkFDWCxRQUFRLENBQUMsT0FBTyxDQUFDO2lCQUNqQixJQUFJLENBQUMsR0FBRyxFQUFFO2dCQUNULE1BQU0sU0FBUyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUM7Z0JBQ3BDLE1BQU0sYUFBYSxHQUFHLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQztnQkFDNUMsSUFDRSxHQUFHLENBQUMsY0FBYyxJQUFJLCtCQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFDdEQ7b0JBQ0EsR0FBRyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLE9BQU8sRUFBRSxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUM7b0JBQzFELE9BQU87aUJBQ1I7cUJBQU0sSUFDTCxDQUFDLCtCQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztvQkFDMUIsYUFBYSxFQUNiO29CQUNBLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLEdBQUcsQ0FBQyxhQUFhLEVBQUUsR0FBRyxFQUFFLE9BQU8sQ0FBQyxDQUFDO29CQUMxRCxPQUFPO2lCQUNSO3FCQUFNLElBQ0wsK0JBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO29CQUN6QixDQUFDLGFBQWEsRUFDZDtvQkFDQSxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsYUFBYSxFQUFFLEdBQUcsRUFBRSxPQUFPLENBQUMsQ0FBQztvQkFDMUQsT0FBTztpQkFDUjtxQkFBTTtvQkFDTCxJQUFJLEdBQUcsQ0FBQyxRQUFRLEtBQUssSUFBSSxFQUFFO3dCQUN6QixJQUNFLEdBQUcsQ0FBQyxPQUFPLEtBQUssSUFBSTs0QkFDcEIsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEtBQUssb0JBQW9CLEVBQzFDOzRCQUNBLEdBQUcsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUUsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDOzRCQUMxRCxPQUFPO3lCQUNSO3dCQUNELEdBQUcsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUUsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDO3dCQUMxRCxPQUFPO3FCQUNSO2lCQUNGO1lBQ0gsQ0FBQyxDQUFDO2lCQUNELEtBQUssQ0FBQyxDQUFDLFFBQTBCLEVBQUUsRUFBRTtnQkFDcEMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQVcsRUFBRSxFQUFFO29CQUMxRCxRQUFRLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUU7d0JBQzdCLElBQUksT0FBTyxDQUFDLFNBQVMsRUFBRTs0QkFDckIsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDO3lCQUNsQjt3QkFDRCxVQUFVLENBQUMsR0FBRyxFQUFFOzRCQUNkLEVBQUUsQ0FBQyxNQUFNLEVBQUUsQ0FBQzt3QkFDZCxDQUFDLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUN2QixDQUFDLENBQUMsQ0FBQztnQkFDTCxDQUFDLENBQUMsQ0FBQztZQUNMLENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDSCxDQUFDO0lBRU8sTUFBTSxDQUFDLFVBQVUsQ0FDdkIsT0FBZ0IsRUFDaEIsRUFBVyxFQUNYLFVBQXNCLEVBQ3RCLE9BQWlCO1FBRWpCLDRCQUFZLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDdkUsZUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ3RDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztDQUNGO0FBbEVELDBDQWtFQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBcInJlZmxlY3QtbWV0YWRhdGFcIjtcbmltcG9ydCB7IEJvdENvbW1hbmQgfSBmcm9tIFwiLi8uLi9jb21tYW5kL2JvdC5jb21tYW5kXCI7XG5pbXBvcnQgXCJjbGFzcy10cmFuc2Zvcm1lclwiO1xuaW1wb3J0IHsgSUNvbW1hbmQgfSBmcm9tIFwiLi4vaW50ZXJmYWNlcy9jb21tYW5kLmludGVyZmFjZVwiO1xuaW1wb3J0IHsgTWVzc2FnZSB9IGZyb20gXCJkaXNjb3JkLmpzXCI7XG5pbXBvcnQgeyBSZXNjdWVDZW50ZXIgfSBmcm9tIFwiLi4vY29yZS9yZXNjdWUuY2VudGVyXCI7XG5pbXBvcnQgeyBDb21tYW5kTWFuYWdlciB9IGZyb20gXCIuLi9jb21tYW5kL21hbmFnZXIuY29tbWFuZFwiO1xuaW1wb3J0IHsgU2VuZGVyIH0gZnJvbSBcIi4vLi4vY29yZS9zZW5kZXJcIjtcbmltcG9ydCB7IENvb2xkb3duLCBDb29sZG93blJlc3BvbnNlIH0gZnJvbSBcIi4uL21vZGVscy9jb29sZG93bi5tb2RlbFwiO1xuaW1wb3J0IHsgTnVsbENoZWNrIH0gZnJvbSBcIi4uL2hlbHBlcnMvbnVsbC5jaGVja2VyLmhlbHBlclwiO1xuXG5leHBvcnQgY2xhc3MgUmVzcG9uc2VIYW5kbGVyIHtcbiAgcHVibGljIHN0YXRpYyBhc3luYyBHZXQobWVzc2FnZTogTWVzc2FnZSwgY29tbWFuZDogSUNvbW1hbmQpIHtcbiAgICBjb25zdCBjbWQgPSBhd2FpdCBDb21tYW5kTWFuYWdlci5WYWxpZGF0ZShjb21tYW5kKTtcbiAgICBpZiAoTnVsbENoZWNrLkZpbmUoY21kKSkge1xuICAgICAgY29uc3QgY29vbGRvd24gPSBhd2FpdCBDb29sZG93bi5HZXQoY21kLCBtZXNzYWdlLm1lbWJlci51c2VyKTtcbiAgICAgIGF3YWl0IGNvb2xkb3duXG4gICAgICAgIC5SZWdpc3RlcihtZXNzYWdlKVxuICAgICAgICAudGhlbigoKSA9PiB7XG4gICAgICAgICAgY29uc3QgcGFyYW1ldGVyID0gY29tbWFuZC5QYXJhbWV0ZXI7XG4gICAgICAgICAgY29uc3QgcGFyYW1SZXF1aXJlZCA9IGNtZC5QYXJhbWV0ZXJSZXF1aXJlZDtcbiAgICAgICAgICBpZiAoXG4gICAgICAgICAgICBjbWQuQ2FuSGF2ZU1lbnRpb24gJiYgTnVsbENoZWNrLkZpbmUobWVzc2FnZS5tZW50aW9ucylcbiAgICAgICAgICApIHtcbiAgICAgICAgICAgIGNtZC5GdW5jdGlvbi5FeGVjdXRlKG1lc3NhZ2UsIGNvbW1hbmQsIGNtZC5EaXJlY3RNZXNzYWdlKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICB9IGVsc2UgaWYgKFxuICAgICAgICAgICAgIU51bGxDaGVjay5GaW5lKHBhcmFtZXRlcikgJiZcbiAgICAgICAgICAgIHBhcmFtUmVxdWlyZWRcbiAgICAgICAgICApIHtcbiAgICAgICAgICAgIHRoaXMuU2VuZFJlc2N1ZShtZXNzYWdlLCBjbWQuRGlyZWN0TWVzc2FnZSwgY21kLCBjb21tYW5kKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICB9IGVsc2UgaWYgKFxuICAgICAgICAgICAgTnVsbENoZWNrLkZpbmUocGFyYW1ldGVyKSAmJlxuICAgICAgICAgICAgIXBhcmFtUmVxdWlyZWRcbiAgICAgICAgICApIHtcbiAgICAgICAgICAgIHRoaXMuU2VuZFJlc2N1ZShtZXNzYWdlLCBjbWQuRGlyZWN0TWVzc2FnZSwgY21kLCBjb21tYW5kKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaWYgKGNtZC5GdW5jdGlvbiAhPT0gbnVsbCkge1xuICAgICAgICAgICAgICBpZiAoXG4gICAgICAgICAgICAgICAgY21kLkRldk9ubHkgPT09IHRydWUgJiZcbiAgICAgICAgICAgICAgICBtZXNzYWdlLmF1dGhvci5pZCA9PT0gXCI0NDI2MjE2NzI3MTQwMTA2MjVcIlxuICAgICAgICAgICAgICApIHtcbiAgICAgICAgICAgICAgICBjbWQuRnVuY3Rpb24uRXhlY3V0ZShtZXNzYWdlLCBjb21tYW5kLCBjbWQuRGlyZWN0TWVzc2FnZSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGNtZC5GdW5jdGlvbi5FeGVjdXRlKG1lc3NhZ2UsIGNvbW1hbmQsIGNtZC5EaXJlY3RNZXNzYWdlKTtcbiAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSlcbiAgICAgICAgLmNhdGNoKChyZXNwb25zZTogQ29vbGRvd25SZXNwb25zZSkgPT4ge1xuICAgICAgICAgIG1lc3NhZ2UuY2hhbm5lbC5zZW5kKHJlc3BvbnNlLmNvbnRlbnQpLnRoZW4oKCRtOiBNZXNzYWdlKSA9PiB7XG4gICAgICAgICAgICBjb29sZG93bi5SZXNwb25kKCRtKS50aGVuKCgpID0+IHtcbiAgICAgICAgICAgICAgaWYgKG1lc3NhZ2UuZGVsZXRhYmxlKSB7XG4gICAgICAgICAgICAgICAgbWVzc2FnZS5kZWxldGUoKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgICAkbS5kZWxldGUoKTtcbiAgICAgICAgICAgICAgfSwgcmVzcG9uc2UudGltZW91dCk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBzdGF0aWMgU2VuZFJlc2N1ZShcbiAgICBtZXNzYWdlOiBNZXNzYWdlLFxuICAgIGRtOiBib29sZWFuLFxuICAgIGJvdENvbW1hbmQ6IEJvdENvbW1hbmQsXG4gICAgY29tbWFuZDogSUNvbW1hbmRcbiAgKSB7XG4gICAgUmVzY3VlQ2VudGVyLlJlcXVpcmVQYXJhbWV0ZXIobWVzc2FnZSwgYm90Q29tbWFuZCwgY29tbWFuZCkudGhlbihlbWJlZCA9PiB7XG4gICAgICBTZW5kZXIuU2VuZEluZm8obWVzc2FnZSwgZW1iZWQsIGRtKTtcbiAgICB9KTtcbiAgfVxufVxuIl19