"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const message_helper_1 = require("../helpers/message.helper");
const command_model_1 = require("../models/command.model");
const client_1 = require("../core/client");
const config_1 = require("../core/config");
const response_handler_1 = require("./response.handler");
const sender_1 = require("../core/sender");
class MessageHandler {
    static async Init() {
        const client = await client_1.ClientManager.Client;
        client.on("message", message => {
            if (message.author.id !== client.user.id) {
                const isCommand = message_helper_1.MessageHelper.IsCommand(config_1.Config, message);
                const cmdName = isCommand
                    ? message_helper_1.MessageHelper.GetCommand(config_1.Config, message).trim()
                    : "";
                const parameter = message_helper_1.MessageHelper.GetParameter(config_1.Config, message).trim();
                if (isCommand && message_helper_1.MessageHelper.IsDMChannel(message)) {
                    sender_1.Sender.Send(message, `Go me nasai!, I can't process commands in DM.`, true);
                    return;
                }
                if (isCommand) {
                    const command = new command_model_1.Command(cmdName, parameter.length > 0 ? parameter : null);
                    console.log(command);
                    response_handler_1.ResponseHandler.Get(message, command);
                }
            }
        });
    }
}
exports.MessageHandler = MessageHandler;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVzc2FnZS5oYW5kbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2hhbmRsZXJzL21lc3NhZ2UuaGFuZGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDhEQUEwRDtBQUMxRCwyREFBa0Q7QUFDbEQsMkNBQStDO0FBQy9DLDJDQUF3QztBQUN4Qyx5REFBcUQ7QUFDckQsMkNBQXdDO0FBRXhDLE1BQWEsY0FBYztJQUNsQixNQUFNLENBQUMsS0FBSyxDQUFDLElBQUk7UUFDdEIsTUFBTSxNQUFNLEdBQUcsTUFBTSxzQkFBYSxDQUFDLE1BQU0sQ0FBQztRQUMxQyxNQUFNLENBQUMsRUFBRSxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsRUFBRTtZQUM3QixJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRSxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFO2dCQUN4QyxNQUFNLFNBQVMsR0FBRyw4QkFBYSxDQUFDLFNBQVMsQ0FBQyxlQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBQzNELE1BQU0sT0FBTyxHQUFHLFNBQVM7b0JBQ3ZCLENBQUMsQ0FBQyw4QkFBYSxDQUFDLFVBQVUsQ0FBQyxlQUFNLEVBQUUsT0FBTyxDQUFDLENBQUMsSUFBSSxFQUFFO29CQUNsRCxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUNQLE1BQU0sU0FBUyxHQUFHLDhCQUFhLENBQUMsWUFBWSxDQUFDLGVBQU0sRUFBRSxPQUFPLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDckUsSUFBSSxTQUFTLElBQUksOEJBQWEsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLEVBQUU7b0JBQ25ELGVBQU0sQ0FBQyxJQUFJLENBQ1QsT0FBTyxFQUNQLCtDQUErQyxFQUMvQyxJQUFJLENBQ0wsQ0FBQztvQkFDRixPQUFPO2lCQUNSO2dCQUNELElBQUksU0FBUyxFQUFFO29CQUNiLE1BQU0sT0FBTyxHQUFHLElBQUksdUJBQU8sQ0FDekIsT0FBTyxFQUNQLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FDeEMsQ0FBQztvQkFDRixPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUNyQixrQ0FBZSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUM7aUJBQ3ZDO2FBQ0Y7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7Q0FDRjtBQTdCRCx3Q0E2QkMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNZXNzYWdlSGVscGVyIH0gZnJvbSBcIi4uL2hlbHBlcnMvbWVzc2FnZS5oZWxwZXJcIjtcbmltcG9ydCB7IENvbW1hbmQgfSBmcm9tIFwiLi4vbW9kZWxzL2NvbW1hbmQubW9kZWxcIjtcbmltcG9ydCB7IENsaWVudE1hbmFnZXIgfSBmcm9tIFwiLi4vY29yZS9jbGllbnRcIjtcbmltcG9ydCB7IENvbmZpZyB9IGZyb20gXCIuLi9jb3JlL2NvbmZpZ1wiO1xuaW1wb3J0IHsgUmVzcG9uc2VIYW5kbGVyIH0gZnJvbSBcIi4vcmVzcG9uc2UuaGFuZGxlclwiO1xuaW1wb3J0IHsgU2VuZGVyIH0gZnJvbSBcIi4uL2NvcmUvc2VuZGVyXCI7XG5cbmV4cG9ydCBjbGFzcyBNZXNzYWdlSGFuZGxlciB7XG4gIHB1YmxpYyBzdGF0aWMgYXN5bmMgSW5pdCgpIHtcbiAgICBjb25zdCBjbGllbnQgPSBhd2FpdCBDbGllbnRNYW5hZ2VyLkNsaWVudDtcbiAgICBjbGllbnQub24oXCJtZXNzYWdlXCIsIG1lc3NhZ2UgPT4ge1xuICAgICAgaWYgKG1lc3NhZ2UuYXV0aG9yLmlkICE9PSBjbGllbnQudXNlci5pZCkge1xuICAgICAgICBjb25zdCBpc0NvbW1hbmQgPSBNZXNzYWdlSGVscGVyLklzQ29tbWFuZChDb25maWcsIG1lc3NhZ2UpO1xuICAgICAgICBjb25zdCBjbWROYW1lID0gaXNDb21tYW5kXG4gICAgICAgICAgPyBNZXNzYWdlSGVscGVyLkdldENvbW1hbmQoQ29uZmlnLCBtZXNzYWdlKS50cmltKClcbiAgICAgICAgICA6IFwiXCI7XG4gICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IE1lc3NhZ2VIZWxwZXIuR2V0UGFyYW1ldGVyKENvbmZpZywgbWVzc2FnZSkudHJpbSgpO1xuICAgICAgICBpZiAoaXNDb21tYW5kICYmIE1lc3NhZ2VIZWxwZXIuSXNETUNoYW5uZWwobWVzc2FnZSkpIHtcbiAgICAgICAgICBTZW5kZXIuU2VuZChcbiAgICAgICAgICAgIG1lc3NhZ2UsXG4gICAgICAgICAgICBgR28gbWUgbmFzYWkhLCBJIGNhbid0IHByb2Nlc3MgY29tbWFuZHMgaW4gRE0uYCxcbiAgICAgICAgICAgIHRydWVcbiAgICAgICAgICApO1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBpZiAoaXNDb21tYW5kKSB7XG4gICAgICAgICAgY29uc3QgY29tbWFuZCA9IG5ldyBDb21tYW5kKFxuICAgICAgICAgICAgY21kTmFtZSxcbiAgICAgICAgICAgIHBhcmFtZXRlci5sZW5ndGggPiAwID8gcGFyYW1ldGVyIDogbnVsbFxuICAgICAgICAgICk7XG4gICAgICAgICAgY29uc29sZS5sb2coY29tbWFuZCk7XG4gICAgICAgICAgUmVzcG9uc2VIYW5kbGVyLkdldChtZXNzYWdlLCBjb21tYW5kKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pO1xuICB9XG59XG4iXX0=