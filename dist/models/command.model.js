"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const null_checker_helper_1 = require("../helpers/null.checker.helper");
class Command {
    constructor(name, parameter) {
        this.Parameter = null;
        this.Name = name.trim();
        if (null_checker_helper_1.NullCheck.Fine(parameter)) {
            this.Parameter = parameter.trim();
        }
    }
}
exports.Command = Command;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWFuZC5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9tb2RlbHMvY29tbWFuZC5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHdFQUEyRDtBQUUzRCxNQUFhLE9BQU87SUFHbEIsWUFBWSxJQUFZLEVBQUUsU0FBaUI7UUFEcEMsY0FBUyxHQUFXLElBQUksQ0FBQztRQUU5QixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUN4QixJQUFJLCtCQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ25DO0lBQ0gsQ0FBQztDQUNGO0FBVEQsMEJBU0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJQ29tbWFuZCB9IGZyb20gXCIuLi9pbnRlcmZhY2VzL2NvbW1hbmQuaW50ZXJmYWNlXCI7XG5pbXBvcnQgeyBOdWxsQ2hlY2sgfSBmcm9tIFwiLi4vaGVscGVycy9udWxsLmNoZWNrZXIuaGVscGVyXCI7XG5cbmV4cG9ydCBjbGFzcyBDb21tYW5kIGltcGxlbWVudHMgSUNvbW1hbmQge1xuICBwdWJsaWMgTmFtZTogc3RyaW5nO1xuICBwdWJsaWMgUGFyYW1ldGVyOiBzdHJpbmcgPSBudWxsO1xuICBjb25zdHJ1Y3RvcihuYW1lOiBzdHJpbmcsIHBhcmFtZXRlcjogc3RyaW5nKSB7XG4gICAgdGhpcy5OYW1lID0gbmFtZS50cmltKCk7XG4gICAgaWYgKE51bGxDaGVjay5GaW5lKHBhcmFtZXRlcikpIHtcbiAgICAgIHRoaXMuUGFyYW1ldGVyID0gcGFyYW1ldGVyLnRyaW0oKTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==