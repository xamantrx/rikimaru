"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const queue_data_1 = require("./../data/queue.data");
const moment_1 = require("moment");
const client_1 = require("../core/client");
const title_helper_1 = require("../helpers/title.helper");
const colors_1 = require("../core/colors");
const subscription_data_1 = require("../data/subscription.data");
const media_status_1 = require("../core/media.status");
const anime_cache_1 = require("../core/anime.cache");
const null_checker_helper_1 = require("../helpers/null.checker.helper");
const config_1 = require("../core/config");
class QueueJob {
    constructor(media, queue) {
        this.media = media;
        this.queue = queue;
    }
    async Check() {
        return new Promise(async (resolve, reject) => {
            const queueEpisode = this.queue.NextEpisode;
            const media = this.media;
            const title = title_helper_1.TitleHelper.Get(media.title);
            this.JobDate = moment_1.unix(media.nextAiringEpisode.airingAt).toDate();
            if (media_status_1.MediaStatus.Completed(media) && media.episodes === 1) {
                await this.FindUser(title, queueEpisode, media);
                resolve();
            }
            else if (queueEpisode < media.nextAiringEpisode.next) {
                await this.FindUser(title, queueEpisode, media);
                resolve();
            }
        });
    }
    FindUser(title, nextEpisode, media) {
        return new Promise(async (resolve, reject) => {
            console.log(`Getting subscribers of "${title}"`);
            const subscribers = await subscription_data_1.SubscriptionData.GetSubscribers(this.media.idMal);
            if (subscribers.length === 0) {
                this.Update();
                resolve();
            }
            for (let i = 0; i < subscribers.length; i++) {
                const subscriber = subscribers[i];
                const user = await client_1.ClientManager.Client.fetchUser(subscriber.DiscordId);
                if (null_checker_helper_1.NullCheck.Fine(user)) {
                    await this.SendMessage(title, nextEpisode, media, user);
                    if (i === subscribers.length - 1) {
                        await this.Update();
                        resolve();
                    }
                }
                else {
                    if (i === subscribers.length - 1) {
                        await this.Update();
                        resolve();
                    }
                }
            }
        });
    }
    SendMessage(title, nextEpisode, media, user) {
        return new Promise(async (resolve, reject) => {
            const embed = await this.EmbedTemplate(media, nextEpisode);
            await user
                .send(embed)
                .then(async () => {
                console.log(`DM has been sent to "${user.username}" for "${title} Episode ${nextEpisode}"`);
                // await this.Sleep(1000);
                // const support = await this.SupportTemplate();
                // await user.send(support).catch(err => {
                //   console.log(err);
                // });
                resolve();
            })
                .catch(err => {
                console.log(err);
                resolve();
            });
        });
    }
    Update() {
        return new Promise(async (resolve, reject) => {
            const media = await anime_cache_1.AnimeCache.Get(this.media.idMal);
            if (null_checker_helper_1.NullCheck.Fine(media)) {
                await queue_data_1.QueueData.Update(media, this);
                console.log(`Removed Queue: ${media.idMal}`);
                resolve();
            }
            else {
                console.warn(`Error while searching : [MediaSearch.Find(${this.media.idMal})]. Trying again...`);
                await this.Update();
                resolve();
            }
        });
    }
    EmbedTemplate(media, episode) {
        return new Promise(async (resolve, reject) => {
            const prefix = config_1.Config.COMMAND_PREFIX;
            const client = client_1.ClientManager.Client;
            const t = title_helper_1.TitleHelper.Get(media.title);
            let episodes = `?`;
            if (null_checker_helper_1.NullCheck.Fine(media.episodes)) {
                episodes = `${media.episodes}`;
            }
            const embed = {
                embed: {
                    color: colors_1.Color.Random,
                    thumbnail: {
                        url: media.coverImage.large
                    },
                    title: `${t}`,
                    url: `${config_1.Config.MAL_ANIME_BASE}/${media.idMal}/`,
                    description: `Episode ***${episode}***/${episodes} is now airing.`,
                    fields: [
                        {
                            name: `Links:`,
                            value: `[MyAnimeList](${config_1.Config.MAL_ANIME_BASE}/${media.idMal}/)  |  [AniList](${config_1.Config.ANILIST_ANIME_BASE}/${media.id}/)`
                        }
                    ],
                    timestamp: new Date(),
                    footer: {
                        icon_url: client.user.avatarURL,
                        text: `© ${config_1.Config.BOT_NAME}`
                    }
                }
            };
            resolve(embed);
        });
    }
}
exports.QueueJob = QueueJob;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicXVldWUuam9iLm1vZGVsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL21vZGVscy9xdWV1ZS5qb2IubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFDQSxxREFBaUQ7QUFDakQsbUNBQXNDO0FBQ3RDLDJDQUErQztBQUUvQywwREFBc0Q7QUFFdEQsMkNBQXVDO0FBQ3ZDLGlFQUE2RDtBQUU3RCx1REFBbUQ7QUFDbkQscURBQWlEO0FBQ2pELHdFQUEyRDtBQUMzRCwyQ0FBd0M7QUFFeEMsTUFBYSxRQUFRO0lBRW5CLFlBQW1CLEtBQWEsRUFBUyxLQUFZO1FBQWxDLFVBQUssR0FBTCxLQUFLLENBQVE7UUFBUyxVQUFLLEdBQUwsS0FBSyxDQUFPO0lBQUcsQ0FBQztJQUVsRCxLQUFLLENBQUMsS0FBSztRQUNoQixPQUFPLElBQUksT0FBTyxDQUFDLEtBQUssRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDM0MsTUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUM7WUFDNUMsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztZQUN6QixNQUFNLEtBQUssR0FBRywwQkFBVyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDM0MsSUFBSSxDQUFDLE9BQU8sR0FBRyxhQUFJLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQy9ELElBQUksMEJBQVcsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLFFBQVEsS0FBSyxDQUFDLEVBQUU7Z0JBQ3hELE1BQU0sSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsWUFBWSxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUNoRCxPQUFPLEVBQUUsQ0FBQzthQUNYO2lCQUFNLElBQUksWUFBWSxHQUFHLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUU7Z0JBQ3RELE1BQU0sSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsWUFBWSxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUNoRCxPQUFPLEVBQUUsQ0FBQzthQUNYO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU8sUUFBUSxDQUFDLEtBQWEsRUFBRSxXQUFtQixFQUFFLEtBQWE7UUFDaEUsT0FBTyxJQUFJLE9BQU8sQ0FBQyxLQUFLLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQzNDLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkJBQTJCLEtBQUssR0FBRyxDQUFDLENBQUM7WUFDakQsTUFBTSxXQUFXLEdBQUcsTUFBTSxvQ0FBZ0IsQ0FBQyxjQUFjLENBQ3ZELElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUNqQixDQUFDO1lBQ0YsSUFBSSxXQUFXLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtnQkFDNUIsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUNkLE9BQU8sRUFBRSxDQUFDO2FBQ1g7WUFDRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDM0MsTUFBTSxVQUFVLEdBQUcsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNsQyxNQUFNLElBQUksR0FBRyxNQUFNLHNCQUFhLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ3hFLElBQUksK0JBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQ3hCLE1BQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztvQkFDeEQsSUFBSSxDQUFDLEtBQUssV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBQ2hDLE1BQU0sSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO3dCQUNwQixPQUFPLEVBQUUsQ0FBQztxQkFDWDtpQkFDRjtxQkFBTTtvQkFDTCxJQUFJLENBQUMsS0FBSyxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3QkFDaEMsTUFBTSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7d0JBQ3BCLE9BQU8sRUFBRSxDQUFDO3FCQUNYO2lCQUNGO2FBQ0Y7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTyxXQUFXLENBQ2pCLEtBQWEsRUFDYixXQUFtQixFQUNuQixLQUFhLEVBQ2IsSUFBVTtRQUVWLE9BQU8sSUFBSSxPQUFPLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUMzQyxNQUFNLEtBQUssR0FBRyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLFdBQVcsQ0FBQyxDQUFDO1lBQzNELE1BQU0sSUFBSTtpQkFDUCxJQUFJLENBQUMsS0FBSyxDQUFDO2lCQUNYLElBQUksQ0FBQyxLQUFLLElBQUksRUFBRTtnQkFDZixPQUFPLENBQUMsR0FBRyxDQUNULHdCQUNFLElBQUksQ0FBQyxRQUNQLFVBQVUsS0FBSyxZQUFZLFdBQVcsR0FBRyxDQUMxQyxDQUFDO2dCQUNGLDBCQUEwQjtnQkFDMUIsZ0RBQWdEO2dCQUNoRCwwQ0FBMEM7Z0JBQzFDLHNCQUFzQjtnQkFDdEIsTUFBTTtnQkFDTixPQUFPLEVBQUUsQ0FBQztZQUNaLENBQUMsQ0FBQztpQkFDRCxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQ1gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDakIsT0FBTyxFQUFFLENBQUM7WUFDWixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLE1BQU07UUFDWixPQUFPLElBQUksT0FBTyxDQUFDLEtBQUssRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDM0MsTUFBTSxLQUFLLEdBQUcsTUFBTSx3QkFBVSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3JELElBQUksK0JBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ3pCLE1BQU0sc0JBQVMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUNwQyxPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixLQUFLLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztnQkFDN0MsT0FBTyxFQUFFLENBQUM7YUFDWDtpQkFBTTtnQkFDTCxPQUFPLENBQUMsSUFBSSxDQUNWLDZDQUNFLElBQUksQ0FBQyxLQUFLLENBQUMsS0FDYixxQkFBcUIsQ0FDdEIsQ0FBQztnQkFDRixNQUFNLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDcEIsT0FBTyxFQUFFLENBQUM7YUFDWDtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLGFBQWEsQ0FBQyxLQUFhLEVBQUUsT0FBZTtRQUNsRCxPQUFPLElBQUksT0FBTyxDQUFNLEtBQUssRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDaEQsTUFBTSxNQUFNLEdBQUcsZUFBTSxDQUFDLGNBQWMsQ0FBQztZQUNyQyxNQUFNLE1BQU0sR0FBRyxzQkFBYSxDQUFDLE1BQU0sQ0FBQztZQUNwQyxNQUFNLENBQUMsR0FBRywwQkFBVyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdkMsSUFBSSxRQUFRLEdBQUcsR0FBRyxDQUFDO1lBQ25CLElBQUksK0JBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQyxRQUFRLEdBQUcsR0FBRyxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDaEM7WUFDRCxNQUFNLEtBQUssR0FBRztnQkFDWixLQUFLLEVBQUU7b0JBQ0wsS0FBSyxFQUFFLGNBQUssQ0FBQyxNQUFNO29CQUNuQixTQUFTLEVBQUU7d0JBQ1QsR0FBRyxFQUFFLEtBQUssQ0FBQyxVQUFVLENBQUMsS0FBSztxQkFDNUI7b0JBQ0QsS0FBSyxFQUFFLEdBQUcsQ0FBQyxFQUFFO29CQUNiLEdBQUcsRUFBRSxHQUFHLGVBQU0sQ0FBQyxjQUFjLElBQUksS0FBSyxDQUFDLEtBQUssR0FBRztvQkFDL0MsV0FBVyxFQUFFLGNBQWMsT0FBTyxPQUFPLFFBQVEsaUJBQWlCO29CQUNsRSxNQUFNLEVBQUU7d0JBQ047NEJBQ0UsSUFBSSxFQUFFLFFBQVE7NEJBQ2QsS0FBSyxFQUFFLGlCQUFpQixlQUFNLENBQUMsY0FBYyxJQUMzQyxLQUFLLENBQUMsS0FDUixvQkFBb0IsZUFBTSxDQUFDLGtCQUFrQixJQUFJLEtBQUssQ0FBQyxFQUFFLElBQUk7eUJBQzlEO3FCQUNGO29CQUNELFNBQVMsRUFBRSxJQUFJLElBQUksRUFBRTtvQkFDckIsTUFBTSxFQUFFO3dCQUNOLFFBQVEsRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVM7d0JBQy9CLElBQUksRUFBRSxLQUFLLGVBQU0sQ0FBQyxRQUFRLEVBQUU7cUJBQzdCO2lCQUNGO2FBQ0YsQ0FBQztZQUNGLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNqQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7Q0EwQkY7QUEvSkQsNEJBK0pDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUXVldWUgfSBmcm9tIFwiLi9zdWJzY3JpcHRpb24ubW9kZWxcIjtcbmltcG9ydCB7IFF1ZXVlRGF0YSB9IGZyb20gXCIuLy4uL2RhdGEvcXVldWUuZGF0YVwiO1xuaW1wb3J0IG1vbWVudCwgeyB1bml4IH0gZnJvbSBcIm1vbWVudFwiO1xuaW1wb3J0IHsgQ2xpZW50TWFuYWdlciB9IGZyb20gXCIuLi9jb3JlL2NsaWVudFwiO1xuaW1wb3J0IHsgSU1lZGlhIH0gZnJvbSBcIi4uL2ludGVyZmFjZXMvcGFnZS5pbnRlcmZhY2VcIjtcbmltcG9ydCB7IFRpdGxlSGVscGVyIH0gZnJvbSBcIi4uL2hlbHBlcnMvdGl0bGUuaGVscGVyXCI7XG5pbXBvcnQgeyBNZWRpYVNlYXJjaCB9IGZyb20gXCIuLi9jb3JlL21lZGlhLnNlYXJjaFwiO1xuaW1wb3J0IHsgQ29sb3IgfSBmcm9tIFwiLi4vY29yZS9jb2xvcnNcIjtcbmltcG9ydCB7IFN1YnNjcmlwdGlvbkRhdGEgfSBmcm9tIFwiLi4vZGF0YS9zdWJzY3JpcHRpb24uZGF0YVwiO1xuaW1wb3J0IHsgVXNlciB9IGZyb20gXCJkaXNjb3JkLmpzXCI7XG5pbXBvcnQgeyBNZWRpYVN0YXR1cyB9IGZyb20gXCIuLi9jb3JlL21lZGlhLnN0YXR1c1wiO1xuaW1wb3J0IHsgQW5pbWVDYWNoZSB9IGZyb20gXCIuLi9jb3JlL2FuaW1lLmNhY2hlXCI7XG5pbXBvcnQgeyBOdWxsQ2hlY2sgfSBmcm9tIFwiLi4vaGVscGVycy9udWxsLmNoZWNrZXIuaGVscGVyXCI7XG5pbXBvcnQgeyBDb25maWcgfSBmcm9tIFwiLi4vY29yZS9jb25maWdcIjtcblxuZXhwb3J0IGNsYXNzIFF1ZXVlSm9iIHtcbiAgcHJpdmF0ZSBKb2JEYXRlOiBEYXRlO1xuICBjb25zdHJ1Y3RvcihwdWJsaWMgbWVkaWE6IElNZWRpYSwgcHVibGljIHF1ZXVlOiBRdWV1ZSkge31cblxuICBwdWJsaWMgYXN5bmMgQ2hlY2soKSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKGFzeW5jIChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGNvbnN0IHF1ZXVlRXBpc29kZSA9IHRoaXMucXVldWUuTmV4dEVwaXNvZGU7XG4gICAgICBjb25zdCBtZWRpYSA9IHRoaXMubWVkaWE7XG4gICAgICBjb25zdCB0aXRsZSA9IFRpdGxlSGVscGVyLkdldChtZWRpYS50aXRsZSk7XG4gICAgICB0aGlzLkpvYkRhdGUgPSB1bml4KG1lZGlhLm5leHRBaXJpbmdFcGlzb2RlLmFpcmluZ0F0KS50b0RhdGUoKTtcbiAgICAgIGlmIChNZWRpYVN0YXR1cy5Db21wbGV0ZWQobWVkaWEpICYmIG1lZGlhLmVwaXNvZGVzID09PSAxKSB7XG4gICAgICAgIGF3YWl0IHRoaXMuRmluZFVzZXIodGl0bGUsIHF1ZXVlRXBpc29kZSwgbWVkaWEpO1xuICAgICAgICByZXNvbHZlKCk7XG4gICAgICB9IGVsc2UgaWYgKHF1ZXVlRXBpc29kZSA8IG1lZGlhLm5leHRBaXJpbmdFcGlzb2RlLm5leHQpIHtcbiAgICAgICAgYXdhaXQgdGhpcy5GaW5kVXNlcih0aXRsZSwgcXVldWVFcGlzb2RlLCBtZWRpYSk7XG4gICAgICAgIHJlc29sdmUoKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgRmluZFVzZXIodGl0bGU6IHN0cmluZywgbmV4dEVwaXNvZGU6IG51bWJlciwgbWVkaWE6IElNZWRpYSkge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZShhc3luYyAocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBjb25zb2xlLmxvZyhgR2V0dGluZyBzdWJzY3JpYmVycyBvZiBcIiR7dGl0bGV9XCJgKTtcbiAgICAgIGNvbnN0IHN1YnNjcmliZXJzID0gYXdhaXQgU3Vic2NyaXB0aW9uRGF0YS5HZXRTdWJzY3JpYmVycyhcbiAgICAgICAgdGhpcy5tZWRpYS5pZE1hbFxuICAgICAgKTtcbiAgICAgIGlmIChzdWJzY3JpYmVycy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgdGhpcy5VcGRhdGUoKTtcbiAgICAgICAgcmVzb2x2ZSgpO1xuICAgICAgfVxuICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBzdWJzY3JpYmVycy5sZW5ndGg7IGkrKykge1xuICAgICAgICBjb25zdCBzdWJzY3JpYmVyID0gc3Vic2NyaWJlcnNbaV07XG4gICAgICAgIGNvbnN0IHVzZXIgPSBhd2FpdCBDbGllbnRNYW5hZ2VyLkNsaWVudC5mZXRjaFVzZXIoc3Vic2NyaWJlci5EaXNjb3JkSWQpO1xuICAgICAgICBpZiAoTnVsbENoZWNrLkZpbmUodXNlcikpIHtcbiAgICAgICAgICBhd2FpdCB0aGlzLlNlbmRNZXNzYWdlKHRpdGxlLCBuZXh0RXBpc29kZSwgbWVkaWEsIHVzZXIpO1xuICAgICAgICAgIGlmIChpID09PSBzdWJzY3JpYmVycy5sZW5ndGggLSAxKSB7XG4gICAgICAgICAgICBhd2FpdCB0aGlzLlVwZGF0ZSgpO1xuICAgICAgICAgICAgcmVzb2x2ZSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBpZiAoaSA9PT0gc3Vic2NyaWJlcnMubGVuZ3RoIC0gMSkge1xuICAgICAgICAgICAgYXdhaXQgdGhpcy5VcGRhdGUoKTtcbiAgICAgICAgICAgIHJlc29sdmUoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgU2VuZE1lc3NhZ2UoXG4gICAgdGl0bGU6IHN0cmluZyxcbiAgICBuZXh0RXBpc29kZTogbnVtYmVyLFxuICAgIG1lZGlhOiBJTWVkaWEsXG4gICAgdXNlcjogVXNlclxuICApIHtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoYXN5bmMgKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgY29uc3QgZW1iZWQgPSBhd2FpdCB0aGlzLkVtYmVkVGVtcGxhdGUobWVkaWEsIG5leHRFcGlzb2RlKTtcbiAgICAgIGF3YWl0IHVzZXJcbiAgICAgICAgLnNlbmQoZW1iZWQpXG4gICAgICAgIC50aGVuKGFzeW5jICgpID0+IHtcbiAgICAgICAgICBjb25zb2xlLmxvZyhcbiAgICAgICAgICAgIGBETSBoYXMgYmVlbiBzZW50IHRvIFwiJHtcbiAgICAgICAgICAgICAgdXNlci51c2VybmFtZVxuICAgICAgICAgICAgfVwiIGZvciBcIiR7dGl0bGV9IEVwaXNvZGUgJHtuZXh0RXBpc29kZX1cImBcbiAgICAgICAgICApO1xuICAgICAgICAgIC8vIGF3YWl0IHRoaXMuU2xlZXAoMTAwMCk7XG4gICAgICAgICAgLy8gY29uc3Qgc3VwcG9ydCA9IGF3YWl0IHRoaXMuU3VwcG9ydFRlbXBsYXRlKCk7XG4gICAgICAgICAgLy8gYXdhaXQgdXNlci5zZW5kKHN1cHBvcnQpLmNhdGNoKGVyciA9PiB7XG4gICAgICAgICAgLy8gICBjb25zb2xlLmxvZyhlcnIpO1xuICAgICAgICAgIC8vIH0pO1xuICAgICAgICAgIHJlc29sdmUoKTtcbiAgICAgICAgfSlcbiAgICAgICAgLmNhdGNoKGVyciA9PiB7XG4gICAgICAgICAgY29uc29sZS5sb2coZXJyKTtcbiAgICAgICAgICByZXNvbHZlKCk7XG4gICAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBVcGRhdGUoKSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKGFzeW5jIChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGNvbnN0IG1lZGlhID0gYXdhaXQgQW5pbWVDYWNoZS5HZXQodGhpcy5tZWRpYS5pZE1hbCk7XG4gICAgICBpZiAoTnVsbENoZWNrLkZpbmUobWVkaWEpKSB7XG4gICAgICAgIGF3YWl0IFF1ZXVlRGF0YS5VcGRhdGUobWVkaWEsIHRoaXMpO1xuICAgICAgICBjb25zb2xlLmxvZyhgUmVtb3ZlZCBRdWV1ZTogJHttZWRpYS5pZE1hbH1gKTtcbiAgICAgICAgcmVzb2x2ZSgpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY29uc29sZS53YXJuKFxuICAgICAgICAgIGBFcnJvciB3aGlsZSBzZWFyY2hpbmcgOiBbTWVkaWFTZWFyY2guRmluZCgke1xuICAgICAgICAgICAgdGhpcy5tZWRpYS5pZE1hbFxuICAgICAgICAgIH0pXS4gVHJ5aW5nIGFnYWluLi4uYFxuICAgICAgICApO1xuICAgICAgICBhd2FpdCB0aGlzLlVwZGF0ZSgpO1xuICAgICAgICByZXNvbHZlKCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIEVtYmVkVGVtcGxhdGUobWVkaWE6IElNZWRpYSwgZXBpc29kZTogbnVtYmVyKSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlPGFueT4oYXN5bmMgKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgY29uc3QgcHJlZml4ID0gQ29uZmlnLkNPTU1BTkRfUFJFRklYO1xuICAgICAgY29uc3QgY2xpZW50ID0gQ2xpZW50TWFuYWdlci5DbGllbnQ7XG4gICAgICBjb25zdCB0ID0gVGl0bGVIZWxwZXIuR2V0KG1lZGlhLnRpdGxlKTtcbiAgICAgIGxldCBlcGlzb2RlcyA9IGA/YDtcbiAgICAgIGlmIChOdWxsQ2hlY2suRmluZShtZWRpYS5lcGlzb2RlcykpIHtcbiAgICAgICAgZXBpc29kZXMgPSBgJHttZWRpYS5lcGlzb2Rlc31gO1xuICAgICAgfVxuICAgICAgY29uc3QgZW1iZWQgPSB7XG4gICAgICAgIGVtYmVkOiB7XG4gICAgICAgICAgY29sb3I6IENvbG9yLlJhbmRvbSxcbiAgICAgICAgICB0aHVtYm5haWw6IHtcbiAgICAgICAgICAgIHVybDogbWVkaWEuY292ZXJJbWFnZS5sYXJnZVxuICAgICAgICAgIH0sXG4gICAgICAgICAgdGl0bGU6IGAke3R9YCxcbiAgICAgICAgICB1cmw6IGAke0NvbmZpZy5NQUxfQU5JTUVfQkFTRX0vJHttZWRpYS5pZE1hbH0vYCxcbiAgICAgICAgICBkZXNjcmlwdGlvbjogYEVwaXNvZGUgKioqJHtlcGlzb2RlfSoqKi8ke2VwaXNvZGVzfSBpcyBub3cgYWlyaW5nLmAsXG4gICAgICAgICAgZmllbGRzOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIG5hbWU6IGBMaW5rczpgLFxuICAgICAgICAgICAgICB2YWx1ZTogYFtNeUFuaW1lTGlzdF0oJHtDb25maWcuTUFMX0FOSU1FX0JBU0V9LyR7XG4gICAgICAgICAgICAgICAgbWVkaWEuaWRNYWxcbiAgICAgICAgICAgICAgfS8pICB8ICBbQW5pTGlzdF0oJHtDb25maWcuQU5JTElTVF9BTklNRV9CQVNFfS8ke21lZGlhLmlkfS8pYFxuICAgICAgICAgICAgfVxuICAgICAgICAgIF0sXG4gICAgICAgICAgdGltZXN0YW1wOiBuZXcgRGF0ZSgpLFxuICAgICAgICAgIGZvb3Rlcjoge1xuICAgICAgICAgICAgaWNvbl91cmw6IGNsaWVudC51c2VyLmF2YXRhclVSTCxcbiAgICAgICAgICAgIHRleHQ6IGDCqSAke0NvbmZpZy5CT1RfTkFNRX1gXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9O1xuICAgICAgcmVzb2x2ZShlbWJlZCk7XG4gICAgfSk7XG4gIH1cblxuICAvLyBwcml2YXRlIFN1cHBvcnRUZW1wbGF0ZSgpIHtcbiAgLy8gICByZXR1cm4gbmV3IFByb21pc2U8YW55PigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gIC8vICAgICBjb25zdCBlbWJlZCA9IHtcbiAgLy8gICAgICAgZW1iZWQ6IHtcbiAgLy8gICAgICAgICBjb2xvcjogQ29sb3IuUmFuZG9tLFxuICAvLyAgICAgICAgIGZpZWxkczogW1xuICAvLyAgICAgICAgICAge1xuICAvLyAgICAgICAgICAgICBuYW1lOiBgU3VwcG9ydCBtZSBvbiBEaXNjb3JkIEJvdCBMaXN0IChEQkwpYCxcbiAgLy8gICAgICAgICAgICAgdmFsdWU6IGBbVm90ZSB0byAke0NvbmZpZy5CT1RfTkFNRX1dKCR7Q29uZmlnLkRCTF9CT1RfTElOS30vdm90ZSlgXG4gIC8vICAgICAgICAgICB9XG4gIC8vICAgICAgICAgXVxuICAvLyAgICAgICB9XG4gIC8vICAgICB9O1xuICAvLyAgICAgcmVzb2x2ZShlbWJlZCk7XG4gIC8vICAgfSk7XG4gIC8vIH1cblxuICAvLyBwcml2YXRlIFNsZWVwKHRpbWVvdXQ6IG51bWJlcikge1xuICAvLyAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gIC8vICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgLy8gICAgICAgcmVzb2x2ZSgpO1xuICAvLyAgICAgfSwgdGltZW91dCk7XG4gIC8vICAgfSk7XG4gIC8vIH1cbn1cbiJdfQ==