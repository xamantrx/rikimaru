"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sender_1 = require("../../core/sender");
const random_helper_1 = require("../../helpers/random.helper");
const mal_bind_data_1 = require("../../data/mal.bind.data");
const mal_bind_model_1 = require("../../models/mal.bind.model");
const client_1 = require("../../core/client");
const user_data_1 = require("../../data/user.data");
const mal_1 = require("../../core/mal");
const config_1 = require("../../core/config");
const null_checker_helper_1 = require("../../helpers/null.checker.helper");
class MalBindFunction {
    async Execute(message, command, dm) {
        await user_data_1.UserData.Insert(message.author.id).catch((err) => {
            console.log(`Internal error: ${err.message}`);
        });
        this.CheckBind(message, command, dm);
    }
    async CheckBind(message, command, dm) {
        const about = await mal_1.MAL.GetProfileAbout(command.Parameter);
        if (!null_checker_helper_1.NullCheck.Fine(about)) {
            this.NotFindError(message, command);
            return;
        }
        const c = await this.SetCode(message, command, about);
        this.ProcessCode(message, command, dm, c, about);
    }
    async ProcessCode(message, command, dm, c, about) {
        const code = mal_bind_model_1.MalBind.CodeFormat(c);
        const mal = await mal_bind_data_1.MalBindData.Get(message.author.id, command.Parameter);
        if (mal !== null) {
            if (mal.Verified === true) {
                this.SendOK(message, command);
                return;
            }
            else {
                this.CheckProfile(message, command, dm, mal_bind_model_1.MalBind.CodeFormat(mal.Code), about);
            }
        }
        else {
            this.CheckProfile(message, command, dm, code, about);
        }
    }
    async CheckProfile(message, command, dm, code, about) {
        const embed = await this.EmbedTemplate(message, command, code);
        if (!null_checker_helper_1.NullCheck.Fine(about)) {
            this.NotFindError(message, command);
            return;
        }
        else {
            if (about.includes(code)) {
                const v = await mal_bind_data_1.MalBindData.Verify(message.author.id, command.Parameter);
                if (v === null) {
                    sender_1.Sender.Send(message, embed, dm);
                }
                else {
                    if (v.Verified) {
                        this.SendOK(message, command);
                        return;
                    }
                }
            }
            else {
                sender_1.Sender.Send(message, embed, dm);
            }
        }
    }
    EmbedTemplate(message, command, code) {
        return new Promise(async (resolve, reject) => {
            const client = client_1.ClientManager.Client;
            const embed = {
                embed: {
                    title: `${config_1.Config.BOT_NAME} MAL Sync Center`,
                    description: `**${config_1.Config.BOT_NAME} Code not found** on your profile. You first need to verify your ownership.`,
                    color: message.member.highestRole.color,
                    thumbnail: { url: message.author.avatarURL },
                    image: { url: `https://i.imgur.com/UFL2LDu.png` },
                    fields: [
                        {
                            name: `Instruction`,
                            value: `*Copy and paste* the verification code below in your *MAL about section.*. You can place it anywhere.\n[Edit Profile](https://myanimelist.net/editprofile.php)`
                        },
                        { name: `Code`, value: `***${code}***\n\nExample:` }
                    ],
                    timestamp: new Date(),
                    footer: {
                        icon_url: client.user.avatarURL,
                        text: `© ${config_1.Config.BOT_NAME}`
                    }
                }
            };
            resolve(embed);
        });
    }
    SetCode(message, command, about) {
        return new Promise((resolve, reject) => {
            const code = random_helper_1.Random.Range(10000000, 99999999).toString();
            if (null_checker_helper_1.NullCheck.Fine(about)) {
                mal_bind_data_1.MalBindData.Insert(message.author.id, command.Parameter, code).then(() => {
                    resolve(code);
                });
            }
            else {
                resolve(code);
            }
        });
    }
    NotFindError(message, command) {
        message.channel.send(`:regional_indicator_x: Go me nasai! I wasn't able to find mal user: **${command.Parameter}**`);
    }
    SendOK(message, command) {
        message.channel.send(`:white_check_mark: Cool! MAL account **"${command.Parameter}"** is **binded** with **"${message.author.username}#${message.author.discriminator}"**, The code can be remove in **mal about section**.`);
    }
}
exports.MalBindFunction = MalBindFunction;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFsYmluZC5jb21tYW5kLmZ1bmN0aW9uLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbW1hbmQvZnVuY3Rpb25zL21hbGJpbmQuY29tbWFuZC5mdW5jdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUdBLDhDQUEyQztBQUMzQywrREFBcUQ7QUFDckQsNERBQXVEO0FBQ3ZELGdFQUFzRDtBQUN0RCw4Q0FBa0Q7QUFDbEQsb0RBQWdEO0FBQ2hELHdDQUFxQztBQUNyQyw4Q0FBMkM7QUFDM0MsMkVBQThEO0FBRTlELE1BQWEsZUFBZTtJQUMxQixLQUFLLENBQUMsT0FBTyxDQUNYLE9BQWlCLEVBQ2pCLE9BQWtCLEVBQ2xCLEVBQVk7UUFFWixNQUFNLG9CQUFRLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBVSxFQUFFLEVBQUU7WUFDNUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsR0FBRyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7UUFDaEQsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVPLEtBQUssQ0FBQyxTQUFTLENBQUMsT0FBaUIsRUFBRSxPQUFrQixFQUFFLEVBQVk7UUFDekUsTUFBTSxLQUFLLEdBQUcsTUFBTSxTQUFHLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMsK0JBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDMUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDcEMsT0FBTztTQUNSO1FBQ0QsTUFBTSxDQUFDLEdBQUcsTUFBTSxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDbkQsQ0FBQztJQUVPLEtBQUssQ0FBQyxXQUFXLENBQ3ZCLE9BQWdCLEVBQ2hCLE9BQWlCLEVBQ2pCLEVBQVcsRUFDWCxDQUFTLEVBQ1QsS0FBYTtRQUViLE1BQU0sSUFBSSxHQUFHLHdCQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ25DLE1BQU0sR0FBRyxHQUFHLE1BQU0sMkJBQVcsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3hFLElBQUksR0FBRyxLQUFLLElBQUksRUFBRTtZQUNoQixJQUFJLEdBQUcsQ0FBQyxRQUFRLEtBQUssSUFBSSxFQUFFO2dCQUN6QixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFDOUIsT0FBTzthQUNSO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxZQUFZLENBQ2YsT0FBTyxFQUNQLE9BQU8sRUFDUCxFQUFFLEVBQ0Ysd0JBQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUM1QixLQUFLLENBQ04sQ0FBQzthQUNIO1NBQ0Y7YUFBTTtZQUNMLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQ3REO0lBQ0gsQ0FBQztJQUVPLEtBQUssQ0FBQyxZQUFZLENBQ3hCLE9BQWdCLEVBQ2hCLE9BQWlCLEVBQ2pCLEVBQVcsRUFDWCxJQUFZLEVBQ1osS0FBYTtRQUViLE1BQU0sS0FBSyxHQUFHLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQy9ELElBQUksQ0FBQywrQkFBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUMxQixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQztZQUNwQyxPQUFPO1NBQ1I7YUFBTTtZQUNMLElBQUksS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDeEIsTUFBTSxDQUFDLEdBQUcsTUFBTSwyQkFBVyxDQUFDLE1BQU0sQ0FDaEMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQ2pCLE9BQU8sQ0FBQyxTQUFTLENBQ2xCLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFO29CQUNkLGVBQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQztpQkFDakM7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLENBQUMsUUFBUSxFQUFFO3dCQUNkLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLE9BQU8sQ0FBQyxDQUFDO3dCQUM5QixPQUFPO3FCQUNSO2lCQUNGO2FBQ0Y7aUJBQU07Z0JBQ0wsZUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2FBQ2pDO1NBQ0Y7SUFDSCxDQUFDO0lBRU8sYUFBYSxDQUFDLE9BQWdCLEVBQUUsT0FBaUIsRUFBRSxJQUFZO1FBQ3JFLE9BQU8sSUFBSSxPQUFPLENBQU0sS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNoRCxNQUFNLE1BQU0sR0FBRyxzQkFBYSxDQUFDLE1BQU0sQ0FBQztZQUNwQyxNQUFNLEtBQUssR0FBRztnQkFDWixLQUFLLEVBQUU7b0JBQ0wsS0FBSyxFQUFFLEdBQUcsZUFBTSxDQUFDLFFBQVEsa0JBQWtCO29CQUMzQyxXQUFXLEVBQUUsS0FDWCxlQUFNLENBQUMsUUFDVCw2RUFBNkU7b0JBQzdFLEtBQUssRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxLQUFLO29CQUN2QyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUU7b0JBQzVDLEtBQUssRUFBRSxFQUFFLEdBQUcsRUFBRSxpQ0FBaUMsRUFBRTtvQkFDakQsTUFBTSxFQUFFO3dCQUNOOzRCQUNFLElBQUksRUFBRSxhQUFhOzRCQUNuQixLQUFLLEVBQUUsZ0tBQWdLO3lCQUN4Szt3QkFDRCxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLE1BQU0sSUFBSSxpQkFBaUIsRUFBRTtxQkFDckQ7b0JBQ0QsU0FBUyxFQUFFLElBQUksSUFBSSxFQUFFO29CQUNyQixNQUFNLEVBQUU7d0JBQ04sUUFBUSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUzt3QkFDL0IsSUFBSSxFQUFFLEtBQUssZUFBTSxDQUFDLFFBQVEsRUFBRTtxQkFDN0I7aUJBQ0Y7YUFDRixDQUFDO1lBQ0YsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2pCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLE9BQU8sQ0FBQyxPQUFnQixFQUFFLE9BQWlCLEVBQUUsS0FBYTtRQUNoRSxPQUFPLElBQUksT0FBTyxDQUFTLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQzdDLE1BQU0sSUFBSSxHQUFHLHNCQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN6RCxJQUFJLCtCQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUN6QiwyQkFBVyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxPQUFPLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDLElBQUksQ0FDakUsR0FBRyxFQUFFO29CQUNILE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDaEIsQ0FBQyxDQUNGLENBQUM7YUFDSDtpQkFBTTtnQkFDTCxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDZjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLFlBQVksQ0FBQyxPQUFnQixFQUFFLE9BQWlCO1FBQ3RELE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUNsQix5RUFDRSxPQUFPLENBQUMsU0FDVixJQUFJLENBQ0wsQ0FBQztJQUNKLENBQUM7SUFFTyxNQUFNLENBQUMsT0FBZ0IsRUFBRSxPQUFpQjtRQUNoRCxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FDbEIsMkNBQ0UsT0FBTyxDQUFDLFNBQ1YsNkJBQTZCLE9BQU8sQ0FBQyxNQUFNLENBQUMsUUFBUSxJQUNsRCxPQUFPLENBQUMsTUFBTSxDQUFDLGFBQ2pCLHVEQUF1RCxDQUN4RCxDQUFDO0lBQ0osQ0FBQztDQUNGO0FBOUlELDBDQThJQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElDb21tYW5kRnVuY3Rpb24gfSBmcm9tIFwiLi4vLi4vaW50ZXJmYWNlcy9jb21tYW5kLmZ1bmN0aW9uLmludGVyZmFjZVwiO1xuaW1wb3J0IHsgTWVzc2FnZSB9IGZyb20gXCJkaXNjb3JkLmpzXCI7XG5pbXBvcnQgeyBJQ29tbWFuZCB9IGZyb20gXCIuLi8uLi9pbnRlcmZhY2VzL2NvbW1hbmQuaW50ZXJmYWNlXCI7XG5pbXBvcnQgeyBTZW5kZXIgfSBmcm9tIFwiLi4vLi4vY29yZS9zZW5kZXJcIjtcbmltcG9ydCB7IFJhbmRvbSB9IGZyb20gXCIuLi8uLi9oZWxwZXJzL3JhbmRvbS5oZWxwZXJcIjtcbmltcG9ydCB7IE1hbEJpbmREYXRhIH0gZnJvbSBcIi4uLy4uL2RhdGEvbWFsLmJpbmQuZGF0YVwiO1xuaW1wb3J0IHsgTWFsQmluZCB9IGZyb20gXCIuLi8uLi9tb2RlbHMvbWFsLmJpbmQubW9kZWxcIjtcbmltcG9ydCB7IENsaWVudE1hbmFnZXIgfSBmcm9tIFwiLi4vLi4vY29yZS9jbGllbnRcIjtcbmltcG9ydCB7IFVzZXJEYXRhIH0gZnJvbSBcIi4uLy4uL2RhdGEvdXNlci5kYXRhXCI7XG5pbXBvcnQgeyBNQUwgfSBmcm9tIFwiLi4vLi4vY29yZS9tYWxcIjtcbmltcG9ydCB7IENvbmZpZyB9IGZyb20gXCIuLi8uLi9jb3JlL2NvbmZpZ1wiO1xuaW1wb3J0IHsgTnVsbENoZWNrIH0gZnJvbSBcIi4uLy4uL2hlbHBlcnMvbnVsbC5jaGVja2VyLmhlbHBlclwiO1xuXG5leHBvcnQgY2xhc3MgTWFsQmluZEZ1bmN0aW9uIGltcGxlbWVudHMgSUNvbW1hbmRGdW5jdGlvbiB7XG4gIGFzeW5jIEV4ZWN1dGUoXG4gICAgbWVzc2FnZT86IE1lc3NhZ2UsXG4gICAgY29tbWFuZD86IElDb21tYW5kLFxuICAgIGRtPzogYm9vbGVhblxuICApOiBQcm9taXNlPHZvaWQ+IHtcbiAgICBhd2FpdCBVc2VyRGF0YS5JbnNlcnQobWVzc2FnZS5hdXRob3IuaWQpLmNhdGNoKChlcnI6IEVycm9yKSA9PiB7XG4gICAgICBjb25zb2xlLmxvZyhgSW50ZXJuYWwgZXJyb3I6ICR7ZXJyLm1lc3NhZ2V9YCk7XG4gICAgfSk7XG4gICAgdGhpcy5DaGVja0JpbmQobWVzc2FnZSwgY29tbWFuZCwgZG0pO1xuICB9XG5cbiAgcHJpdmF0ZSBhc3luYyBDaGVja0JpbmQobWVzc2FnZT86IE1lc3NhZ2UsIGNvbW1hbmQ/OiBJQ29tbWFuZCwgZG0/OiBib29sZWFuKSB7XG4gICAgY29uc3QgYWJvdXQgPSBhd2FpdCBNQUwuR2V0UHJvZmlsZUFib3V0KGNvbW1hbmQuUGFyYW1ldGVyKTtcbiAgICBpZiAoIU51bGxDaGVjay5GaW5lKGFib3V0KSkge1xuICAgICAgdGhpcy5Ob3RGaW5kRXJyb3IobWVzc2FnZSwgY29tbWFuZCk7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGNvbnN0IGMgPSBhd2FpdCB0aGlzLlNldENvZGUobWVzc2FnZSwgY29tbWFuZCwgYWJvdXQpO1xuICAgIHRoaXMuUHJvY2Vzc0NvZGUobWVzc2FnZSwgY29tbWFuZCwgZG0sIGMsIGFib3V0KTtcbiAgfVxuXG4gIHByaXZhdGUgYXN5bmMgUHJvY2Vzc0NvZGUoXG4gICAgbWVzc2FnZTogTWVzc2FnZSxcbiAgICBjb21tYW5kOiBJQ29tbWFuZCxcbiAgICBkbTogYm9vbGVhbixcbiAgICBjOiBzdHJpbmcsXG4gICAgYWJvdXQ6IHN0cmluZ1xuICApIHtcbiAgICBjb25zdCBjb2RlID0gTWFsQmluZC5Db2RlRm9ybWF0KGMpO1xuICAgIGNvbnN0IG1hbCA9IGF3YWl0IE1hbEJpbmREYXRhLkdldChtZXNzYWdlLmF1dGhvci5pZCwgY29tbWFuZC5QYXJhbWV0ZXIpO1xuICAgIGlmIChtYWwgIT09IG51bGwpIHtcbiAgICAgIGlmIChtYWwuVmVyaWZpZWQgPT09IHRydWUpIHtcbiAgICAgICAgdGhpcy5TZW5kT0sobWVzc2FnZSwgY29tbWFuZCk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuQ2hlY2tQcm9maWxlKFxuICAgICAgICAgIG1lc3NhZ2UsXG4gICAgICAgICAgY29tbWFuZCxcbiAgICAgICAgICBkbSxcbiAgICAgICAgICBNYWxCaW5kLkNvZGVGb3JtYXQobWFsLkNvZGUpLFxuICAgICAgICAgIGFib3V0XG4gICAgICAgICk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuQ2hlY2tQcm9maWxlKG1lc3NhZ2UsIGNvbW1hbmQsIGRtLCBjb2RlLCBhYm91dCk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBhc3luYyBDaGVja1Byb2ZpbGUoXG4gICAgbWVzc2FnZTogTWVzc2FnZSxcbiAgICBjb21tYW5kOiBJQ29tbWFuZCxcbiAgICBkbTogYm9vbGVhbixcbiAgICBjb2RlOiBzdHJpbmcsXG4gICAgYWJvdXQ6IHN0cmluZ1xuICApIHtcbiAgICBjb25zdCBlbWJlZCA9IGF3YWl0IHRoaXMuRW1iZWRUZW1wbGF0ZShtZXNzYWdlLCBjb21tYW5kLCBjb2RlKTtcbiAgICBpZiAoIU51bGxDaGVjay5GaW5lKGFib3V0KSkge1xuICAgICAgdGhpcy5Ob3RGaW5kRXJyb3IobWVzc2FnZSwgY29tbWFuZCk7XG4gICAgICByZXR1cm47XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmIChhYm91dC5pbmNsdWRlcyhjb2RlKSkge1xuICAgICAgICBjb25zdCB2ID0gYXdhaXQgTWFsQmluZERhdGEuVmVyaWZ5KFxuICAgICAgICAgIG1lc3NhZ2UuYXV0aG9yLmlkLFxuICAgICAgICAgIGNvbW1hbmQuUGFyYW1ldGVyXG4gICAgICAgICk7XG4gICAgICAgIGlmICh2ID09PSBudWxsKSB7XG4gICAgICAgICAgU2VuZGVyLlNlbmQobWVzc2FnZSwgZW1iZWQsIGRtKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBpZiAodi5WZXJpZmllZCkge1xuICAgICAgICAgICAgdGhpcy5TZW5kT0sobWVzc2FnZSwgY29tbWFuZCk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBTZW5kZXIuU2VuZChtZXNzYWdlLCBlbWJlZCwgZG0pO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgRW1iZWRUZW1wbGF0ZShtZXNzYWdlOiBNZXNzYWdlLCBjb21tYW5kOiBJQ29tbWFuZCwgY29kZTogc3RyaW5nKSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlPGFueT4oYXN5bmMgKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgY29uc3QgY2xpZW50ID0gQ2xpZW50TWFuYWdlci5DbGllbnQ7XG4gICAgICBjb25zdCBlbWJlZCA9IHtcbiAgICAgICAgZW1iZWQ6IHtcbiAgICAgICAgICB0aXRsZTogYCR7Q29uZmlnLkJPVF9OQU1FfSBNQUwgU3luYyBDZW50ZXJgLFxuICAgICAgICAgIGRlc2NyaXB0aW9uOiBgKioke1xuICAgICAgICAgICAgQ29uZmlnLkJPVF9OQU1FXG4gICAgICAgICAgfSBDb2RlIG5vdCBmb3VuZCoqIG9uIHlvdXIgcHJvZmlsZS4gWW91IGZpcnN0IG5lZWQgdG8gdmVyaWZ5IHlvdXIgb3duZXJzaGlwLmAsXG4gICAgICAgICAgY29sb3I6IG1lc3NhZ2UubWVtYmVyLmhpZ2hlc3RSb2xlLmNvbG9yLFxuICAgICAgICAgIHRodW1ibmFpbDogeyB1cmw6IG1lc3NhZ2UuYXV0aG9yLmF2YXRhclVSTCB9LFxuICAgICAgICAgIGltYWdlOiB7IHVybDogYGh0dHBzOi8vaS5pbWd1ci5jb20vVUZMMkxEdS5wbmdgIH0sXG4gICAgICAgICAgZmllbGRzOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIG5hbWU6IGBJbnN0cnVjdGlvbmAsXG4gICAgICAgICAgICAgIHZhbHVlOiBgKkNvcHkgYW5kIHBhc3RlKiB0aGUgdmVyaWZpY2F0aW9uIGNvZGUgYmVsb3cgaW4geW91ciAqTUFMIGFib3V0IHNlY3Rpb24uKi4gWW91IGNhbiBwbGFjZSBpdCBhbnl3aGVyZS5cXG5bRWRpdCBQcm9maWxlXShodHRwczovL215YW5pbWVsaXN0Lm5ldC9lZGl0cHJvZmlsZS5waHApYFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHsgbmFtZTogYENvZGVgLCB2YWx1ZTogYCoqKiR7Y29kZX0qKipcXG5cXG5FeGFtcGxlOmAgfVxuICAgICAgICAgIF0sXG4gICAgICAgICAgdGltZXN0YW1wOiBuZXcgRGF0ZSgpLFxuICAgICAgICAgIGZvb3Rlcjoge1xuICAgICAgICAgICAgaWNvbl91cmw6IGNsaWVudC51c2VyLmF2YXRhclVSTCxcbiAgICAgICAgICAgIHRleHQ6IGDCqSAke0NvbmZpZy5CT1RfTkFNRX1gXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9O1xuICAgICAgcmVzb2x2ZShlbWJlZCk7XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIFNldENvZGUobWVzc2FnZTogTWVzc2FnZSwgY29tbWFuZDogSUNvbW1hbmQsIGFib3V0OiBzdHJpbmcpIHtcbiAgICByZXR1cm4gbmV3IFByb21pc2U8c3RyaW5nPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBjb25zdCBjb2RlID0gUmFuZG9tLlJhbmdlKDEwMDAwMDAwLCA5OTk5OTk5OSkudG9TdHJpbmcoKTtcbiAgICAgIGlmIChOdWxsQ2hlY2suRmluZShhYm91dCkpIHtcbiAgICAgICAgTWFsQmluZERhdGEuSW5zZXJ0KG1lc3NhZ2UuYXV0aG9yLmlkLCBjb21tYW5kLlBhcmFtZXRlciwgY29kZSkudGhlbihcbiAgICAgICAgICAoKSA9PiB7XG4gICAgICAgICAgICByZXNvbHZlKGNvZGUpO1xuICAgICAgICAgIH1cbiAgICAgICAgKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJlc29sdmUoY29kZSk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIE5vdEZpbmRFcnJvcihtZXNzYWdlOiBNZXNzYWdlLCBjb21tYW5kOiBJQ29tbWFuZCkge1xuICAgIG1lc3NhZ2UuY2hhbm5lbC5zZW5kKFxuICAgICAgYDpyZWdpb25hbF9pbmRpY2F0b3JfeDogR28gbWUgbmFzYWkhIEkgd2Fzbid0IGFibGUgdG8gZmluZCBtYWwgdXNlcjogKioke1xuICAgICAgICBjb21tYW5kLlBhcmFtZXRlclxuICAgICAgfSoqYFxuICAgICk7XG4gIH1cblxuICBwcml2YXRlIFNlbmRPSyhtZXNzYWdlOiBNZXNzYWdlLCBjb21tYW5kOiBJQ29tbWFuZCkge1xuICAgIG1lc3NhZ2UuY2hhbm5lbC5zZW5kKFxuICAgICAgYDp3aGl0ZV9jaGVja19tYXJrOiBDb29sISBNQUwgYWNjb3VudCAqKlwiJHtcbiAgICAgICAgY29tbWFuZC5QYXJhbWV0ZXJcbiAgICAgIH1cIioqIGlzICoqYmluZGVkKiogd2l0aCAqKlwiJHttZXNzYWdlLmF1dGhvci51c2VybmFtZX0jJHtcbiAgICAgICAgbWVzc2FnZS5hdXRob3IuZGlzY3JpbWluYXRvclxuICAgICAgfVwiKiosIFRoZSBjb2RlIGNhbiBiZSByZW1vdmUgaW4gKiptYWwgYWJvdXQgc2VjdGlvbioqLmBcbiAgICApO1xuICB9XG59XG4iXX0=