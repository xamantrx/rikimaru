"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sender_1 = require("../../core/sender");
const random_helper_1 = require("../../helpers/random.helper");
const client_1 = require("../../core/client");
const user_data_1 = require("../../data/user.data");
const config_1 = require("../../core/config");
const ani_bind_model_1 = require("../../models/ani.bind.model");
const ani_bind_data_1 = require("../../data/ani.bind.data");
const anilist_1 = require("../../core/anilist");
const json_helper_1 = require("../../helpers/json.helper");
const anilist_user_model_1 = require("../../models/anilist.user.model");
const null_checker_helper_1 = require("../../helpers/null.checker.helper");
class AniBindFunction {
    async Execute(message, command, dm) {
        await user_data_1.UserData.Insert(message.author.id).catch((err) => {
            console.log(err.message);
        });
        this.CheckBind(message, command, dm);
    }
    async CheckBind(message, command, dm) {
        const anilistUserResult = await anilist_1.AniList.UserQuery(command.Parameter);
        if (!null_checker_helper_1.NullCheck.Fine(anilistUserResult)) {
            this.NotFindError(message, command);
            return;
        }
        else {
            const anilistUser = await json_helper_1.JsonHelper.Convert(anilistUserResult, anilist_user_model_1.Root);
            if (!null_checker_helper_1.NullCheck.Fine(anilistUser)) {
                this.NotFindError(message, command);
                return;
            }
            const user = anilistUser.data.User;
            if (!null_checker_helper_1.NullCheck.Fine(user)) {
                this.NotFindError(message, command);
                return;
            }
            const c = await this.SetCode(message, command, user);
            this.ProcessCode(message, command, dm, c, user);
        }
    }
    async ProcessCode(message, command, dm, c, user) {
        const code = ani_bind_model_1.AniBind.CodeFormat(c);
        const ani = await ani_bind_data_1.AniBindData.Get(message.author.id, user.name);
        if (ani !== null) {
            if (ani.Verified === true) {
                this.SendOK(message, command);
                return;
            }
            else {
                this.CheckProfile(message, command, dm, ani_bind_model_1.AniBind.CodeFormat(ani.Code), user);
            }
        }
        else {
            this.CheckProfile(message, command, dm, code, user);
        }
    }
    async CheckProfile(message, command, dm, code, user) {
        const embed = await this.EmbedTemplate(message, command, code);
        if (null_checker_helper_1.NullCheck.Fine(user.about) && user.about.includes(code)) {
            const v = await ani_bind_data_1.AniBindData.Verify(message.author.id, user.name);
            if (v === null) {
                sender_1.Sender.Send(message, embed, dm);
            }
            else {
                if (v.Verified) {
                    this.SendOK(message, command);
                    return;
                }
            }
        }
        else {
            sender_1.Sender.Send(message, embed, dm);
        }
    }
    EmbedTemplate(message, command, code) {
        return new Promise(async (resolve, reject) => {
            const client = client_1.ClientManager.Client;
            const embed = {
                embed: {
                    title: `${config_1.Config.BOT_NAME} AniList Sync Center`,
                    description: `**${config_1.Config.BOT_NAME} Code not found** on your profile. You first need to verify your ownership.`,
                    color: message.member.highestRole.color,
                    thumbnail: { url: message.author.avatarURL },
                    image: { url: `https://i.imgur.com/SwKmEzo.png` },
                    fields: [
                        {
                            name: `Instruction`,
                            value: `*Copy and paste* the verification code below in your *AniList about section.*. You can place it anywhere.\n[Edit Profile](https://anilist.co/settings)`
                        },
                        { name: `Code`, value: `***${code}***\n\nExample:` }
                    ],
                    timestamp: new Date(),
                    footer: {
                        icon_url: client.user.avatarURL,
                        text: `© ${config_1.Config.BOT_NAME}`
                    }
                }
            };
            resolve(embed);
        });
    }
    SetCode(message, command, user) {
        return new Promise((resolve, reject) => {
            const code = random_helper_1.Random.Range(10000000, 99999999).toString();
            if (null_checker_helper_1.NullCheck.Fine(user)) {
                ani_bind_data_1.AniBindData.Insert(message.author.id, user.id, command.Parameter, code).then(() => {
                    resolve(code);
                });
            }
            else {
                resolve(code);
            }
        });
    }
    NotFindError(message, command) {
        message.channel.send(`:regional_indicator_x: Go me nasai! I wasn't able to find AniList user: **${command.Parameter}**`);
    }
    SendOK(message, command) {
        message.channel.send(`:white_check_mark: Cool! AniList account **"${command.Parameter}"** is **binded** with **"${message.author.username}#${message.author.discriminator}"**, The code can be remove in **anilist about section**.`);
    }
}
exports.AniBindFunction = AniBindFunction;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5pYmluZC5jb21tYW5kLmZ1bmN0aW9uLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbW1hbmQvZnVuY3Rpb25zL2FuaWJpbmQuY29tbWFuZC5mdW5jdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUdBLDhDQUEyQztBQUMzQywrREFBcUQ7QUFDckQsOENBQWtEO0FBQ2xELG9EQUFnRDtBQUNoRCw4Q0FBMkM7QUFDM0MsZ0VBQXNEO0FBQ3RELDREQUF1RDtBQUN2RCxnREFBNkM7QUFDN0MsMkRBQXVEO0FBQ3ZELHdFQUE2RDtBQUM3RCwyRUFBOEQ7QUFFOUQsTUFBYSxlQUFlO0lBQzFCLEtBQUssQ0FBQyxPQUFPLENBQ1gsT0FBaUIsRUFDakIsT0FBa0IsRUFDbEIsRUFBWTtRQUVaLE1BQU0sb0JBQVEsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFVLEVBQUUsRUFBRTtZQUM1RCxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMzQixDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRU8sS0FBSyxDQUFDLFNBQVMsQ0FBQyxPQUFpQixFQUFFLE9BQWtCLEVBQUUsRUFBWTtRQUN6RSxNQUFNLGlCQUFpQixHQUFHLE1BQU0saUJBQU8sQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3JFLElBQUksQ0FBQywrQkFBUyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFO1lBQ3RDLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ3BDLE9BQU87U0FDUjthQUFNO1lBQ0wsTUFBTSxXQUFXLEdBQUcsTUFBTSx3QkFBVSxDQUFDLE9BQU8sQ0FDMUMsaUJBQWlCLEVBQ2pCLHlCQUFJLENBQ0wsQ0FBQztZQUNGLElBQUksQ0FBQywrQkFBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBRTtnQkFDaEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBQ3BDLE9BQU87YUFDUjtZQUNELE1BQU0sSUFBSSxHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ25DLElBQUksQ0FBQywrQkFBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDekIsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBQ3BDLE9BQU87YUFDUjtZQUNELE1BQU0sQ0FBQyxHQUFHLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3JELElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQ2pEO0lBQ0gsQ0FBQztJQUVPLEtBQUssQ0FBQyxXQUFXLENBQ3ZCLE9BQWdCLEVBQ2hCLE9BQWlCLEVBQ2pCLEVBQVcsRUFDWCxDQUFTLEVBQ1QsSUFBVTtRQUVWLE1BQU0sSUFBSSxHQUFHLHdCQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ25DLE1BQU0sR0FBRyxHQUFHLE1BQU0sMkJBQVcsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2hFLElBQUksR0FBRyxLQUFLLElBQUksRUFBRTtZQUNoQixJQUFJLEdBQUcsQ0FBQyxRQUFRLEtBQUssSUFBSSxFQUFFO2dCQUN6QixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFDOUIsT0FBTzthQUNSO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxZQUFZLENBQ2YsT0FBTyxFQUNQLE9BQU8sRUFDUCxFQUFFLEVBQ0Ysd0JBQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUM1QixJQUFJLENBQ0wsQ0FBQzthQUNIO1NBQ0Y7YUFBTTtZQUNMLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQ3JEO0lBQ0gsQ0FBQztJQUVPLEtBQUssQ0FBQyxZQUFZLENBQ3hCLE9BQWdCLEVBQ2hCLE9BQWlCLEVBQ2pCLEVBQVcsRUFDWCxJQUFZLEVBQ1osSUFBVTtRQUVWLE1BQU0sS0FBSyxHQUFHLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQy9ELElBQUksK0JBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQzNELE1BQU0sQ0FBQyxHQUFHLE1BQU0sMkJBQVcsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2pFLElBQUksQ0FBQyxLQUFLLElBQUksRUFBRTtnQkFDZCxlQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUM7YUFDakM7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLENBQUMsUUFBUSxFQUFFO29CQUNkLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLE9BQU8sQ0FBQyxDQUFDO29CQUM5QixPQUFPO2lCQUNSO2FBQ0Y7U0FDRjthQUFNO1lBQ0wsZUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1NBQ2pDO0lBQ0gsQ0FBQztJQUVPLGFBQWEsQ0FBQyxPQUFnQixFQUFFLE9BQWlCLEVBQUUsSUFBWTtRQUNyRSxPQUFPLElBQUksT0FBTyxDQUFNLEtBQUssRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDaEQsTUFBTSxNQUFNLEdBQUcsc0JBQWEsQ0FBQyxNQUFNLENBQUM7WUFDcEMsTUFBTSxLQUFLLEdBQUc7Z0JBQ1osS0FBSyxFQUFFO29CQUNMLEtBQUssRUFBRSxHQUFHLGVBQU0sQ0FBQyxRQUFRLHNCQUFzQjtvQkFDL0MsV0FBVyxFQUFFLEtBQ1gsZUFBTSxDQUFDLFFBQ1QsNkVBQTZFO29CQUM3RSxLQUFLLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsS0FBSztvQkFDdkMsU0FBUyxFQUFFLEVBQUUsR0FBRyxFQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFO29CQUM1QyxLQUFLLEVBQUUsRUFBRSxHQUFHLEVBQUUsaUNBQWlDLEVBQUU7b0JBQ2pELE1BQU0sRUFBRTt3QkFDTjs0QkFDRSxJQUFJLEVBQUUsYUFBYTs0QkFDbkIsS0FBSyxFQUFFLHdKQUF3Sjt5QkFDaEs7d0JBQ0QsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxNQUFNLElBQUksaUJBQWlCLEVBQUU7cUJBQ3JEO29CQUNELFNBQVMsRUFBRSxJQUFJLElBQUksRUFBRTtvQkFDckIsTUFBTSxFQUFFO3dCQUNOLFFBQVEsRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVM7d0JBQy9CLElBQUksRUFBRSxLQUFLLGVBQU0sQ0FBQyxRQUFRLEVBQUU7cUJBQzdCO2lCQUNGO2FBQ0YsQ0FBQztZQUNGLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNqQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTyxPQUFPLENBQUMsT0FBZ0IsRUFBRSxPQUFpQixFQUFFLElBQVU7UUFDN0QsT0FBTyxJQUFJLE9BQU8sQ0FBUyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUM3QyxNQUFNLElBQUksR0FBRyxzQkFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsUUFBUSxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDekQsSUFBSSwrQkFBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDeEIsMkJBQVcsQ0FBQyxNQUFNLENBQ2hCLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUNqQixJQUFJLENBQUMsRUFBRSxFQUNQLE9BQU8sQ0FBQyxTQUFTLEVBQ2pCLElBQUksQ0FDTCxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUU7b0JBQ1YsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNoQixDQUFDLENBQUMsQ0FBQzthQUNKO2lCQUFNO2dCQUNMLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNmO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU8sWUFBWSxDQUFDLE9BQWdCLEVBQUUsT0FBaUI7UUFDdEQsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQ2xCLDZFQUNFLE9BQU8sQ0FBQyxTQUNWLElBQUksQ0FDTCxDQUFDO0lBQ0osQ0FBQztJQUVPLE1BQU0sQ0FBQyxPQUFnQixFQUFFLE9BQWlCO1FBQ2hELE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUNsQiwrQ0FDRSxPQUFPLENBQUMsU0FDViw2QkFBNkIsT0FBTyxDQUFDLE1BQU0sQ0FBQyxRQUFRLElBQ2xELE9BQU8sQ0FBQyxNQUFNLENBQUMsYUFDakIsMkRBQTJELENBQzVELENBQUM7SUFDSixDQUFDO0NBQ0Y7QUF2SkQsMENBdUpDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSUNvbW1hbmRGdW5jdGlvbiB9IGZyb20gXCIuLi8uLi9pbnRlcmZhY2VzL2NvbW1hbmQuZnVuY3Rpb24uaW50ZXJmYWNlXCI7XHJcbmltcG9ydCB7IE1lc3NhZ2UgfSBmcm9tIFwiZGlzY29yZC5qc1wiO1xyXG5pbXBvcnQgeyBJQ29tbWFuZCB9IGZyb20gXCIuLi8uLi9pbnRlcmZhY2VzL2NvbW1hbmQuaW50ZXJmYWNlXCI7XHJcbmltcG9ydCB7IFNlbmRlciB9IGZyb20gXCIuLi8uLi9jb3JlL3NlbmRlclwiO1xyXG5pbXBvcnQgeyBSYW5kb20gfSBmcm9tIFwiLi4vLi4vaGVscGVycy9yYW5kb20uaGVscGVyXCI7XHJcbmltcG9ydCB7IENsaWVudE1hbmFnZXIgfSBmcm9tIFwiLi4vLi4vY29yZS9jbGllbnRcIjtcclxuaW1wb3J0IHsgVXNlckRhdGEgfSBmcm9tIFwiLi4vLi4vZGF0YS91c2VyLmRhdGFcIjtcclxuaW1wb3J0IHsgQ29uZmlnIH0gZnJvbSBcIi4uLy4uL2NvcmUvY29uZmlnXCI7XHJcbmltcG9ydCB7IEFuaUJpbmQgfSBmcm9tIFwiLi4vLi4vbW9kZWxzL2FuaS5iaW5kLm1vZGVsXCI7XHJcbmltcG9ydCB7IEFuaUJpbmREYXRhIH0gZnJvbSBcIi4uLy4uL2RhdGEvYW5pLmJpbmQuZGF0YVwiO1xyXG5pbXBvcnQgeyBBbmlMaXN0IH0gZnJvbSBcIi4uLy4uL2NvcmUvYW5pbGlzdFwiO1xyXG5pbXBvcnQgeyBKc29uSGVscGVyIH0gZnJvbSBcIi4uLy4uL2hlbHBlcnMvanNvbi5oZWxwZXJcIjtcclxuaW1wb3J0IHsgUm9vdCwgVXNlciB9IGZyb20gXCIuLi8uLi9tb2RlbHMvYW5pbGlzdC51c2VyLm1vZGVsXCI7XHJcbmltcG9ydCB7IE51bGxDaGVjayB9IGZyb20gXCIuLi8uLi9oZWxwZXJzL251bGwuY2hlY2tlci5oZWxwZXJcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBBbmlCaW5kRnVuY3Rpb24gaW1wbGVtZW50cyBJQ29tbWFuZEZ1bmN0aW9uIHtcclxuICBhc3luYyBFeGVjdXRlKFxyXG4gICAgbWVzc2FnZT86IE1lc3NhZ2UsXHJcbiAgICBjb21tYW5kPzogSUNvbW1hbmQsXHJcbiAgICBkbT86IGJvb2xlYW5cclxuICApOiBQcm9taXNlPHZvaWQ+IHtcclxuICAgIGF3YWl0IFVzZXJEYXRhLkluc2VydChtZXNzYWdlLmF1dGhvci5pZCkuY2F0Y2goKGVycjogRXJyb3IpID0+IHtcclxuICAgICAgY29uc29sZS5sb2coZXJyLm1lc3NhZ2UpO1xyXG4gICAgfSk7XHJcbiAgICB0aGlzLkNoZWNrQmluZChtZXNzYWdlLCBjb21tYW5kLCBkbSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGFzeW5jIENoZWNrQmluZChtZXNzYWdlPzogTWVzc2FnZSwgY29tbWFuZD86IElDb21tYW5kLCBkbT86IGJvb2xlYW4pIHtcclxuICAgIGNvbnN0IGFuaWxpc3RVc2VyUmVzdWx0ID0gYXdhaXQgQW5pTGlzdC5Vc2VyUXVlcnkoY29tbWFuZC5QYXJhbWV0ZXIpO1xyXG4gICAgaWYgKCFOdWxsQ2hlY2suRmluZShhbmlsaXN0VXNlclJlc3VsdCkpIHtcclxuICAgICAgdGhpcy5Ob3RGaW5kRXJyb3IobWVzc2FnZSwgY29tbWFuZCk7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGNvbnN0IGFuaWxpc3RVc2VyID0gYXdhaXQgSnNvbkhlbHBlci5Db252ZXJ0PFJvb3Q+KFxyXG4gICAgICAgIGFuaWxpc3RVc2VyUmVzdWx0LFxyXG4gICAgICAgIFJvb3RcclxuICAgICAgKTtcclxuICAgICAgaWYgKCFOdWxsQ2hlY2suRmluZShhbmlsaXN0VXNlcikpIHtcclxuICAgICAgICB0aGlzLk5vdEZpbmRFcnJvcihtZXNzYWdlLCBjb21tYW5kKTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuICAgICAgY29uc3QgdXNlciA9IGFuaWxpc3RVc2VyLmRhdGEuVXNlcjtcclxuICAgICAgaWYgKCFOdWxsQ2hlY2suRmluZSh1c2VyKSkge1xyXG4gICAgICAgIHRoaXMuTm90RmluZEVycm9yKG1lc3NhZ2UsIGNvbW1hbmQpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG4gICAgICBjb25zdCBjID0gYXdhaXQgdGhpcy5TZXRDb2RlKG1lc3NhZ2UsIGNvbW1hbmQsIHVzZXIpO1xyXG4gICAgICB0aGlzLlByb2Nlc3NDb2RlKG1lc3NhZ2UsIGNvbW1hbmQsIGRtLCBjLCB1c2VyKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgYXN5bmMgUHJvY2Vzc0NvZGUoXHJcbiAgICBtZXNzYWdlOiBNZXNzYWdlLFxyXG4gICAgY29tbWFuZDogSUNvbW1hbmQsXHJcbiAgICBkbTogYm9vbGVhbixcclxuICAgIGM6IHN0cmluZyxcclxuICAgIHVzZXI6IFVzZXJcclxuICApIHtcclxuICAgIGNvbnN0IGNvZGUgPSBBbmlCaW5kLkNvZGVGb3JtYXQoYyk7XHJcbiAgICBjb25zdCBhbmkgPSBhd2FpdCBBbmlCaW5kRGF0YS5HZXQobWVzc2FnZS5hdXRob3IuaWQsIHVzZXIubmFtZSk7XHJcbiAgICBpZiAoYW5pICE9PSBudWxsKSB7XHJcbiAgICAgIGlmIChhbmkuVmVyaWZpZWQgPT09IHRydWUpIHtcclxuICAgICAgICB0aGlzLlNlbmRPSyhtZXNzYWdlLCBjb21tYW5kKTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5DaGVja1Byb2ZpbGUoXHJcbiAgICAgICAgICBtZXNzYWdlLFxyXG4gICAgICAgICAgY29tbWFuZCxcclxuICAgICAgICAgIGRtLFxyXG4gICAgICAgICAgQW5pQmluZC5Db2RlRm9ybWF0KGFuaS5Db2RlKSxcclxuICAgICAgICAgIHVzZXJcclxuICAgICAgICApO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLkNoZWNrUHJvZmlsZShtZXNzYWdlLCBjb21tYW5kLCBkbSwgY29kZSwgdXNlcik7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGFzeW5jIENoZWNrUHJvZmlsZShcclxuICAgIG1lc3NhZ2U6IE1lc3NhZ2UsXHJcbiAgICBjb21tYW5kOiBJQ29tbWFuZCxcclxuICAgIGRtOiBib29sZWFuLFxyXG4gICAgY29kZTogc3RyaW5nLFxyXG4gICAgdXNlcjogVXNlclxyXG4gICkge1xyXG4gICAgY29uc3QgZW1iZWQgPSBhd2FpdCB0aGlzLkVtYmVkVGVtcGxhdGUobWVzc2FnZSwgY29tbWFuZCwgY29kZSk7XHJcbiAgICBpZiAoTnVsbENoZWNrLkZpbmUodXNlci5hYm91dCkgJiYgdXNlci5hYm91dC5pbmNsdWRlcyhjb2RlKSkge1xyXG4gICAgICBjb25zdCB2ID0gYXdhaXQgQW5pQmluZERhdGEuVmVyaWZ5KG1lc3NhZ2UuYXV0aG9yLmlkLCB1c2VyLm5hbWUpO1xyXG4gICAgICBpZiAodiA9PT0gbnVsbCkge1xyXG4gICAgICAgIFNlbmRlci5TZW5kKG1lc3NhZ2UsIGVtYmVkLCBkbSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaWYgKHYuVmVyaWZpZWQpIHtcclxuICAgICAgICAgIHRoaXMuU2VuZE9LKG1lc3NhZ2UsIGNvbW1hbmQpO1xyXG4gICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgU2VuZGVyLlNlbmQobWVzc2FnZSwgZW1iZWQsIGRtKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgRW1iZWRUZW1wbGF0ZShtZXNzYWdlOiBNZXNzYWdlLCBjb21tYW5kOiBJQ29tbWFuZCwgY29kZTogc3RyaW5nKSB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2U8YW55Pihhc3luYyAocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgIGNvbnN0IGNsaWVudCA9IENsaWVudE1hbmFnZXIuQ2xpZW50O1xyXG4gICAgICBjb25zdCBlbWJlZCA9IHtcclxuICAgICAgICBlbWJlZDoge1xyXG4gICAgICAgICAgdGl0bGU6IGAke0NvbmZpZy5CT1RfTkFNRX0gQW5pTGlzdCBTeW5jIENlbnRlcmAsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjogYCoqJHtcclxuICAgICAgICAgICAgQ29uZmlnLkJPVF9OQU1FXHJcbiAgICAgICAgICB9IENvZGUgbm90IGZvdW5kKiogb24geW91ciBwcm9maWxlLiBZb3UgZmlyc3QgbmVlZCB0byB2ZXJpZnkgeW91ciBvd25lcnNoaXAuYCxcclxuICAgICAgICAgIGNvbG9yOiBtZXNzYWdlLm1lbWJlci5oaWdoZXN0Um9sZS5jb2xvcixcclxuICAgICAgICAgIHRodW1ibmFpbDogeyB1cmw6IG1lc3NhZ2UuYXV0aG9yLmF2YXRhclVSTCB9LFxyXG4gICAgICAgICAgaW1hZ2U6IHsgdXJsOiBgaHR0cHM6Ly9pLmltZ3VyLmNvbS9Td0ttRXpvLnBuZ2AgfSxcclxuICAgICAgICAgIGZpZWxkczogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgbmFtZTogYEluc3RydWN0aW9uYCxcclxuICAgICAgICAgICAgICB2YWx1ZTogYCpDb3B5IGFuZCBwYXN0ZSogdGhlIHZlcmlmaWNhdGlvbiBjb2RlIGJlbG93IGluIHlvdXIgKkFuaUxpc3QgYWJvdXQgc2VjdGlvbi4qLiBZb3UgY2FuIHBsYWNlIGl0IGFueXdoZXJlLlxcbltFZGl0IFByb2ZpbGVdKGh0dHBzOi8vYW5pbGlzdC5jby9zZXR0aW5ncylgXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHsgbmFtZTogYENvZGVgLCB2YWx1ZTogYCoqKiR7Y29kZX0qKipcXG5cXG5FeGFtcGxlOmAgfVxyXG4gICAgICAgICAgXSxcclxuICAgICAgICAgIHRpbWVzdGFtcDogbmV3IERhdGUoKSxcclxuICAgICAgICAgIGZvb3Rlcjoge1xyXG4gICAgICAgICAgICBpY29uX3VybDogY2xpZW50LnVzZXIuYXZhdGFyVVJMLFxyXG4gICAgICAgICAgICB0ZXh0OiBgwqkgJHtDb25maWcuQk9UX05BTUV9YFxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfTtcclxuICAgICAgcmVzb2x2ZShlbWJlZCk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgU2V0Q29kZShtZXNzYWdlOiBNZXNzYWdlLCBjb21tYW5kOiBJQ29tbWFuZCwgdXNlcjogVXNlcikge1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlPHN0cmluZz4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICBjb25zdCBjb2RlID0gUmFuZG9tLlJhbmdlKDEwMDAwMDAwLCA5OTk5OTk5OSkudG9TdHJpbmcoKTtcclxuICAgICAgaWYgKE51bGxDaGVjay5GaW5lKHVzZXIpKSB7XHJcbiAgICAgICAgQW5pQmluZERhdGEuSW5zZXJ0KFxyXG4gICAgICAgICAgbWVzc2FnZS5hdXRob3IuaWQsXHJcbiAgICAgICAgICB1c2VyLmlkLFxyXG4gICAgICAgICAgY29tbWFuZC5QYXJhbWV0ZXIsXHJcbiAgICAgICAgICBjb2RlXHJcbiAgICAgICAgKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgIHJlc29sdmUoY29kZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcmVzb2x2ZShjb2RlKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIE5vdEZpbmRFcnJvcihtZXNzYWdlOiBNZXNzYWdlLCBjb21tYW5kOiBJQ29tbWFuZCkge1xyXG4gICAgbWVzc2FnZS5jaGFubmVsLnNlbmQoXHJcbiAgICAgIGA6cmVnaW9uYWxfaW5kaWNhdG9yX3g6IEdvIG1lIG5hc2FpISBJIHdhc24ndCBhYmxlIHRvIGZpbmQgQW5pTGlzdCB1c2VyOiAqKiR7XHJcbiAgICAgICAgY29tbWFuZC5QYXJhbWV0ZXJcclxuICAgICAgfSoqYFxyXG4gICAgKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgU2VuZE9LKG1lc3NhZ2U6IE1lc3NhZ2UsIGNvbW1hbmQ6IElDb21tYW5kKSB7XHJcbiAgICBtZXNzYWdlLmNoYW5uZWwuc2VuZChcclxuICAgICAgYDp3aGl0ZV9jaGVja19tYXJrOiBDb29sISBBbmlMaXN0IGFjY291bnQgKipcIiR7XHJcbiAgICAgICAgY29tbWFuZC5QYXJhbWV0ZXJcclxuICAgICAgfVwiKiogaXMgKipiaW5kZWQqKiB3aXRoICoqXCIke21lc3NhZ2UuYXV0aG9yLnVzZXJuYW1lfSMke1xyXG4gICAgICAgIG1lc3NhZ2UuYXV0aG9yLmRpc2NyaW1pbmF0b3JcclxuICAgICAgfVwiKiosIFRoZSBjb2RlIGNhbiBiZSByZW1vdmUgaW4gKiphbmlsaXN0IGFib3V0IHNlY3Rpb24qKi5gXHJcbiAgICApO1xyXG4gIH1cclxufVxyXG4iXX0=