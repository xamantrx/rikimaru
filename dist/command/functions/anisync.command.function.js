"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sender_1 = require("../../core/sender");
const user_data_1 = require("../../data/user.data");
const subscription_data_1 = require("../../data/subscription.data");
const client_1 = require("../../core/client");
const anime_cache_1 = require("../../core/anime.cache");
const queue_data_1 = require("../../data/queue.data");
const media_status_1 = require("../../core/media.status");
const config_1 = require("../../core/config");
const ani_bind_data_1 = require("../../data/ani.bind.data");
const anilist_1 = require("../../core/anilist");
const json_helper_1 = require("../../helpers/json.helper");
const ani_sync_model_1 = require("../../models/ani.sync.model");
const null_checker_helper_1 = require("../../helpers/null.checker.helper");
class AniSyncFunction {
    async Execute(message, command, dm) {
        await this.GetAll(message, command, dm).catch(err => {
            console.log(err);
            this.SendStatus(message, dm);
        });
        const client = client_1.ClientManager.Client;
        const res$m = `Your *AniList currently watching list* is now synced with your subscriptions.`;
        const prefix = config_1.Config.COMMAND_PREFIX;
        sender_1.Sender.Send(message, {
            embed: {
                color: message.member.highestRole.color,
                thumbnail: { url: message.author.avatarURL },
                title: `${config_1.Config.BOT_NAME} AniList Auto Subscribe`,
                description: res$m,
                fields: [
                    {
                        name: `To unsubscribe, type:`,
                        value: `\`${prefix}unsub anime title or keyword here\``
                    },
                    {
                        name: `To view all subscription, type:`,
                        value: `\`${prefix}viewsubs\``
                    },
                    {
                        name: `Please Note: `,
                        value: `If you've just modified your list, please wait at least 1 minute to **${prefix}anisync**.`
                    }
                ],
                timestamp: new Date(),
                footer: {
                    icon_url: client.user.avatarURL,
                    text: `© ${config_1.Config.BOT_NAME}`
                }
            }
        }, dm);
    }
    GetAll(message, command, dm) {
        return new Promise(async (resolve, reject) => {
            await user_data_1.UserData.Insert(message.author.id).catch(err => console.log(err));
            this.Run(resolve, reject, message, command, dm);
        });
    }
    async Run(resolve, reject, message, command, dm) {
        const ani = await ani_bind_data_1.AniBindData.Get(message.author.id, command.Parameter);
        if (ani === null) {
            this.SendStatus(message, dm);
            return;
        }
        if (ani.Verified === true) {
            const user = await user_data_1.UserData.GetUser(message.author.id);
            if (user === null) {
                sender_1.Sender.SendError(message, dm);
                return;
            }
            const apiResult = await anilist_1.AniList.MediaListQuery(ani.AniListId);
            if (!null_checker_helper_1.NullCheck.Fine(apiResult)) {
                sender_1.Sender.SendError(message, dm);
                return;
            }
            const converted = await json_helper_1.JsonHelper.Convert(apiResult, ani_sync_model_1.ListRoot);
            if (!null_checker_helper_1.NullCheck.Fine(converted)) {
                sender_1.Sender.SendError(message, dm);
                return;
            }
            const list = converted.data.collection.lists[0].entries;
            if (!null_checker_helper_1.NullCheck.Fine(list)) {
                sender_1.Sender.SendError(message, dm);
                return;
            }
            const subs = await subscription_data_1.SubscriptionData.GetUserSubs(user.Id);
            for (let i = 0; i < subs.length; i++) {
                const $s = subs[i];
                const anilistAnime = list.find($ma => $ma.media.idMal === $s.MediaId);
                if (anilistAnime !== null && anilistAnime !== undefined) {
                }
                else {
                    await subscription_data_1.SubscriptionData.Delete($s.MediaId, user.DiscordId);
                }
            }
            for (let i = 0; i < list.length; i++) {
                const fromList = list[i];
                const anime = await anime_cache_1.AnimeCache.Get(fromList.media.idMal);
                if (media_status_1.MediaStatus.Ongoing(anime) || media_status_1.MediaStatus.NotYetAired(anime)) {
                    await queue_data_1.QueueData.Insert(anime.idMal, anime.nextAiringEpisode.next);
                    await subscription_data_1.SubscriptionData.Insert(anime.idMal, user.Id);
                    if (i === list.length - 1) {
                        resolve();
                    }
                }
                else {
                    if (i === list.length - 1) {
                        resolve();
                    }
                }
            }
        }
        else {
            this.SendStatus(message, dm);
        }
    }
    SendStatus(message, dm) {
        sender_1.Sender.Send(message, `Oops! Your AniList account is not verified and binded.\n Enter the command **${config_1.Config.COMMAND_PREFIX}anibind anilistusername**`, dm);
    }
}
exports.AniSyncFunction = AniSyncFunction;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5pc3luYy5jb21tYW5kLmZ1bmN0aW9uLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbW1hbmQvZnVuY3Rpb25zL2FuaXN5bmMuY29tbWFuZC5mdW5jdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUdBLDhDQUEyQztBQUMzQyxvREFBZ0Q7QUFDaEQsb0VBQWdFO0FBQ2hFLDhDQUFrRDtBQUNsRCx3REFBb0Q7QUFDcEQsc0RBQWtEO0FBQ2xELDBEQUFzRDtBQUN0RCw4Q0FBMkM7QUFDM0MsNERBQXVEO0FBQ3ZELGdEQUE2QztBQUM3QywyREFBdUQ7QUFDdkQsZ0VBQXVEO0FBQ3ZELDJFQUE4RDtBQUU5RCxNQUFhLGVBQWU7SUFDMUIsS0FBSyxDQUFDLE9BQU8sQ0FDWCxPQUFpQixFQUNqQixPQUFrQixFQUNsQixFQUFZO1FBRVosTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ2xELE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDakIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDL0IsQ0FBQyxDQUFDLENBQUM7UUFDSCxNQUFNLE1BQU0sR0FBRyxzQkFBYSxDQUFDLE1BQU0sQ0FBQztRQUNwQyxNQUFNLEtBQUssR0FBRywrRUFBK0UsQ0FBQztRQUM5RixNQUFNLE1BQU0sR0FBRyxlQUFNLENBQUMsY0FBYyxDQUFDO1FBQ3JDLGVBQU0sQ0FBQyxJQUFJLENBQ1QsT0FBTyxFQUNQO1lBQ0UsS0FBSyxFQUFFO2dCQUNMLEtBQUssRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxLQUFLO2dCQUN2QyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUU7Z0JBQzVDLEtBQUssRUFBRSxHQUFHLGVBQU0sQ0FBQyxRQUFRLHlCQUF5QjtnQkFDbEQsV0FBVyxFQUFFLEtBQUs7Z0JBQ2xCLE1BQU0sRUFBRTtvQkFDTjt3QkFDRSxJQUFJLEVBQUUsdUJBQXVCO3dCQUM3QixLQUFLLEVBQUUsS0FBSyxNQUFNLHFDQUFxQztxQkFDeEQ7b0JBQ0Q7d0JBQ0UsSUFBSSxFQUFFLGlDQUFpQzt3QkFDdkMsS0FBSyxFQUFFLEtBQUssTUFBTSxZQUFZO3FCQUMvQjtvQkFDRDt3QkFDRSxJQUFJLEVBQUUsZUFBZTt3QkFDckIsS0FBSyxFQUFFLHlFQUF5RSxNQUFNLFlBQVk7cUJBQ25HO2lCQUNGO2dCQUNELFNBQVMsRUFBRSxJQUFJLElBQUksRUFBRTtnQkFDckIsTUFBTSxFQUFFO29CQUNOLFFBQVEsRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVM7b0JBQy9CLElBQUksRUFBRSxLQUFLLGVBQU0sQ0FBQyxRQUFRLEVBQUU7aUJBQzdCO2FBQ0Y7U0FDRixFQUNELEVBQUUsQ0FDSCxDQUFDO0lBQ0osQ0FBQztJQUVPLE1BQU0sQ0FBQyxPQUFnQixFQUFFLE9BQWlCLEVBQUUsRUFBVztRQUM3RCxPQUFPLElBQUksT0FBTyxDQUFDLEtBQUssRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDM0MsTUFBTSxvQkFBUSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUN4RSxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztRQUNsRCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTyxLQUFLLENBQUMsR0FBRyxDQUNmLE9BQW1CLEVBQ25CLE1BQThCLEVBQzlCLE9BQWdCLEVBQ2hCLE9BQWlCLEVBQ2pCLEVBQVc7UUFFWCxNQUFNLEdBQUcsR0FBRyxNQUFNLDJCQUFXLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN4RSxJQUFJLEdBQUcsS0FBSyxJQUFJLEVBQUU7WUFDaEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDN0IsT0FBTztTQUNSO1FBRUQsSUFBSSxHQUFHLENBQUMsUUFBUSxLQUFLLElBQUksRUFBRTtZQUN6QixNQUFNLElBQUksR0FBRyxNQUFNLG9CQUFRLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDdkQsSUFBSSxJQUFJLEtBQUssSUFBSSxFQUFFO2dCQUNqQixlQUFNLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDOUIsT0FBTzthQUNSO1lBQ0QsTUFBTSxTQUFTLEdBQUcsTUFBTSxpQkFBTyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDOUQsSUFBSSxDQUFDLCtCQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFO2dCQUM5QixlQUFNLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDOUIsT0FBTzthQUNSO1lBQ0QsTUFBTSxTQUFTLEdBQUcsTUFBTSx3QkFBVSxDQUFDLE9BQU8sQ0FBVyxTQUFTLEVBQUUseUJBQVEsQ0FBQyxDQUFDO1lBQzFFLElBQUksQ0FBQywrQkFBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRTtnQkFDOUIsZUFBTSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQzlCLE9BQU87YUFDUjtZQUNELE1BQU0sSUFBSSxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7WUFDeEQsSUFBSSxDQUFDLCtCQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUN6QixlQUFNLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDOUIsT0FBTzthQUNSO1lBQ0QsTUFBTSxJQUFJLEdBQUcsTUFBTSxvQ0FBZ0IsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3pELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNwQyxNQUFNLEVBQUUsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ25CLE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssS0FBSyxFQUFFLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ3RFLElBQUksWUFBWSxLQUFLLElBQUksSUFBSSxZQUFZLEtBQUssU0FBUyxFQUFFO2lCQUN4RDtxQkFBTTtvQkFDTCxNQUFNLG9DQUFnQixDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztpQkFDM0Q7YUFDRjtZQUVELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNwQyxNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pCLE1BQU0sS0FBSyxHQUFHLE1BQU0sd0JBQVUsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDekQsSUFBSSwwQkFBVyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSwwQkFBVyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDaEUsTUFBTSxzQkFBUyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDbEUsTUFBTSxvQ0FBZ0IsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQ3BELElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUN6QixPQUFPLEVBQUUsQ0FBQztxQkFDWDtpQkFDRjtxQkFBTTtvQkFDTCxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3QkFDekIsT0FBTyxFQUFFLENBQUM7cUJBQ1g7aUJBQ0Y7YUFDRjtTQUNGO2FBQU07WUFDTCxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztTQUM5QjtJQUNILENBQUM7SUFFTyxVQUFVLENBQUMsT0FBZ0IsRUFBRSxFQUFXO1FBQzlDLGVBQU0sQ0FBQyxJQUFJLENBQ1QsT0FBTyxFQUNQLGdGQUNFLGVBQU0sQ0FBQyxjQUNULDJCQUEyQixFQUMzQixFQUFFLENBQ0gsQ0FBQztJQUNKLENBQUM7Q0FDRjtBQTlIRCwwQ0E4SEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJQ29tbWFuZEZ1bmN0aW9uIH0gZnJvbSBcIi4uLy4uL2ludGVyZmFjZXMvY29tbWFuZC5mdW5jdGlvbi5pbnRlcmZhY2VcIjtcclxuaW1wb3J0IHsgTWVzc2FnZSB9IGZyb20gXCJkaXNjb3JkLmpzXCI7XHJcbmltcG9ydCB7IElDb21tYW5kIH0gZnJvbSBcIi4uLy4uL2ludGVyZmFjZXMvY29tbWFuZC5pbnRlcmZhY2VcIjtcclxuaW1wb3J0IHsgU2VuZGVyIH0gZnJvbSBcIi4uLy4uL2NvcmUvc2VuZGVyXCI7XHJcbmltcG9ydCB7IFVzZXJEYXRhIH0gZnJvbSBcIi4uLy4uL2RhdGEvdXNlci5kYXRhXCI7XHJcbmltcG9ydCB7IFN1YnNjcmlwdGlvbkRhdGEgfSBmcm9tIFwiLi4vLi4vZGF0YS9zdWJzY3JpcHRpb24uZGF0YVwiO1xyXG5pbXBvcnQgeyBDbGllbnRNYW5hZ2VyIH0gZnJvbSBcIi4uLy4uL2NvcmUvY2xpZW50XCI7XHJcbmltcG9ydCB7IEFuaW1lQ2FjaGUgfSBmcm9tIFwiLi4vLi4vY29yZS9hbmltZS5jYWNoZVwiO1xyXG5pbXBvcnQgeyBRdWV1ZURhdGEgfSBmcm9tIFwiLi4vLi4vZGF0YS9xdWV1ZS5kYXRhXCI7XHJcbmltcG9ydCB7IE1lZGlhU3RhdHVzIH0gZnJvbSBcIi4uLy4uL2NvcmUvbWVkaWEuc3RhdHVzXCI7XHJcbmltcG9ydCB7IENvbmZpZyB9IGZyb20gXCIuLi8uLi9jb3JlL2NvbmZpZ1wiO1xyXG5pbXBvcnQgeyBBbmlCaW5kRGF0YSB9IGZyb20gXCIuLi8uLi9kYXRhL2FuaS5iaW5kLmRhdGFcIjtcclxuaW1wb3J0IHsgQW5pTGlzdCB9IGZyb20gXCIuLi8uLi9jb3JlL2FuaWxpc3RcIjtcclxuaW1wb3J0IHsgSnNvbkhlbHBlciB9IGZyb20gXCIuLi8uLi9oZWxwZXJzL2pzb24uaGVscGVyXCI7XHJcbmltcG9ydCB7IExpc3RSb290IH0gZnJvbSBcIi4uLy4uL21vZGVscy9hbmkuc3luYy5tb2RlbFwiO1xyXG5pbXBvcnQgeyBOdWxsQ2hlY2sgfSBmcm9tIFwiLi4vLi4vaGVscGVycy9udWxsLmNoZWNrZXIuaGVscGVyXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgQW5pU3luY0Z1bmN0aW9uIGltcGxlbWVudHMgSUNvbW1hbmRGdW5jdGlvbiB7XHJcbiAgYXN5bmMgRXhlY3V0ZShcclxuICAgIG1lc3NhZ2U/OiBNZXNzYWdlLFxyXG4gICAgY29tbWFuZD86IElDb21tYW5kLFxyXG4gICAgZG0/OiBib29sZWFuXHJcbiAgKTogUHJvbWlzZTx2b2lkPiB7XHJcbiAgICBhd2FpdCB0aGlzLkdldEFsbChtZXNzYWdlLCBjb21tYW5kLCBkbSkuY2F0Y2goZXJyID0+IHtcclxuICAgICAgY29uc29sZS5sb2coZXJyKTtcclxuICAgICAgdGhpcy5TZW5kU3RhdHVzKG1lc3NhZ2UsIGRtKTtcclxuICAgIH0pO1xyXG4gICAgY29uc3QgY2xpZW50ID0gQ2xpZW50TWFuYWdlci5DbGllbnQ7XHJcbiAgICBjb25zdCByZXMkbSA9IGBZb3VyICpBbmlMaXN0IGN1cnJlbnRseSB3YXRjaGluZyBsaXN0KiBpcyBub3cgc3luY2VkIHdpdGggeW91ciBzdWJzY3JpcHRpb25zLmA7XHJcbiAgICBjb25zdCBwcmVmaXggPSBDb25maWcuQ09NTUFORF9QUkVGSVg7XHJcbiAgICBTZW5kZXIuU2VuZChcclxuICAgICAgbWVzc2FnZSxcclxuICAgICAge1xyXG4gICAgICAgIGVtYmVkOiB7XHJcbiAgICAgICAgICBjb2xvcjogbWVzc2FnZS5tZW1iZXIuaGlnaGVzdFJvbGUuY29sb3IsXHJcbiAgICAgICAgICB0aHVtYm5haWw6IHsgdXJsOiBtZXNzYWdlLmF1dGhvci5hdmF0YXJVUkwgfSxcclxuICAgICAgICAgIHRpdGxlOiBgJHtDb25maWcuQk9UX05BTUV9IEFuaUxpc3QgQXV0byBTdWJzY3JpYmVgLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246IHJlcyRtLFxyXG4gICAgICAgICAgZmllbGRzOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICBuYW1lOiBgVG8gdW5zdWJzY3JpYmUsIHR5cGU6YCxcclxuICAgICAgICAgICAgICB2YWx1ZTogYFxcYCR7cHJlZml4fXVuc3ViIGFuaW1lIHRpdGxlIG9yIGtleXdvcmQgaGVyZVxcYGBcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgIG5hbWU6IGBUbyB2aWV3IGFsbCBzdWJzY3JpcHRpb24sIHR5cGU6YCxcclxuICAgICAgICAgICAgICB2YWx1ZTogYFxcYCR7cHJlZml4fXZpZXdzdWJzXFxgYFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgbmFtZTogYFBsZWFzZSBOb3RlOiBgLFxyXG4gICAgICAgICAgICAgIHZhbHVlOiBgSWYgeW91J3ZlIGp1c3QgbW9kaWZpZWQgeW91ciBsaXN0LCBwbGVhc2Ugd2FpdCBhdCBsZWFzdCAxIG1pbnV0ZSB0byAqKiR7cHJlZml4fWFuaXN5bmMqKi5gXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIF0sXHJcbiAgICAgICAgICB0aW1lc3RhbXA6IG5ldyBEYXRlKCksXHJcbiAgICAgICAgICBmb290ZXI6IHtcclxuICAgICAgICAgICAgaWNvbl91cmw6IGNsaWVudC51c2VyLmF2YXRhclVSTCxcclxuICAgICAgICAgICAgdGV4dDogYMKpICR7Q29uZmlnLkJPVF9OQU1FfWBcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH0sXHJcbiAgICAgIGRtXHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBHZXRBbGwobWVzc2FnZTogTWVzc2FnZSwgY29tbWFuZDogSUNvbW1hbmQsIGRtOiBib29sZWFuKSB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2UoYXN5bmMgKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICBhd2FpdCBVc2VyRGF0YS5JbnNlcnQobWVzc2FnZS5hdXRob3IuaWQpLmNhdGNoKGVyciA9PiBjb25zb2xlLmxvZyhlcnIpKTtcclxuICAgICAgdGhpcy5SdW4ocmVzb2x2ZSwgcmVqZWN0LCBtZXNzYWdlLCBjb21tYW5kLCBkbSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgYXN5bmMgUnVuKFxyXG4gICAgcmVzb2x2ZTogKCkgPT4gdm9pZCxcclxuICAgIHJlamVjdDogKHJlYXNvbj86IGFueSkgPT4gdm9pZCxcclxuICAgIG1lc3NhZ2U6IE1lc3NhZ2UsXHJcbiAgICBjb21tYW5kOiBJQ29tbWFuZCxcclxuICAgIGRtOiBib29sZWFuXHJcbiAgKSB7XHJcbiAgICBjb25zdCBhbmkgPSBhd2FpdCBBbmlCaW5kRGF0YS5HZXQobWVzc2FnZS5hdXRob3IuaWQsIGNvbW1hbmQuUGFyYW1ldGVyKTtcclxuICAgIGlmIChhbmkgPT09IG51bGwpIHtcclxuICAgICAgdGhpcy5TZW5kU3RhdHVzKG1lc3NhZ2UsIGRtKTtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChhbmkuVmVyaWZpZWQgPT09IHRydWUpIHtcclxuICAgICAgY29uc3QgdXNlciA9IGF3YWl0IFVzZXJEYXRhLkdldFVzZXIobWVzc2FnZS5hdXRob3IuaWQpO1xyXG4gICAgICBpZiAodXNlciA9PT0gbnVsbCkge1xyXG4gICAgICAgIFNlbmRlci5TZW5kRXJyb3IobWVzc2FnZSwgZG0pO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG4gICAgICBjb25zdCBhcGlSZXN1bHQgPSBhd2FpdCBBbmlMaXN0Lk1lZGlhTGlzdFF1ZXJ5KGFuaS5BbmlMaXN0SWQpO1xyXG4gICAgICBpZiAoIU51bGxDaGVjay5GaW5lKGFwaVJlc3VsdCkpIHtcclxuICAgICAgICBTZW5kZXIuU2VuZEVycm9yKG1lc3NhZ2UsIGRtKTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuICAgICAgY29uc3QgY29udmVydGVkID0gYXdhaXQgSnNvbkhlbHBlci5Db252ZXJ0PExpc3RSb290PihhcGlSZXN1bHQsIExpc3RSb290KTtcclxuICAgICAgaWYgKCFOdWxsQ2hlY2suRmluZShjb252ZXJ0ZWQpKSB7XHJcbiAgICAgICAgU2VuZGVyLlNlbmRFcnJvcihtZXNzYWdlLCBkbSk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcbiAgICAgIGNvbnN0IGxpc3QgPSBjb252ZXJ0ZWQuZGF0YS5jb2xsZWN0aW9uLmxpc3RzWzBdLmVudHJpZXM7XHJcbiAgICAgIGlmICghTnVsbENoZWNrLkZpbmUobGlzdCkpIHtcclxuICAgICAgICBTZW5kZXIuU2VuZEVycm9yKG1lc3NhZ2UsIGRtKTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuICAgICAgY29uc3Qgc3VicyA9IGF3YWl0IFN1YnNjcmlwdGlvbkRhdGEuR2V0VXNlclN1YnModXNlci5JZCk7XHJcbiAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgc3Vicy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgIGNvbnN0ICRzID0gc3Vic1tpXTtcclxuICAgICAgICBjb25zdCBhbmlsaXN0QW5pbWUgPSBsaXN0LmZpbmQoJG1hID0+ICRtYS5tZWRpYS5pZE1hbCA9PT0gJHMuTWVkaWFJZCk7XHJcbiAgICAgICAgaWYgKGFuaWxpc3RBbmltZSAhPT0gbnVsbCAmJiBhbmlsaXN0QW5pbWUgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBhd2FpdCBTdWJzY3JpcHRpb25EYXRhLkRlbGV0ZSgkcy5NZWRpYUlkLCB1c2VyLkRpc2NvcmRJZCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGxpc3QubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICBjb25zdCBmcm9tTGlzdCA9IGxpc3RbaV07XHJcbiAgICAgICAgY29uc3QgYW5pbWUgPSBhd2FpdCBBbmltZUNhY2hlLkdldChmcm9tTGlzdC5tZWRpYS5pZE1hbCk7XHJcbiAgICAgICAgaWYgKE1lZGlhU3RhdHVzLk9uZ29pbmcoYW5pbWUpIHx8IE1lZGlhU3RhdHVzLk5vdFlldEFpcmVkKGFuaW1lKSkge1xyXG4gICAgICAgICAgYXdhaXQgUXVldWVEYXRhLkluc2VydChhbmltZS5pZE1hbCwgYW5pbWUubmV4dEFpcmluZ0VwaXNvZGUubmV4dCk7XHJcbiAgICAgICAgICBhd2FpdCBTdWJzY3JpcHRpb25EYXRhLkluc2VydChhbmltZS5pZE1hbCwgdXNlci5JZCk7XHJcbiAgICAgICAgICBpZiAoaSA9PT0gbGlzdC5sZW5ndGggLSAxKSB7XHJcbiAgICAgICAgICAgIHJlc29sdmUoKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgaWYgKGkgPT09IGxpc3QubGVuZ3RoIC0gMSkge1xyXG4gICAgICAgICAgICByZXNvbHZlKCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLlNlbmRTdGF0dXMobWVzc2FnZSwgZG0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBTZW5kU3RhdHVzKG1lc3NhZ2U6IE1lc3NhZ2UsIGRtOiBib29sZWFuKSB7XHJcbiAgICBTZW5kZXIuU2VuZChcclxuICAgICAgbWVzc2FnZSxcclxuICAgICAgYE9vcHMhIFlvdXIgQW5pTGlzdCBhY2NvdW50IGlzIG5vdCB2ZXJpZmllZCBhbmQgYmluZGVkLlxcbiBFbnRlciB0aGUgY29tbWFuZCAqKiR7XHJcbiAgICAgICAgQ29uZmlnLkNPTU1BTkRfUFJFRklYXHJcbiAgICAgIH1hbmliaW5kIGFuaWxpc3R1c2VybmFtZSoqYCxcclxuICAgICAgZG1cclxuICAgICk7XHJcbiAgfVxyXG59XHJcbiJdfQ==