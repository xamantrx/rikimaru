import { ICommand } from "../interfaces/command.interface";
import { NullCheck } from "../helpers/null.checker.helper";

export class Command implements ICommand {
  public Name: string;
  public Parameter: string = null;
  constructor(name: string, parameter: string) {
    this.Name = name.trim();
    if (NullCheck.Fine(parameter)) {
      this.Parameter = parameter.trim();
    }
  }
}
