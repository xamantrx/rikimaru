import { Config } from "./config";
import rp from "request-promise";
import { JsonHelper } from "../helpers/json.helper";
import { MalAnime } from "../models/mal.anime.model";
import cheerio from "cheerio";

export class MAL {
  private static Fetching = false;

  public static GetCWList(username: string) {
    return new Promise<MalAnime[]>(async (resolve, reject) => {
      await this.OnDoneFetch();
      const url = Config.MAL_CW_LINK(username);
      const options = {
        uri: url,
        json: true
      };
      this.Fetching = true;
      rp(options)
        .then(async (result: any) => {
          const converted = await JsonHelper.ArrayConvert<MalAnime>(
            result,
            MalAnime
          );
          if (converted != null || converted !== undefined) {
            this.Fetching = false;
            resolve(converted);
          } else {
            console.log(`Result is either 'null' or 'undefined'.`);
            this.Fetching = false;
            resolve(null);
          }
        })
        .catch((err: any) => {
          console.log(err);
          this.Fetching = false;
          resolve(null);
        });
    });
  }

  public static GetProfileAbout(username: string) {
    return new Promise<string>(async (resolve, reject) => {
      await this.OnDoneFetch();
      const url = `${Config.MAL_PROFILE_BASE}/${username}`;
      const options = {
        uri: url,
        transform: function(body: string) {
          return cheerio.load(body);
        }
      };
      this.Fetching = true;
      rp(options)
        .then(($: CheerioStatic) => {
          this.Fetching = false;
          resolve(
            $(".profile-about-user")
              .find(".word-break")
              .text()
          );
        })
        .catch(() => {
          console.log(`MAL user "${username}" not found...`);
          this.Fetching = false;
          resolve(null);
        });
    });
  }

  private static OnDoneFetch() {
    return new Promise((resolve, reject) => {
      setInterval(() => {
        if (!this.Fetching) {
          resolve();
        }
      }, 100);
    });
  }
}
