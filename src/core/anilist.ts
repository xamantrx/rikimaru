import endeavor = require("endeavor");
import { SearchVariables } from "../graphql/variables/search.variables";
import { GraphQL } from "../graphql/graphql";
import { Config } from "./config";
import rp from "request-promise";

export class AniList {
  private static Querying = false;
  private static Pinging = false;

  public static MediaSearch(search: string) {
    return new Promise<object>(async (resolve, reject) => {
      const query = GraphQL.SearchQL;
      const variables = SearchVariables.Get(search, 1, 100, "ANIME");
      await this.OnQueryDone();
      this.Querying = true;
      await endeavor
        .queryAnilist({ query, variables })
        .then(result => {
          this.Querying = false;
          resolve(result);
        })
        .catch(error => {
          this.Querying = false;
          reject(error);
        });
    });
  }

  public static MediaQuery(id: number) {
    return new Promise<object>(async (resolve, reject) => {
      const query = GraphQL.AnimeQL;
      const variables = SearchVariables.Media(id);
      await this.OnQueryDone();
      this.Querying = true;
      await endeavor
        .queryAnilist({ query, variables })
        .then(result => {
          this.Querying = false;
          resolve(result);
        })
        .catch(error => {
          this.Querying = false;
          console.log(error);
        });
    });
  }

  public static UserQuery(username: string) {
    return new Promise<object>(async (resolve, reject) => {
      const pinged = await this.Ping(username);
      if (pinged) {
        const query = GraphQL.UserQL;
        const variables = SearchVariables.User(username);
        await this.OnQueryDone();
        this.Querying = true;
        await endeavor
          .queryAnilist({ query, variables })
          .then(result => {
            this.Querying = false;
            resolve(result);
          })
          .catch(error => {
            console.log(error);
            this.Querying = false;
            resolve(null);
          });
      } else {
        console.log(`AniList user "${username}" was not found...`);
        resolve(null);
      }
    });
  }

  public static async MediaListQuery(id: number) {
    return new Promise<object>(async (resolve, reject) => {
      const query = GraphQL.UserMediaListQL;
      const variables = SearchVariables.UserMediaList(id);
      await this.OnQueryDone();
      this.Querying = true;
      await endeavor
        .queryAnilist({ query, variables })
        .then(result => {
          this.Querying = false;
          resolve(result);
        })
        .catch(error => {
          console.log(error);
          this.Querying = false;
          resolve(null);
        });
    });
  }

  private static Ping(username: string) {
    return new Promise<boolean>(async (resolve, reject) => {
      await this.OnPingDone();
      const url = `${Config.ANILIST_USER_BASE}/${username}`;
      const options = { uri: url };
      this.Pinging = true;
      rp(options)
        .then(async () => {
          this.Pinging = false;
          resolve(true);
        })
        .catch(() => {
          this.Pinging = false;
          resolve(false);
        });
    });
  }

  private static OnQueryDone() {
    return new Promise((resolve, reject) => {
      setInterval(() => {
        if (!this.Querying) {
          resolve();
        }
      }, 100);
    });
  }

  private static OnPingDone() {
    return new Promise((resolve, reject) => {
      setInterval(() => {
        if (!this.Pinging) {
          resolve();
        }
      }, 100);
    });
  }
}
