import { Client, User, ClientUser } from "discord.js";
import { Config } from "./config";
// import DBL from "dblapi.js";
import { NullCheck } from "../helpers/null.checker.helper";

export class ClientManager {
  public static Client = new Client();

  public static Init() {
    // const dbl = new DBL(Config.DBL_TOKEN);
    this.Client.on("guildCreate", guild => {
      console.log(
        `New server joined: ${guild.name} (Id: ${guild.id}). This server has ${
          guild.memberCount
        } members!`
      );
    });

    this.Client.on("ready", () => {
      console.log(
        `Bot has started, with ${this.Client.users.size} users, in ${
          this.Client.channels.size
        } channels of ${this.Client.guilds.size} servers.`
      );
      // dbl.postStats(client.guilds.size);
      // setInterval(() => {
      //   dbl.postStats(client.guilds.size);
      // }, 1800000);
    });
    this.Client.login(Config.BOT_TOKEN);
  }

  public static get BotName() {
    return this.Client.user.username;
  }

  public static GetClientUser() {
    return new Promise<ClientUser>((resolve, reject) => {
      setInterval(() => {
        if (NullCheck.Fine(this.Client.user)) {
          resolve(this.Client.user);
        }
      }, 100);
    });
  }
}
