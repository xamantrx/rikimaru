import { BotCommand } from "./bot.command";
import {
  help,
  dmhelp,
  when,
  dmwhen,
  sub,
  viewsubs,
  ping,
  dmping,
  logall,
  unsub,
  dmsub,
  dmviewsubs,
  dmunsub
} from "./commands";
import { ICommand } from "../interfaces/command.interface";
import { malbind, malsync, anibind, anisync } from "./commands";

export class CommandManager {
  private static BotCommands: BotCommand[] = [];

  public static Init(): void {
    this.BotCommands.push(help);
    this.BotCommands.push(dmhelp);
    this.BotCommands.push(when);
    this.BotCommands.push(dmwhen);
    this.BotCommands.push(sub);
    this.BotCommands.push(dmsub);
    this.BotCommands.push(viewsubs);
    this.BotCommands.push(dmviewsubs);
    this.BotCommands.push(unsub);
    this.BotCommands.push(dmunsub);
    this.BotCommands.push(malbind);
    this.BotCommands.push(malsync);
    this.BotCommands.push(anibind);
    this.BotCommands.push(anisync);
    this.BotCommands.push(ping);
    this.BotCommands.push(dmping);
    this.BotCommands.push(logall);
  }

  public static get Commands() {
    return this.BotCommands;
  }

  public static Validate(command: ICommand) {
    return new Promise<BotCommand>((resolve, reject) => {
      for (let i = 0; i < this.BotCommands.length; i++) {
        const cmd = this.BotCommands[i];
        if (cmd.Name === command.Name) {
          resolve(cmd);
          return;
        } else {
          if (i === this.BotCommands.length - 1) {
            console.log(`Unknown Command.`);
            resolve(null);
          }
        }
      }
    });
  }
}
